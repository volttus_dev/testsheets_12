# -*- coding: utf-8 -*-
import base64
from email.utils import formataddr

from werkzeug.urls import url_encode

from odoo.osv.query import Query
# from odoo.fields import FailedValue

from collections import defaultdict
import logging

from odoo import api, exceptions
from odoo import tools, _
from odoo import fields, models
from odoo.exceptions import AccessError

_logger = logging.getLogger(__name__)


# class electric_component_base(MailThread):
class electric_component_base(models.Model):
    _mail_flat_thread = True  # flatten the discussino history

    @api.multi
    def message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None):
        """ Main public API to add followers to a record set. Its main purpose is
        to perform access rights checks before calling ``_message_subscribe``. """
        if not self or (not partner_ids and not channel_ids):
            return True

        partner_ids = partner_ids or []
        channel_ids = channel_ids or []
        adding_current = set(partner_ids) == set([self.env.user.partner_id.id])
        customer_ids = [] if adding_current else None

        if not channel_ids and partner_ids and adding_current:
            try:
                self.check_access_rights('read')
                self.check_access_rule('read')
            except exceptions.AccessError:
                return False
        else:
            self.check_access_rights('write')
            self.check_access_rule('write')

        # filter inactive
        if partner_ids and not adding_current:
            partner_ids = self.env['res.partner'].sudo().search([('id', 'in', partner_ids), ('active', '=', True)]).ids

        return self._message_subscribe(partner_ids, channel_ids, subtype_ids, customer_ids=customer_ids)

    def _message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None, customer_ids=None):
        """ Main private API to add followers to a record set. This method adds
        partners and channels, given their IDs, as followers of all records
        contained in the record set.

        If subtypes are given existing followers are erased with new subtypes.
        If default one have to be computed only missing followers will be added
        with default subtypes matching the record set model.

        This private method does not specifically check for access right. Use
        ``message_subscribe`` public API when not sure about access rights.

        :param customer_ids: see ``_insert_followers`` """
        if not self:
            return True

        if not subtype_ids:
            self.env['mail.followers']._insert_followers(
                self._name, self.ids, partner_ids, None, channel_ids, None,
                customer_ids=customer_ids)
        else:
            self.env['mail.followers']._insert_followers(
                self._name, self.ids,
                partner_ids, dict((pid, subtype_ids) for pid in partner_ids),
                channel_ids, dict((cid, subtype_ids) for cid in channel_ids),
                customer_ids=customer_ids, check_existing=True, existing_policy='replace')

        return True


    def _message_post_after_hook(self, message, msg_vals, model_description=False, mail_auto_delete=True):
        """ Hook to add custom behavior after having posted the message. Both
        message and computed value are given, to try to lessen query count by
        using already-computed values instead of having to rebrowse things. """
        # Set main attachment field if necessary
        attachment_ids = msg_vals['attachment_ids']
        if not self._abstract and attachment_ids and self.ids and not self.message_main_attachment_id:
            all_attachments = self.env['ir.attachment'].browse([attachment_tuple[1] for attachment_tuple in attachment_ids])
            prioritary_attachments = all_attachments.filtered(lambda x: x.mimetype.endswith('pdf')) \
                                     or all_attachments.filtered(lambda x: x.mimetype.startswith('image')) \
                                     or all_attachments
            self.sudo().write({'message_main_attachment_id': prioritary_attachments[0].id})
        # Notify recipients of the newly-created message (Inbox / Email + channels)
        if msg_vals.get('moderation_status') != 'pending_moderation':
            message._notify(
                self, msg_vals,
                force_send=self.env.context.get('mail_notify_force_send', True),
                send_after_commit=True,
                model_description=model_description,
                mail_auto_delete=mail_auto_delete,
            )

            # Post-process: subscribe author
            if msg_vals['author_id'] and msg_vals['model'] and self.ids and msg_vals['message_type'] != 'notification' and not self._context.get('mail_create_nosubscribe'):
                self._message_subscribe([msg_vals['author_id']])
        else:
            message._notify_pending_by_chat()


    def _message_post_process_attachments(self, attachments, attachment_ids, message_data):
        """ Preprocess attachments for mail_thread.message_post() or mail_mail.create().

        :param list attachments: list of attachment tuples in the form ``(name,content)``,
                                 where content is NOT base64 encoded
        :param list attachment_ids: a list of attachment ids, not in tomany command form
        :param dict message_data: model: the model of the attachments parent record,
          res_id: the id of the attachments parent record
        """
        IrAttachment = self.env['ir.attachment']
        m2m_attachment_ids = []
        cid_mapping = {}
        fname_mapping = {}
        if attachment_ids:
            filtered_attachment_ids = self.env['ir.attachment'].sudo().search([
                ('res_model', '=', 'mail.compose.message'),
                ('create_uid', '=', self._uid),
                ('id', 'in', attachment_ids)])
            if filtered_attachment_ids:
                filtered_attachment_ids.write({'res_model': message_data['model'], 'res_id': message_data['res_id']})
            m2m_attachment_ids += [(4, id) for id in attachment_ids]
        # Handle attachments parameter, that is a dictionary of attachments
        for attachment in attachments:
            cid = False
            if len(attachment) == 2:
                name, content = attachment
            elif len(attachment) == 3:
                name, content, info = attachment
                cid = info and info.get('cid')
            else:
                continue
            if isinstance(content, pycompat.text_type):
                content = content.encode('utf-8')
            elif content is None:
                continue
            data_attach = {
                'name': name,
                'datas': base64.b64encode(content),
                'type': 'binary',
                'datas_fname': name,
                'description': name,
                'res_model': message_data['model'],
                'res_id': message_data['res_id'],
            }
            new_attachment = IrAttachment.create(data_attach)
            m2m_attachment_ids.append((4, new_attachment.id))
            if cid:
                cid_mapping[cid] = new_attachment
            fname_mapping[name] = new_attachment

        if cid_mapping and message_data.get('body'):
            root = lxml.html.fromstring(tools.ustr(message_data['body']))
            postprocessed = False
            for node in root.iter('img'):
                if node.get('src', '').startswith('cid:'):
                    cid = node.get('src').split('cid:')[1]
                    attachment = cid_mapping.get(cid)
                    if not attachment:
                        attachment = fname_mapping.get(node.get('data-filename'), '')
                    if attachment:
                        attachment.generate_access_token()
                        node.set('src', '/web/image/%s?access_token=%s' % (attachment.id, attachment.access_token))
                        postprocessed = True
            if postprocessed:
                body = lxml.html.tostring(root, pretty_print=False, encoding='UTF-8')
                message_data['body'] = body

        return m2m_attachment_ids


    @api.multi
    @api.returns('mail.message', lambda value: value.id)
    def message_post(self, body='', subject=None,
                     message_type='notification', subtype=None,
                     parent_id=False, attachments=None,
                     notif_layout=False, add_sign=True, model_description=False,
                     mail_auto_delete=True, **kwargs):
        """ Post a new message in an existing thread, returning the new
            mail.message ID.
            :param int thread_id: thread ID to post into, or list with one ID;
                if False/0, mail.message model will also be set as False
            :param str body: body of the message, usually raw HTML that will
                be sanitized
            :param str type: see mail_message.message_type field
            :param int parent_id: handle reply to a previous message by adding the
                parent partners to the message in case of private discussion
            :param tuple(str,str) attachments or list id: list of attachment tuples in the form
                ``(name,content)``, where content is NOT base64 encoded
            Extra keyword arguments will be used as default column values for the
            new mail.message record. Special cases:
                - attachment_ids: supposed not attached to any document; attach them
                    to the related document. Should only be set by Chatter.
            :return int: ID of newly created mail.message
        """
        if attachments is None:
            attachments = {}
        if self.ids and not self.ensure_one():
            raise exceptions.Warning(
                _('Invalid record set: should be called as model (without records) or on single-record recordset'))

        # if we're processing a message directly coming from the gateway, the destination model was
        # set in the context.
        model = False
        if self.ids:
            self.ensure_one()
            model = kwargs.get('model', False) if self._name == 'mail.thread' else self._name
            if model and model != self._name and hasattr(self.env[model], 'message_post'):
                return self.env[model].browse(self.ids).message_post(
                    body=body, subject=subject, message_type=message_type,
                    subtype=subtype, parent_id=parent_id, attachments=attachments,
                    notif_layout=notif_layout, add_sign=add_sign,
                    mail_auto_delete=mail_auto_delete, model_description=model_description, **kwargs)

        # 0: Find the message's author, because we need it for private discussion
        author_id = kwargs.get('author_id')
        if author_id is None:  # keep False values
            author_id = self.env['mail.message']._get_default_author().id

        # 2: Private message: add recipients (recipients and author of parent message) - current author
        #   + legacy-code management (! we manage only 4 and 6 commands)
        partner_ids = set()
        kwargs_partner_ids = kwargs.pop('partner_ids', [])
        for partner_id in kwargs_partner_ids:
            if isinstance(partner_id, (list, tuple)) and partner_id[0] == 4 and len(partner_id) == 2:
                partner_ids.add(partner_id[1])
            if isinstance(partner_id, (list, tuple)) and partner_id[0] == 6 and len(partner_id) == 3:
                partner_ids |= set(partner_id[2])
            elif isinstance(partner_id, pycompat.integer_types):
                partner_ids.add(partner_id)
            else:
                pass  # we do not manage anything else
        if parent_id and not model:
            parent_message = self.env['mail.message'].browse(parent_id)
            private_followers = set([partner.id for partner in parent_message.partner_ids])
            if parent_message.author_id:
                private_followers.add(parent_message.author_id.id)
            private_followers -= set([author_id])
            partner_ids |= private_followers

        # 4: mail.message.subtype
        subtype_id = kwargs.get('subtype_id', False)
        if not subtype_id:
            subtype = subtype or 'mt_note'
            if '.' not in subtype:
                subtype = 'mail.%s' % subtype
            subtype_id = self.env['ir.model.data'].xmlid_to_res_id(subtype)

        # automatically subscribe recipients if asked to
        if self._context.get('mail_post_autofollow') and self.ids and partner_ids:
            partner_to_subscribe = partner_ids
            if self._context.get('mail_post_autofollow_partner_ids'):
                partner_to_subscribe = [p for p in partner_ids if
                                        p in self._context.get('mail_post_autofollow_partner_ids')]
            self.message_subscribe(list(partner_to_subscribe))

        # _mail_flat_thread: automatically set free messages to the first posted message
        MailMessage = self.env['mail.message']
        if self._mail_flat_thread and model and not parent_id and self.ids:
            messages = MailMessage.search(['&', ('res_id', '=', self.ids[0]), ('model', '=', model)], order="id ASC",
                                          limit=1)
            parent_id = messages.ids and messages.ids[0] or False
        # we want to set a parent: force to set the parent_id to the oldest ancestor, to avoid having more than 1 level of thread
        elif parent_id:
            messages = MailMessage.sudo().search([('id', '=', parent_id), ('parent_id', '!=', False)], limit=1)
            # avoid loops when finding ancestors
            processed_list = []
            if messages:
                message = messages[0]
                while (message.parent_id and message.parent_id.id not in processed_list):
                    processed_list.append(message.parent_id.id)
                    message = message.parent_id
                parent_id = message.id

        values = kwargs
        values.update({
            'author_id': author_id,
            'model': model,
            'res_id': model and self.ids[0] or False,
            'body': body,
            'subject': subject or False,
            'message_type': message_type,
            'parent_id': parent_id,
            'subtype_id': subtype_id,
            'partner_ids': [(4, pid) for pid in partner_ids],
            'channel_ids': kwargs.get('channel_ids', []),
            'add_sign': add_sign
        })
        if notif_layout:
            values['layout'] = notif_layout

        # 3. Attachments
        #   - HACK TDE FIXME: Chatter: attachments linked to the document (not done JS-side), load the message
        attachment_ids = self._message_post_process_attachments(attachments, kwargs.pop('attachment_ids', []), values)
        values['attachment_ids'] = attachment_ids

        # Avoid warnings about non-existing fields
        for x in ('from', 'to', 'cc'):
            values.pop(x, None)

        # Post the message
        # canned_response_ids are added by js to be used by other computations (odoobot)
        # we need to pop it from values since it is not stored on mail.message
        canned_response_ids = values.pop('canned_response_ids', False)
        new_message = MailMessage.create(values)
        values['canned_response_ids'] = canned_response_ids
        self._message_post_after_hook(new_message, values, model_description=model_description,
                                      mail_auto_delete=mail_auto_delete)
        return new_message

    @api.multi
    def _message_add_suggested_recipient(self, result, partner=None, email=None, reason=''):
        """ Called by message_get_suggested_recipients, to add a suggested
            recipient in the result dictionary. The form is :
                partner_id, partner_name<partner_email> or partner_name, reason """
        self.ensure_one()
        if email and not partner:
            # get partner info from email
            partner_info = self.message_partner_info_from_emails([email])[0]
            if partner_info.get('partner_id'):
                partner = self.env['res.partner'].sudo().browse([partner_info['partner_id']])[0]
        if email and email in [val[1] for val in result[self.ids[0]]]:  # already existing email -> skip
            return result
        if partner and partner in self.message_partner_ids:  # recipient already in the followers -> skip
            return result
        if partner and partner.id in [val[0] for val in result[self.ids[0]]]:  # already existing partner ID -> skip
            return result
        if partner and partner.email:  # complete profile: id, name <email>
            result[self.ids[0]].append((partner.id, '%s<%s>' % (partner.name, partner.email), reason))
        elif partner:  # incomplete profile: id, name
            result[self.ids[0]].append((partner.id, '%s' % (partner.name), reason))
        else:  # unknown partner, we are probably managing an email address
            result[self.ids[0]].append((False, email, reason))
        return result

    @api.multi
    def message_get_suggested_recipients(self):
        """ Returns suggested recipients for ids. Those are a list of
        tuple (partner_id, partner_name, reason), to be managed by Chatter. """
        result = dict((res_id, []) for res_id in self.ids)
        if 'user_id' in self._fields:
            for obj in self.sudo():  # SUPERUSER because of a read on res.users that would crash otherwise
                if not obj.user_id or not obj.user_id.partner_id:
                    continue
                obj._message_add_suggested_recipient(result, partner=obj.user_id.partner_id,
                                                     reason=self._fields['user_id'].string)
        return result

    def get_record_as_default(self, cr, uid, components, component):
        field_keys = components.fields_get_keys()
        field_values = component.fields_get(field_keys)
        default_dict = {}
        for key in field_keys:
            if key in ['site_id', 'project_id', 'delegated_id']:
                continue
            else:
                if key in field_values:
                    field_value = field_values[key]
                    type = ''
                    if ('type' in field_value):
                        type = field_value['type']
                    if (component[key] != False) & (type != 'binary'):
                        default_dict['default_' + key] = str(component[key])
        return default_dict

    def get_current_employee(self, cr, uid, context=None):
        employee = self.pool['hr.employee']
        domain = [('user_id', '=', uid)]
        employees = []
        for rec in employee.browse(cr, uid, employee.search(cr, uid, domain, context=context), context=context):
            employees.append(rec.id)
        return employees

    def open_component_view(self):
        for component in self:
            if 'delegated_id' in component:
                def_id = component.id
            else:
                true_components = self.env[component.component_model]
                def_id = true_components.search([('delegated_id', '=', component.id)], limit=1)[0].id
            return {
                'type': 'ir.actions.act_window',
                'res_model': component.component_model,  # this model
                'res_id': def_id,  # the current wizard record
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new'}

    def test_avaliable(self):
        # Aqui se pregunta por uno de los modelos que estarán en el tese,
        # pudiera ser cualquiera
        return 'component_test.dry_transformer_test' in self.pool;

    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': "component.component", 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', "component.component"), ('res_id', 'in', self.ids)])
        return action

    def action_get_component_form_view(self, cr, uid, ids, context=None):
        for component in self.browse(cr, uid, ids, context):
            electric_component = self.pool.get(component.component_model)
            for electric in electric_component.browse(cr, uid, ids, context=context):
                return electric_component.get_formview_action(cr, uid, electric.id, context)

    def electric_info(self):
        # res = dict.fromkeys(ids, "")
        res = ""
        return res

    def get_formview_action(self, cr, uid, id, context=None):
        context = context or {}
        component = self.browse(cr, uid, id, {'limit': 1})
        electric_component = self.pool.get(component.component_model)
        return super(models.Model, electric_component).get_formview_action(cr, uid, id, context=context)

    def _test_image_small(self, vals):
        if 'image_small' not in vals:
            if ('image' in vals):
                vals['image_small'] = tools.image_resize_image_small(vals.get('image'), avoid_if_small=True)
            else:
                if ('image_medium' in vals):
                    vals['image_small'] = tools.image_resize_image_small(vals.get('image_medium'), avoid_if_small=True)
        return vals

    def remove_component(self, cr, uid, ids, arg, context=None):
        return False


class electric_component(electric_component_base):
    _inherit = 'component.component'
    _description = 'Components'

    @api.one
    def electric_info(self):
        return ""  # super(electric_component, self).electric_info()

    @api.one
    def _test_count(self):
        if "component_test.order" in self.env:
            for component in self:
                count = self.env["component_test.order"].search_count([('base_component_id', "=", component.id)])
                self.test_count = count
        else:
            self.test_count = 0

    @api.one
    def _get_attachment_number(self):
        component_docs = self.env['ir.attachment']
        self.attachment_number = component_docs.search_count(
            [('res_model', '=', "component.component"), ('res_id', '=', self.ids)])

    # Columns
    # electric_info = fields.Char(compute='electric_info', string='Information')
    customer_id = fields.Many2one('res.partner', 'Customer')
    component_model = fields.Char('Component Model')
    component_model_test = fields.Char('Component Model Test')
    test_count = fields.Integer(compute='_test_count', string='Test Sheets')
    attachment_number = fields.Integer(compute='_get_attachment_number', string="Documents Attached")
    attachment_ids = fields.One2many('ir.attachment', 'res_id',
                                     domain=[('res_model', '=', 'component.component')],
                                     context={'default_res_model': 'component.component'},
                                     string='Documents')
    color = fields.Selection([(1, 'white'), (2, 'antiquewhite'), (3, 'palegreen'), (4, 'cyan'), (5, 'mintcream')],
                             'Color', default=1)

    @api.model
    def create(self, values):
        return super(electric_component, self).create(values)

    def can_create(self):
        return self.electric_component_type != 'all_components'

    # _defaults = {
    #     'electric_component_type': 'all_components',
    #     'color': 1
    # }

    def update_hotzone(self, vals):
        return vals

    def component_color(self, cr, uid, ids, context=None):
        res = dict.fromkeys(ids, 0)
        for component in self.browse(cr, uid, ids, context=context):
            res[component.id] = component.electric_component_type

    def unlink(self):
        for component in self.browse(self.ids):
            tests = self._test_count()
            # Si el componente tiene test vamos a quitarlo de la lista de los que hay que borrar
            # y vamos a desactivarlo
            if component.test_count > 0:
                self.write({'active': False})
                self.ids.remove(component.id)
                continue
            if not ('delegated_id' in component):
                true_components = self.env[component.component_model]
                true_components.search([('delegated_id', '=', component.id)], limit=1).unlink()
        if self.ids:
            return super(electric_component, self).unlink()
