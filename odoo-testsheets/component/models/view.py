# from odoo.addons.base import

from odoo import fields, models


class View(models.Model):
    _inherit = 'ir.ui.view'
    type = fields.Selection(selection_add=[('component_explorer', 'Component Explorer'), ('component_explorer_tree', 'Component Explorer Tree')])


class ActWindowView(models.Model):
    _inherit = ['ir.actions.act_window.view']
    view_mode = fields.Selection(selection_add=[('component_explorer', 'Component Explorer'), ('component_explorer_tree', 'Component Explorer Tree')])


class ActWindows(models.Model):
    _inherit = ['ir.actions.act_window']
    view_type = fields.Selection(selection_add=[('component_explorer', 'Component Explorer'), ('component_explorer_tree', 'Component Explorer Tree')])
