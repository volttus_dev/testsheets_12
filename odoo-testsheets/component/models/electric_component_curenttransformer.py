# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'curent_transformer'
ASSET_MODEL = 'component.curent_transformer'
ASSET_MODEL_TEST = 'component_test.curent_transformer_test'


class ElectricComponentcurentTransformer(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'HV=' + str(rec.HV) + ' ,'
            value = value +'LV=' + str(rec.LV) + ' ,'
            value = value +'PowerRating=' + str(rec.PowerRating) + ' ,'
            res[rec.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data. Specific fields of Current Transformers ***************
    PowerRating = fields.Char('PowerRating', size=20, required=False)
    HV = fields.Char('HV', size=20, required=False)
    LV = fields.Char('LV', size=20, required=False)
    Frequency = fields.Char('Frequency', size=20, required=False)
    Serial = fields.Char('Serial', size=20, required=False)
    Type = fields.Char('Type', size=20, required=False)

    MFGDate = fields.Date('MFGDate', required=False)
    Phase = fields.Char('Phase', size=20, required=False)
    Tap = fields.Char('Tap', size=20, required=False)
    TCooling = fields.Char('TCooling', size=20, required=False)
    Impedance = fields.Char('Impedance', size=20, required=False)
    ImpTemp = fields.Char('ImpTemp', size=20, required=False)
    TempRise = fields.Char('TempRise', size=20, required=False)
    WindingConf = fields.Char('WindingConf', size=20, required=False)
    Style = fields.Char('Style', size=20, required=False)
    Equip_Designation = fields.Char('Equip_Designation', size=40, required=False)
    Substation = fields.Char('Substation', size=40, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentcurentTransformer, self).create(values)

