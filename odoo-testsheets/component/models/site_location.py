# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import json

from odoo import fields, tools, api, models
from .project import model_with_image

_logger = logging.getLogger(__name__)

# Este modelo tiene un SingleLineDiagram, se crea porque El Site, Location y Sublocation tienen este atributo


class model_with_sld(models.Model):
    _name = 'component.sld'
    _description = 'model with single line diagram'
    _inherit = 'component.imaged'

    @api.one
    @api.depends('single_line_diagram')
    def _get_single_line_diagram(self):
        # result = dict.fromkeys(self.ids, False)
        # for obj in self:
        #     result[obj.id] = tools.image_get_resized_images(obj.single_line_diagram, return_medium=False, big_name='single_line_diagram', small_name='single_line_diagram_small', avoid_resize_medium=True)
        # return result
        resized_image = tools.image_get_resized_images(self.single_line_diagram, return_big=True)
        self.single_line_diagram_small = resized_image['image_small']

    @api.one
    def _set_single_line_diagram(self, value):
        # return self.write(cr, uid, [id], {'single_line_diagram': tools.image_resize_image_big(value)}, context=context)
        self.single_line_diagram = tools.image_resize_image_big(value)  # base64.b64encode(value)

    @api.one
    def _get_single_line_diagram_hotpoints(self):
        # result = dict.fromkeys(self.ids, False)
        for obj in self:
            fc_header = "{\"type\":\"FeatureCollection\",\"features\":["
            firstComponent = True
            fields = ['single_line_diagram_hotpoint','maintenance_state_color']
            if 'component_ids' in self._proper_fields:
                components = self.search_read([["id", "in", self.ids]], fields=['component_ids'])
                for component in self.env['component.component'].search_read([['id', 'in', components[0]['component_ids']]], fields=fields):
                    if component['single_line_diagram_hotpoint']:
                        if firstComponent:
                            feature = json.loads(component['single_line_diagram_hotpoint'])
                            feature['properties']['maintenance_state_color'] = component['maintenance_state_color'];
                            fc_header += json.dumps(feature)
                            firstComponent = False
                        else:
                            feature = json.loads(component['single_line_diagram_hotpoint'])
                            feature['properties']['maintenance_state_color'] = component['maintenance_state_color'];
                            fc_header += ","+json.dumps(feature)
            fc_footer = "]}"
            result = fc_header+fc_footer
        # return result
        self.single_line_diagram_hotpoints = result

    @api.one
    def _get_electric_board_hotpoints(self):
        # result = dict.fromkeys(self.ids, False)
        for obj in self:
            fc_header = "{\"type\":\"FeatureCollection\",\"features\":["
            firstComponent = True
            fields = ['electric_board_hotpoint','maintenance_state_color']
            for component in self.env['component.component'].search_read([['parent_id', '=',obj.id],['parent_model', '=',self._name]], fields=fields):
                if component['electric_board_hotpoint']:
                    if firstComponent:
                        feature = json.loads(component['electric_board_hotpoint'])
                        feature['properties']['maintenance_state_color'] = component['maintenance_state_color'];
                        fc_header += json.dumps(feature)
                        firstComponent = False
                    else:
                        feature = json.loads(component['electric_board_hotpoint'])
                        feature['properties']['maintenance_state_color'] = component['maintenance_state_color'];
                        fc_header += ","+json.dumps(feature)
            fc_footer = "]}"
            result = fc_header + fc_footer
        # return result
        self.electric_board_hotpoints = result

    single_line_diagram = fields.Binary(string="Single Line Diagram",
                               help="This field holds the image used as Single Line Diagram for the Site or Location, limited to 1024x1024px.")
    single_line_diagram_small = fields.Binary(compute='_get_single_line_diagram', inverse='_set_single_line_diagram',
                                       string="Small-sized single_line_diagram", store=True, multi="_get_single_line_diagram",
                                       help="Small-sized single_line_diagram. It is automatically " \
                                            "resized as a 64x64px image, with aspect ratio preserved. " \
                                            "Use this field anywhere a small image is required.")
    single_line_diagram_hotpoints = fields.Char(compute='_get_single_line_diagram_hotpoints', string="Single Line Diagram Hot Points", store=False,
                                                         help="This field holds the Vector features for hot zones thats represents Devices on SLD")
    electric_board_hotpoints = fields.Char(compute='_get_electric_board_hotpoints', string="Electric Board Hot Points", store=False,
                                                         help="This field holds the Vector features for hot zones thats represents Devices on Electric Board")

    @api.multi
    def write(self, vals):
        res = super(model_with_sld, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        res = super(model_with_sld, self).create(vals)
        return res


class site(models.Model):
    _name = 'component.site'
    _inherit = "component.sld"

    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': "component.site", 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', "component.site"), ('res_id', 'in', self.ids)])
        return action

    def action_get_service_reports_tree_view(self):
        for component in self:
            ctx = {
                    'default_base_site_id': component.id,
                    'default_site_address': component.siteAddress,
                    'default_site_contact_name': component.site_contacts.name,
                    'default_site_contact_phone': component.site_contacts.phone,
                    'default_customer_name':  component.project_id.company_id.name,
                    'default_customer_contact_name': component.project_id.company_contacts.name,
                    'default_customer_contact_phone': component.project_id.company_contacts.phone,
                    'default_representative_name': component.project_id.krka_contacts.name,
                    'default_representative_phone': component.project_id.krka_contacts.phone,
                 }

            return {
                'name': 'Field Service Reports View',
                'type': 'ir.actions.act_window',
                'res_model': "service_reports.order",
                'domain': str([('base_site_id', '=', component.id)]),
                'view_type': 'form',
                'view_mode': 'tree',
                'context': ctx,
            }

    @api.one
    def _get_attachment_number(self):
        component_docs = self.env['ir.attachment']
        self.attachment_number = component_docs.search_count([('res_model', '=', "component.site"), ('res_id', '=', self.ids)])

    @api.one
    def _location_count(self):
        locations = self.env['component.location']
        self.location_count = locations.search_count([('site_id',"=", self.ids)])

    @api.one
    def _service_reports_count(self):
        for component in self:
            if "service_reports.order" in self.env:
                count = self.env["service_reports.order"].search_count([('base_site_id', "=", component.id)])
            else:
                count = 0

            self.service_reports_count = count

    name = fields.Char('Site Name', required=True, help='An internal identification of the Site')
    description = fields.Text('Description', size=255, required=False, help='A description of the Site')
    project_id = fields.Many2one('component.project','Project')
    location_ids = fields.One2many('component.location', 'site_id', 'Location', help='The list of Location in this Site')
    sequence = fields.Integer('Sequence',help='Used to sort Sites', default=1)
    location_count = fields.Integer(compute='_location_count', string='Location count')
    attachment_number = fields.Integer(compute='_get_attachment_number', string="Documents Attached")
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'component.site')], string='Attachments')
    site_contacts = fields.Many2one('res.partner', 'Facilities Manager', domain=[('company_type', '=', 'person')])
    power_technicians = fields.Many2one('res.partner', 'Power Technicians', domain=[('company_type', '=', 'person')])
    site_facilities_technician = fields.Many2one('res.partner', 'Site Facilities Technician',
                                                 domain=[('company_type', '=', 'person')])
    location_code = fields.Char('Location Code #', required=False, help='Location Code')
    siteAddress = fields.Char('Site Address', required=False, help='Number and Street,')
    siteAddress1 = fields.Char(required=False, help='City and State')
    siteAddress2 = fields.Char(required=False, help='Zip and Country')
    service_reports_count = fields.Integer(compute='_service_reports_count', string='Report Services')

    # _defaults = {
    #     'sequence': 1,
    # }

    def open_site_view(self):
        if isinstance(self.ids, (list,tuple)):
            id = self.ids[0]
        else:
            id = self.ids
        return {
            'type': 'ir.actions.act_window',
            'res_model': "component.site", # this model
            'res_id': id, # the current wizard record
            'view_type': 'form',
            'view_mode': 'form,list',
            'target': 'new'}

    def set_background_color(self, cr, uid, id, background, context=None):
        self.write(cr, uid, [id], {'background_color': background}, context=context)

    @api.multi
    def write(self, vals):
        res = super(site, self).write(vals)
        return res

    @api.model
    def create(self, vals):
        res = super(site, self).create(vals)
        return res

    def unlink(self):
        locations = self.env['component.location']
        locations.search([('site_id', 'in', self.ids)]).unlink()
        res = super(site, self).unlink()
        return res


class site_location_base(models.Model):
    _name = 'component.locationbase'
    _inherit = "component.sld"

    name = fields.Char('Location Name', size=32, required=True, help='An internal identification of a Place')
    shape = fields.Selection([('square','Square'),('round','Round')],'Shape', required=True, default='square')
    position_h = fields.Float('Horizontal Position', help="The location's horizontal position from the left side to the location's center, in pixels", default=10)
    position_v = fields.Float('Vertical Position', help="The location's vertical position from the top to the location's center, in pixels", default=10)
    width = fields.Float('Width', help="The location's width in pixels", default=50)
    height = fields.Float('Height', help="The location's height in pixels", default=50)
    active = fields.Boolean('Active', help='If false, the location is deactivated and will not be available in the point of sale', default=True)

    # _defaults = {
    #     'shape': 'square',
    #     'seats': 1,
    #     'position_h': 10,
    #     'position_v': 10,
    #     'height': 50,
    #     'width':  50,
    #     'active': True,
    # }

    def action_get_components_kanban_view(self, cr, uid, ids, context=None):
        ctx = dict()
        for location in self.browse(cr, uid, ids, context):
            ctx.update({
                'default_location_id': location.id,
                'default_site_id': location.site_id.id,
            })
            return {
                'name': ('New component'),
                'view_type': 'kanban',
                'view_mode': 'kanban,list',
                'res_model': 'component.component',
                'context': ctx,
                'domain': str([('location_id', '=', location.id)]),
                'type': 'ir.actions.act_window',
            }

    def create_from_ui(self, cr, uid, location, context=None):
        if location.get('site_id', False):
            site_id = location['site_id'][0]
            location['site_id'] = site_id
        if location.get('id', False):   # Modifiy existing location
            location_id = location['id']
            del location['id']
            self.write(cr, uid, [location_id], location, context=context)
        else:
            location_id = self.create(cr, uid, location, context=context)

        return location_id

    @api.multi
    def write(self, vals):
        # self._test_image_small(vals)
        res = super(site_location_base, self).write(vals)
        return res


class site_location(models.Model):
    _name = 'component.location'
    _inherit = 'component.locationbase'

    def _component_count(self):
        res = dict.fromkeys(self.ids, 0)
        components = self.env['component.component']
        for component in components.read_group([('parent_model', '=', 'component.location'), ('parent_id', 'in', self.ids)],
                                          ['parent_id'], ['parent_id']):
            res[component['parent_id']] = components.search_count([('parent_model', '=', 'component.location'), ('parent_id', '=', component['parent_id'])])
        return res

    def open_location_view(self):
        if isinstance(self.ids, (list,tuple)):
            id = self.ids[0]
        else:
            id = self.ids
        return {
            'type': 'ir.actions.act_window',
            'res_model': "component.location", # this model
            'res_id': id, # the current wizard record
            'view_type': 'form',
            'view_mode': 'form,list',
            'target': 'new'}

    @api.one
    def _sublocation_count(self):
        sublocations = self.env['component.sublocation']
        self.sublocation_count = sublocations.search_count([('location_id',"=", self.ids)])

    @api.one
    def _get_coomponent_ids(self):
        sublocations = self.env['component.sublocation']
        for location in self:
            ids = []
            for sublocation in sublocations.search_read([('location_id',"=",location.id)], fields=["component_ids"]):
                ids.extend(sublocation['component_ids'])
        self.component_ids = ids

    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': "component.location", 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', "component.location"), ('res_id', 'in', self.ids)])
        return action

    @api.one
    def _get_attachment_number(self):
        component_docs = self.env['ir.attachment']
        self.attachment_number = component_docs.search_count([('res_model', '=', "component.location"), ('res_id', '=', self.ids)])

    site_id = fields.Many2one('component.site','Site', required=True)
    sublocation_count = fields.Integer(compute='_sublocation_count', string='Component count')
    sublocation_ids = fields.One2many('component.sublocation', 'location_id', 'Sub-locations', help='The list of sub-locations in this Location')
    description = fields.Text('Description', size=255, required=False, help='A description of the Site')
    component_ids = fields.One2many(compute='_get_coomponent_ids', string='Devices', obj="ir.model")
    component_count = fields.Integer(compute='_component_count', string='Component count')
    attachment_number = fields.Integer(compute='_get_attachment_number', string="Documents Attached" )
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'component.location')], string='Attachments')

    def unlink(self):
        sublocations = self.env['component.sublocation']
        sublocations.search([('location_id', 'in', self.ids)]).unlink()

        devices = self.env['component.component']
        devices.search([('parent_id', 'in', self.ids), ('parent_model', '=', 'component.location')]).unlink()

        res = super(site_location, self).unlink()
        return res

    @api.multi
    def write(self, vals):
        # self._test_image_small(vals)
        res = super(site_location, self).write(vals)
        return res


class site_sublocation(models.Model):
    _name = 'component.sublocation'
    _inherit = 'component.locationbase'

    @api.one
    def _component_count(self):
        components = self.env['component.component']
        for component in components.read_group([('parent_model', '=', 'component.sublocation'), ('parent_id', 'in', self.ids)],
                                               ['parent_id'], ['parent_id']):
            self.component_count = components.search_count([('parent_model', '=', 'component.sublocation'), ('parent_id', '=', component['parent_id'])])

    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': "component.sublocation", 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', "component.sublocation"), ('res_id', 'in', self.ids)])
        return action

    @api.one
    def _get_attachment_number(self):
        component_docs = self.env['ir.attachment']
        self.attachment_number = component_docs.search_count([('res_model', '=', 'component.sublocation'), ('res_id', '=', self.ids)])

    # Columnas
    location_id = fields.Many2one('component.location','Location', required=True)
    component_ids = fields.One2many('component.component', 'parent_id', domain=[('parent_model', '=', 'component.sublocation')], string='Devices')
    component_count = fields.Integer(compute='_component_count', string='Component count')
    description = fields.Text('Description', required=False, size=255, help='A description of the Site')
    attachment_number = fields.Integer(compute='_get_attachment_number', string="Documents Attached")
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'component.sublocation')], string='Attachments')

    def open_sublocation_view(self):
        if isinstance(self.ids, (list,tuple)):
            id = self.ids[0]
        else:
            id = self.ids
        return {
            'type': 'ir.actions.act_window',
            'res_model': "component.sublocation", # this model
            'res_id': id,  # the current wizard record
            'view_type': 'form',
            'view_mode': 'form,list',
            'target': 'new'}

    # def unlink(self, cr, uid, ids, context=None):
    def unlink(self):
        devices = self.env['component.component']
        devices.search([('parent_id', 'in', self.ids), ('parent_model', '=', 'component.sublocation')]).unlink()

        res = super(site_sublocation, self).unlink()
        return res
