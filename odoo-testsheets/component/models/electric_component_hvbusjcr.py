# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'hv_busjcr'
ASSET_MODEL = 'component.hv_busjcr'
ASSET_MODEL_TEST = 'component_test.hv_busjcr_test'


class ElectricComponentHVBusJCR(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')
    Equip_Designation = fields.Char('Equip_Designation', size=40, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentHVBusJCR, self).create(values)

