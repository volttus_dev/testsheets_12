from odoo import fields, models, tools, api


class model_with_image(models.Model):
    _name = 'component.imaged'
    _description = 'Model with image'

    @api.one
    @api.depends('image')
    def _get_compute_images(self):
        resized_images = tools.image_get_resized_images(self.image, return_big=True, avoid_resize_medium=True)
        self.image_medium = resized_images['image_medium']
        self.image_small = resized_images['image_small']

    image = fields.Binary("Image", help="This field holds the image used as image for the asset, limited to 1024x1024px.")
    image_medium = fields.Binary(compute='_get_compute_images',
                                 string="Medium-sized image", store=False,
                                 help="Medium-sized image of the asset. It is automatically " \
                                      "resized as a 128x128px image, with aspect ratio preserved, " \
                                      "only when the image exceeds one of those sizes. Use this field in form views or some kanban views.")
    image_small = fields.Binary(compute='_get_compute_images',
                                string="Small-sized image", store=False,
                                help="Small-sized image of the asset. It is automatically " \
                                     "resized as a 64x64px image, with aspect ratio preserved. " \
                                     "Use this field anywhere a small image is required.")


class Component_project(models.Model):
    _name = 'component.project'
    _description = 'Component Projects'
    _inherit = "component.imaged"

    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': "component.project", 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', "component.project"), ('res_id', 'in', self.ids)])
        return action

    @api.one
    def _get_attachment_number(self):
        component_docs = self.env['ir.attachment']
        self.attachment_number = component_docs.search_count(
            [('res_model', '=', "component.project"), ('res_id', '=', self.ids)])

    @api.one
    def _site_count(self):
        sites = self.env['component.site']
        self.site_count = sites.search_count([('project_id', "=", self.ids)])

    @api.multi
    def action_get_attachment_tree_view(self):
        attachment_action = self.env.ref('base.action_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': "component.project", 'default_res_id': self.ids[0]}
        action['domain'] = str(['&', ('res_model', '=', "component.project"), ('res_id', 'in', self.ids)])
        # action['target'] = 'new'
        return action

    # Columns
    name = fields.Char('Project Name', size=64, required=True)
    abstract = fields.Char('Project abstract', size=256, required=False)

    company_id = fields.Many2one('res.company', 'Client')
    company_contacts = fields.Many2one('res.partner', 'company_Contact', domain=[('company_type', '=', 'person')])
    krka_contacts = fields.Many2one('res.partner', 'krka_Contact', domain=[('company_type', '=', 'person')])

    krkacontacts = fields.Many2many('res.users', 'krka_contacts', 'id', 'partner_id', string='krkacontacts')
    procontacts = fields.Many2many('res.users', 'project_contacts', 'id', 'partner_id', string='procontacts')

    contract_no = fields.Char('Contract Nummber', size=64)
    scope_of_work = fields.Text('Scope of work', size=256, required=False, translate=False)
    site_ids = fields.One2many('component.site', 'id', 'Sites', help='The list of project sites')
    site_count = fields.Integer(compute='_site_count', string='Site count')
    attachment_number = fields.Integer(compute='_get_attachment_number', string="Documents Attached")
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'component.project')], string='Attachments')

    def open_project_view(self, cr, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            id = ids[0]
        else:
            id = ids
        return {
            'type': 'ir.actions.act_window',
            'res_model': "component.project",  # this model
            'res_id': id,  # the current wizard record
            'view_type': 'form',
            'view_mode': 'form,list',
            'target': 'new'}

    def action_get_sites_kanban_view(self, cr, uid, ids, context=None):
        ctx = dict()
        for project in self.browse(cr, uid, ids, context):
            ctx.update({
                'default_project_id': project.id,
            })
            return {
                'name': ('New project'),
                'view_type': 'kanban',
                'view_mode': 'kanban,list',
                'res_model': 'component.project',
                'context': ctx,
                'domain': str([('project_id', '=', project.id)]),
                'type': 'ir.actions.act_window',
            }

    @api.model
    def create(self, vals):
        res = super(Component_project, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        res = super(Component_project, self).write(vals)
        return res

    def unlink(self):
        sites = self.env['component.site']
        sites.search([('project_id', 'in', self.ids)]).unlink()

        res = super(Component_project, self).unlink()
        return res
