# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'automatic_transferswitch'
ASSET_MODEL = 'component.automatic_transferswitch'
ASSET_MODEL_TEST = 'component_test.automatic_transferswitch_test'


class ElectricComponentAutomaticTransferSwitch(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'Style=' + str(rec.Style) + ' ,'
            value = value +'Type=' + str(rec.Type) + ' ,'
            value = value +'Model=' + str(rec.Model) + ' ,'
            res[rec.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data. Specific fields of Automatic transfer switch ***************
    InterruptingRating = fields.Float('InterruptingRating', default=0)
    CurrentRating = fields.Float('CurrentRating', default=0)
    Serial = fields.Char('Serial', size=20, required=False)

    Style = fields.Char('Style', size=20, required=False)
    Type = fields.Char('Type', size=20, required=False)
    FrameSize = fields.Float('FrameSize', default=0)
    Model = fields.Char('Model', size=20, required=False)
    Equip_Designation = fields.Char('Equip_Designation', size=40, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):

        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentAutomaticTransferSwitch, self).create(values)

