# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'load_bs'
ASSET_MODEL = 'component.load_bs'
ASSET_MODEL_TEST = 'component_test.load_bs_test'


class ElectricComponentLoadBS(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'Type=' + str(rec.Type) + ' ,'
            value += 'Catalog No=' + str(rec.CatNo) + ' ,'
            value += 'Voltage=' + str(rec.Voltage) + ' ,'
            res[rec.ids[0]] = value
        return res

    @api.model
    def message_subscribe(self, partner_ids=None, channel_ids=None):
        return self['delegated_id'].message_subscribe(partner_ids=partner_ids, channel_ids=channel_ids)

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')
    operation = fields.Selection([('main','Main'),('tie','Tie'),('feeder','Feeder')], 'Breaker operation', default='main')

    # ************* Equipment Data. Specific fields of Dry Transformers ***************
    Current = fields.Integer('Current:', default=0)
    Type = fields.Char('Type', size=20, required=False)
    CatNo = fields.Char('Catalog No', size=20, required=False)
    Voltage = fields.Integer('Voltage:', default=0)
    StyleNo = fields.Char('Style No:', size=20, required=False)
    Equip_Designation = fields.Char('Equip_Designation:', size=40, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')

    def update_operation(self, vals):
        if ('operation' in vals):
            if (vals['operation'] != False):
                if vals['operation'] == 'main':
                    vals['color'] = 2
                elif vals['operation'] == 'tie':
                    vals['color'] = 3
                else:
                    vals['color'] = 4
        return vals

    # _defaults = {
    #     'operation': 'main',
    # }

    @api.multi
    def write(self, vals):
        # self._test_image_small(vals)
        vals = self.update_operation(vals)
        res = super(ElectricComponentLoadBS, self).write(vals)
        return res

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        values = self.update_operation(values)
        return super(ElectricComponentLoadBS, self).create(values)

