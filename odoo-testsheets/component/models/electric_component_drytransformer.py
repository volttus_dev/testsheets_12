# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'dry_transformer'
ASSET_MODEL = 'component.dry_transformer'
ASSET_MODEL_TEST = 'component_test.dry_transformer_test'


class ElectricComponentDryTransformer(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'HV=' + str(rec.HV) + ' ,'
            value = value +'LV=' + str(rec.LV) + ' ,'
            value = value +'PowerRating=' + str(rec.PowerRating) + ' ,'
            res[rec.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data. Specific fields of Dry Transformers ***************
    PowerRating = fields.Integer('PowerRating(kVA):', default=0)
    HV = fields.Integer('Hight Voltage(V):', default=0)
    LV = fields.Integer('Low Voltage(V):', default=0)
    Frequency = fields.Integer('Frequency(Hz):', default=0)
    Serial = fields.Char('Serial No.:', size=20, required=False)
    Type = fields.Char('Type:', size=20, required=False)

    MFGDate = fields.Date('MFGDate:', required=False)
    Phase = fields.Char('Phase:', size=20, required=False)
    Tap = fields.Char('Tap:', size=20, required=False)
    TCooling = fields.Char('Type of Cooling:', size=20, required=False)
    Impedance = fields.Float('Impedance(%):', default=0)
    ImpTemp = fields.Float(' at(°C):', default=0)
    TempRise = fields.Float('TempRise(°C)', default=0)
    WindingConf = fields.Char('WindingConf', size=20, required=False)
    Style = fields.Char('Style', size=20, required=False)
    component_info = fields.Char(compute='_component_info', string='Information')
    Equip_Designation = fields.Char('Equip_Designation:', size=40, required=False)

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentDryTransformer, self).create(values)

