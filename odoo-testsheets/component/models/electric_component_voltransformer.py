# -*- coding: utf-8 -*-
from odoo import api
from .electric_component import electric_component_base
from odoo import fields, models

ASSET_TYPE = 'vol_transformer'
ASSET_MODEL = 'component.vol_transformer'
ASSET_MODEL_TEST = 'component_test.vol_transformer_test'


class ElectricComponentVolTransformer(electric_component_base):
    _name = ASSET_MODEL

    _inherits = {
        'component.component': 'delegated_id',
    }

    def _component_info(self):
        res = dict.fromkeys(self.ids, "")
        for rec in self:
            value = 'Type=' + str(rec.Type) + ' ,'
            value = value +'Ratio=' + str(rec.Ratio) + ' ,'
            value = value +'Class=' + str(rec.Class) + ' ,'
            res[rec.ids[0]] = value
        return res

    # Columns
    delegated_id = fields.Many2one('component.component', required=True, ondelete='cascade')

    # ************* Equipment Data.  ***************
    Style = fields.Char('Style/Cat. number:', size=20, required=False)
    Ratio = fields.Char('Ratio:', size=20, required=False)
    Type = fields.Char('Type:', size=20, required=False)
    Class = fields.Char('Class:', size=20, required=False)
    Accuracy = fields.Char('Accuracy Class:', size=20, required=False)
    VARating = fields.Integer('VA Rating(VA):', default=0)
    PrVolt = fields.Integer('Primary Voltage(V):', default=0)
    Equip_Designation = fields.Char('Equip_Designation', size=40, required=False)
    Substation = fields.Char('Substation', size=40, required=False)

    # ****************Fuse Data**************************************
    FuseManuf = fields.Char('Manufacturer:', size=20, required=False)
    FuseType = fields.Char('Fuse Type:', size=20, required=False)
    FuseClass = fields.Char('Class:', size=20, required=False)
    FuseVol = fields.Integer('Voltage(V):', default=0)
    FuseAmp = fields.Integer('Amps(A):', default=0)
    FuseIntRat = fields.Char('Inter. Rating:', size=20, required=False)
    FuseBil = fields.Char('Bil:', size=20, required=False)

    component_info = fields.Char(compute='_component_info', string='Information')

    @api.model
    def create(self, values):
        if not values.get('electric_component_type') and 'electric_component_type' not in self._context:
            values['electric_component_type'] = ASSET_TYPE
            values['component_model'] = ASSET_MODEL
            values['component_model_test'] = ASSET_MODEL_TEST
        return super(ElectricComponentVolTransformer, self).create(values)

