odoo.define('component_explorer.session', function (require) {
    'use strict';

    var core = require('web.core');
    var session = require('web.Session');
    var voltus_ajax = require('component_explorer.ajax');

    var FieldBinaryFile = require('web.basic_fields').FieldBinaryFile;
    var contentdisposition = require('web.contentdisposition');
    var framework = require('web.framework');
    var crash_manager = require('web.crash_manager');
    var utils = require('web.utils');



    session.include({
      voltus_get_file: function (options) {
        if (this.override_session){
            options.data.session_id = this.session_id;
        }
        options.session = this;
        return voltus_ajax.get_file(options);
      },
    });

    FieldBinaryFile.include({
       on_save_as: function (ev) {
        if (!this.value) {
            this.do_warn(_t("Save As..."), _t("The field is empty, there's nothing to save !"));
            ev.stopPropagation();
        } else if (this.res_id) {
            framework.blockUI();
            var c = crash_manager;
            var filename_fieldname = this.attrs.filename;
            this.getSession().voltus_get_file({
                'url': '/web/content',
                'data': {
                    'model': this.model,
                    'id': this.res_id,
                    'field': this.name,
                    'filename_field': filename_fieldname,
                    'filename': this.recordData[filename_fieldname] || "",
                    'download': true,
                    'data': utils.is_bin_size(this.value) ? null : this.value,
                },
                'complete': framework.unblockUI,
                'error': c.rpc_error.bind(c),
            });
            ev.stopPropagation();
        }
    },
    });

});