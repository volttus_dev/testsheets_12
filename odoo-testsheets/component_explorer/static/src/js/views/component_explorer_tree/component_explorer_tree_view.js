odoo.define('component_explorer.ComponentExplorerTreeView', function (require) {
    'use strict';
    var  AbstractView = require('web.AbstractView');
    var  view_registry = require('web.view_registry');
    var  ComponentExplorerTreeController = require('component_explorer.ComponentExplorerTreeController');
    var  ComponentExplorerTreeModel = require('component_explorer.ComponentExplorerTreeModel');
    var  ComponentExplorerTreeRenderer = require('component_explorer.ComponentExplorerTreeRenderer');
    var  ComponentExplorerTreeView = AbstractView.extend({
        display_name: 'Component Explorer',
        icon: 'fa-list-ul',
        config: {
            Model: ComponentExplorerTreeModel,
            Controller: ComponentExplorerTreeController,
            Renderer: ComponentExplorerTreeRenderer,
        },
        viewType: 'component_explorer_tree',

        init: function (viewInfo, params) {
            this._super.apply(this, arguments);
        },
    });

    view_registry.add('component_explorer_tree', ComponentExplorerTreeView);
    return ComponentExplorerTreeView;
});
