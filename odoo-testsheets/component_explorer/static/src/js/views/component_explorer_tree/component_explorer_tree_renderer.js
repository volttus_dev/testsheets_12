odoo.define('component_explorer.ComponentExplorerTreeRenderer', function (require) {
    'use strict';

    var AbstractRenderer = require('web.AbstractRenderer');
    var BasicRenderer = require('web.BasicRenderer');
    var core = require('web.core');
    var qweb = core.qweb;
    var widgets = require('component_explorer.fancy_tree_widget');
    var tree_widgets = require('component_explorer.tree_view_widgets');
    var CExplorerPropertiesFormView = require('web.FormView');
    var CExplorerFormView = require('web.FormView');
    var dialogs = require('web.view_dialogs');
    var dialog = require('web.Dialog');
    var _t = core._t;

    var STATE_COLOR_SELECTION = new Array();
    STATE_COLOR_SELECTION[0] = 'red';
    STATE_COLOR_SELECTION[1] = 'Chartreuse';
    STATE_COLOR_SELECTION[2] = 'blue';
    STATE_COLOR_SELECTION[3] = 'yellow';
    STATE_COLOR_SELECTION[4] = 'magenta';
    STATE_COLOR_SELECTION[5] = 'cyan';
    STATE_COLOR_SELECTION[6] = 'black';
    STATE_COLOR_SELECTION[7] = 'white';
    STATE_COLOR_SELECTION[8] = 'orange';
    STATE_COLOR_SELECTION[9] = 'skyblue';

    var featureStyles = STATE_COLOR_SELECTION.map(function (color) {
        return Object.assign({}, OpenLayers.Feature.Vector.style.default, {strokeColor: color[1]});
    });

    var ComponentExplorerTreeRenderer = AbstractRenderer.extend({
        className: 'component_explorer_render_container',
        events: _.extend({}, BasicRenderer.prototype.events, {
          'click .open_component_view': '_onOpenComponentView',
          'click .open_project_view': '_onOpenProjectView',
          'click .open_site_view': '_onOpenSiteView',
          'click .open_location_view': '_onOpenLocationView',
          'click .open_sublocation_view': '_onOpenSubLocationView',
          'click .open_service_reports': '_onOpenServiceReports',
          'click .delete_project': '_onDeleteProject',
          'click .delete_site': '_onDeleteSite',
          'click .delete_location': '_onDeleteLocation',
          'click .delete_sublocation': '_onDeleteSublocation',
          'click .delete_device': '_onDeleteDevice',
          'click .sld_page' : '_onSldPageClick',
        }),
        custom_events: _.extend({}, BasicRenderer.prototype.custom_events, {
            env_updated: '_onEnvUpdated',
            //'navigation_move':'_onNavigationMove',
            //'activate_next_widget' : '_onActivateNextWidget',
            reload_component_view: '_onReloadComponentView',
            reload_tree: '_onReloadTree',
            reload: '_onReload',
        }),

        _onSldPageClick: function(event){
          //this.popupClear();
        },

        _onReload: function(event) {
          alert('wwwwwwwwww');
        },

        _onReloadTree: function(event){
          this.tree.reload_content();
        },

        _onReloadComponentView: function(event){
          this.reload_component_view();
        },

        reload_component_view: function(){
            if (this.current_component_view_model && this.current_component_view_id){
                this.show_component_view(this.current_component_view_model, this.current_component_view_id);
                this.show_sld_view(this.current_component_view_model, this.current_component_view_id, true);
            }
        },


        init: function (parent, state, params) {
            this._super.apply(this, arguments);

            this.tree = null;
            this._domain = [];

            this.component_types = [];
            this.component_types.push({title: 'Cable', model: "component.lv_ac_cable"});
            this.component_types.push({title: 'Dry Transformer', model: "component.dry_transformer"});
            this.component_types.push({title: 'Liquid Transformer', model: "component.liquid_transformer"});
            this.component_types.push({title: 'Current Transformer', model: "component.curent_transformer"});
            this.component_types.push({title: 'Voltage Transformer', model: "component.vol_transformer"});
            this.component_types.push({title: 'AC LV Circuit breaker', model: "component.lv_cb"});
            this.component_types.push({title: 'DC LV Circuit breaker', model: "component.lvdc_cb"});
            this.component_types.push({title: 'AC HV Circuit breaker', model: "component.hvac_circuitbreaker"});
            this.component_types.push({title: 'Load Break Switch', model: "component.load_bs"});
            this.component_types.push({title: 'Molded Case Breaker(with relay)', model: "component.mcb_withrelay"});
            this.component_types.push({title: 'Automatic Transfer Switch', model: "component.automatic_transferswitch"});
            this.component_types.push({title: 'LV Bus', model: "component.lv_ac_bus"});
            this.component_types.push({title: 'HV Bus', model: "component.hv_ac_bus"});
        },

        updateState: function (state, params) {
            this._domain = params.domain;
            if (params.domain.length > 0){
                var self = this;
                return this._rpc({
                   model: 'component.component',
                   method: 'search_read',
                   domain: params.domain,
                }).then(function(components){
                  if (components.length > 0){
                        var locations = [];
                        var sublocations = [];
                        for(var i= 0; i < components.length; i++){
                            sublocations.push(components[i].parent_id);
                        }

                        self.load_treeview(locations, sublocations, params.domain);
                        window.setTimeout(function () {
                          var first_component = components[0];
                          self.show_component_layout(first_component);
                        }, 300);
                   }
                });
            }else{
              this._domain = [];
              this.state = state;
              return params.noRender ? $.when() : this._render();
            }
         },


         show_component_layout: function(component){
            var self = this;
            this._rpc({
                   model: 'component.component',
                   method: 'search_read',
                   domain: [['id', '=', component.id]],
                }).then(function (cmp) {
                    var sublocation_id = component.parent_id;
                    self._rpc({
                         model: 'component.sublocation',
                         method: 'search_read',
                         domain: [['id', '=', sublocation_id]],
                       }).then(function (sublocation) {
                           var location_id = sublocation[0].location_id[0];
                           self._rpc({
                                model: 'component.location',
                                method: 'search_read',
                                domain: [['id', '=', location_id]],
                              }).then(function (location) {
                                 if (cmp[0]["electric_board_hotpoint"]){
                                   self.tree.activate_sublocation_node(sublocation_id);
                                   self.select_component_in_layout(component.id);
                                   self.$('.sld_page').click();
                                 }else if (cmp[0]["single_line_diagram_hotpoint"]){
                                   self.tree.activate_location_node(location_id);
                                   self.select_component_in_layout(component.id);
                                   self.$('.sld_page').click();
                                 }
                           });
                        });
             });
        },

        select_component_in_layout(id){
          var adomain = [["id","=",parseInt(id)]]
          var self = this;
          self._rpc({
              model: 'component.component',
              method: 'search_read',
              domain: adomain,
          }).then(function(component){
              var content = qweb.render('ComponentKanbanView', {record: component[0], parent: component[0].parent_model});
              var geojson_format = new OpenLayers.Format.GeoJSON();
              var feature = null;

              if (component[0]["electric_board_hotpoint"]){
                feature = geojson_format.read(component[0]["electric_board_hotpoint"], "Feature");
              }else if (component[0]["single_line_diagram_hotpoint"]){
                feature = geojson_format.read(component[0]["single_line_diagram_hotpoint"], "Feature");
              }

              if (feature){
                  var centroid = feature.geometry.getCentroid();
                  var popup = new OpenLayers.Popup.FramedCloud(
                      'component.component',
                      new OpenLayers.LonLat(centroid.x, centroid.y),
                      new OpenLayers.Size(200,50),
                      content,
                      null,
                      true
                  );

                  self.map.addPopup(popup);
                  self.process_popup(component, popup, feature);
              }
          });
        },

        _render: function () {
            this.defs = [];
            var self = this;
            this.$el.empty();
            this.$el.append(qweb.render('ComponentExplorerTreeView', {}));

            //Render FancyTree
            this.load_treeview(null, null, null);
            return this._super.apply(this, arguments);
        },

        _onEnvUpdated: function (event) {
            event.stopPropagation();
        },

        _onOpenServiceReports: function(ev){
           ev.preventDefault();
            ev.stopPropagation();
            var site_id = this.current_sld_id;
            var self = this;
            this._rpc({
               model: 'component.site',
               method: 'action_get_service_reports_tree_view',
               args: [Number(site_id)]
            }).then(function(action){
                action.views = [[false, 'list']];
                self.do_action(action);
            });
        },

        /********** Kanban Events ***********/

        _onOpenComponentView: function(ev){
          ev.preventDefault();
          ev.stopImmediatePropagation();
          var id = ev.currentTarget.getAttribute('data-id');
          var model = ev.currentTarget.getAttribute('data-model');
          this.show_component_detail_view(model, Number(id));
        },

        show_component_detail_view: function(model, id){
            this.cdetail_panel_parent = this.$(".o_cexplorer_cdetail");
            this.cdetail_panel_parent.children().remove();
            var view_fields = ['id','name','type'];
            var domain = [['model','=',model], ['type', '=', 'form']];

            var self = this;
            this._rpc({
               model: 'ir.ui.view',
               method: 'search_read',
               domain: domain,
               limit: 1,
            }).then(function(view){
              self._rpc({
               model: model,
               method: 'search_read',
               domain: [['delegated_id', '=', id]],
            }).then(function(current_record){
               self._rpc({
                   model: view[0].model,
                   method: 'fields_view_get',
                   kwargs:{
                     view_id: view[0].id,
                     view_type: 'form',
                   }
                }).then(function(info){
                    var viewInfo = info;

                    var params = {
                           context: {},
                           currentId: Number(current_record[0].id),
                           mode: 'readonly',
                           modelName: view[0].model,
                           default_buttons: true,
                    };

                    var form_view = new CExplorerFormView(viewInfo, params);
                    form_view.getController(self).then(function (controller) {
                        self.$('.detail_page').show();
                        self.$('.detail_page').click();
                        //self.cdetail_panel_parent.detach();

                        self.$('.o_cexplorer_cdetail').children().remove();
                        controller.appendTo(self.cdetail_panel_parent);

                        self.$('.o_cexplorer-cdetail-buttons').children().remove();
                        controller.renderButtons(self.$('.o_cexplorer-cdetail-buttons'));
                        controller.electric_component = true;
                    });
                });
              })
            });
        },

        _onOpenProjectView(ev){
          ev.preventDefault();
          ev.stopPropagation();
          var id = ev.currentTarget.getAttribute('data-id');
          this.show_site_view(id);
          this.show_property_view('component.project', id);
        },

        _onOpenSiteView(ev){
           ev.preventDefault();
           ev.stopPropagation();
           var id = ev.currentTarget.getAttribute('data-id');
           this.show_location_view(id);
           this.show_property_view('component.site', id);
        },

        _onOpenLocationView(ev){
           ev.preventDefault();
           ev.stopPropagation();
           var id = ev.currentTarget.getAttribute('data-id');
           this.show_sublocation_view(id);
           this.show_property_view('component.location', id);
        },

        _onOpenSubLocationView(ev){
           ev.preventDefault();
           ev.stopPropagation();
           var id = ev.currentTarget.getAttribute('data-id');
           this.show_component_view('component.sublocation', id);
           this.show_property_view('component.sublocation', id);
        },

        _onDeleteComponent: function(ev, model){
           ev.preventDefault();
           ev.stopPropagation();

           var id = ev.currentTarget.getAttribute('data-id');
           if (this.tree){
              var key = model.split('\.')[1]+"_"+id;
                    this.tree.delete_selected(key);
           }
        },

        _onDeleteProject: function(ev){
           var model = 'component.project';
           this._onDeleteComponent(ev, model);
        },

        _onDeleteSite: function(ev){
           var model = 'component.site';
           this._onDeleteComponent(ev, model);
        },

        _onDeleteLocation: function(ev){
           var model = 'component.location';
           this._onDeleteComponent(ev, model);
        },

        _onDeleteSublocation: function(ev){
           var model = 'component.sublocation';
           this._onDeleteComponent(ev, model);
        },

        _onDeleteDevices: function(ev){
           var model = 'component.component';
           this._onDeleteComponent(ev, model);
        },

        /********** End Kanban Events ***********/

        load_treeview: function (locations, sublocations, domain) {
            if ((this.$("#tree")) != undefined) {
                this.$("#tree").remove();
            }
            this.tree = new widgets.ExplorerTreeView(this, locations, sublocations, domain);
            this.tree.appendTo(this.$('.o_cexplorer_sidebar_container'));
        },


        select_component_model: function (do_it) {
            var buttons = [
                {
                    text: _t("Ok"),
                    classes: 'btn-primary',
                    close: true,
                    click: do_it
                },
                {
                    text: _t("Cancel"),
                    close: true,
                    click: options && options.cancel_callback
                }
            ];
            var options = {
                size: 'medium',
                buttons: buttons,
                title: _t("Select Component Type"),
                $content: qweb.render('ComponentTypeSelectDialog', {component_types: this.component_types}),
            };
            var component_dialog = new dialog(this, _.extend(options)).open();
            var self = this;
            component_dialog.$el.find('input').click(function() {
                var value = $(this).attr('value');
                self.selected_component_model = value;
            });
            return component_dialog;
        },


        add_record: function(model, context) {
            if (model){
                this.current_model = model;
            }
            if (this.current_model == "component.component") {
                var parent = context['default_parent_id'];
                var component_fields = ['id', 'parent_id', 'parent_model'];
                var domain = [["parent_id","=",parent],["parent_model","=","component.sublocation"]];
                var self = this;
                this._rpc({
                    model: 'component.component',
                    method: 'search_read',
                    fields: component_fields,
                    domain: domain,
                }).then(function (components) {
                    if (components.length > 0){
                        self.select_component_model(function (dialog) {
                            var popo = self.select_component_model;
                            self.do_switch_view(self.selected_component_model, context);
                        });
                    }else{
                        self.do_switch_view("component.board", context);
                    }
                });
            }else {
                this.do_switch_view(model, context);
            }
        },

        get_parent_model: function (model) {
            if (model == 'component.site'){
                return 'component.project';
            }else if (model == 'component.location'){
                return 'component.site';
            }else if (model == 'component.sublocation'){
                return 'component.location';
            }
            return null;
        },

        do_switch_view: function(model, context){
            var self = this;
            var view_fields = ['id','name','type'];
            var domain = [['model','=',model], ['type','=','form']];
            this._rpc({
                    model: 'ir.ui.view',
                    method: 'search_read',
                    fields: this.view_fields,
                    args: this.domain,
                    limit: 1,
            }).then(function (view) {
                var def = $.Deferred();
                if (self.prev_form_dialog){
                    self.prev_form_dialog.destroy();
                }
                self.prev_form_dialog = new dialogs.FormViewDialog(self, {
                    res_model: model,
                    context: context,
                    title: "New..",
                    view_id: view.id,
                    res_id: context["id"],
                    readonly: false,
                    on_saved: function (record, changed) {
                        if (changed) {
                            self.prev_form_dialog.close();
                                var id = -1;
                                var parent_model = self.get_parent_model(model);
                                if (context['default_parent_id']){
                                    id = context['default_parent_id'];
                                    parent_model = context['default_parent_model'];
                                }else if (context['default_site_id']){
                                    id = context['default_site_id'];
                                }else if (context['default_location_id']){
                                    id = context['default_location_id'];
                                }else if (context['default_project_id']){
                                    id = context['default_project_id'];
                                }
                                $.when(self.tree.reload_content()).done(function () {
                                    if (id != -1){
                                        self.tree.activate_node_by_model(parent_model, id);
                                    }else{
                                        self.show_project_view();
                                        self.tree.activate_node_by_key('root');
                                    }
                                });
                        }
                    },
                }).open();

                self.prev_form_dialog.on('closed', self, function () {
                    def.resolve();
                });
            });
        },

        update_view: function (model_predicate, id, domain) {
            var full_model = 'component.'+ model_predicate;
            if (model_predicate == 'project') {
                this.show_site_view(id);
                this.show_property_view(full_model, id);
                this.hide_sld_view();
            } else if (model_predicate == 'site') {
                this.show_location_view(id);
                this.show_property_view(full_model, id);
                this.show_sld_view(full_model, id, true);
            } else if (model_predicate == 'location') {
                //this.show_location_mixed_view(id, domain);
                this.show_sublocation_view(id);
                this.show_property_view(full_model, id);
                this.show_sld_view(full_model, id, true);
            } else if (model_predicate == 'sublocation') {
                this.show_component_view(id);
                this.show_property_view(full_model, id);
                this.show_sld_view(full_model, id, true);
            }

            this.$('.cexplorer_page').click();
            this.hide_component_detail_view();
        },

        show_project_view: function () {
            this.hide_component_detail_view();
            this.right_panel_parent = this.$(".o_cexplorer_view");
            this.$(".o_cexplorer_view").children().remove();
            var project_view = new tree_widgets.ProjectTreeView(this);
            this.explorer_view = project_view;
            project_view.appendTo(this.right_panel_parent);
        },

        show_site_view: function (project) {
            this.right_panel_parent = this.$(".o_cexplorer_view");
            this.$(".o_cexplorer_view").children().remove();
            var site_view = new tree_widgets.SiteTreeView(this, project);
            this.explorer_view = site_view;
            site_view.appendTo(this.right_panel_parent);
        },

        show_location_view: function (site) {
            this.right_panel_parent = this.$(".o_cexplorer_view");
            //this.remove_previous_view();
            this.$(".o_cexplorer_view").children().remove();
            var location_view = new tree_widgets.LocationTreeView(this, site);
            this.explorer_view = location_view;
            location_view.appendTo(this.right_panel_parent);
        },

        show_sublocation_view: function (location) {
            this.right_panel_parent = this.$(".o_cexplorer_view");
            //this.remove_previous_view();
            this.$(".o_cexplorer_view").children().remove();
            var sublocation_view = new tree_widgets.SubLocationTreeView(this, location);
            this.explorer_view = sublocation_view;
            sublocation_view.appendTo(this.right_panel_parent);
        },

        show_component_view: function (component) {
            //this.current_component_view_model = model;
            //this.current_component_view_id = id;

            this.right_panel_parent = this.$(".o_cexplorer_view");
            this.$(".o_cexplorer_view").children().remove();
            var component_view_parent = this.right_panel_parent;
            var component_view = new tree_widgets.ComponentTreeView(this, component, this._domain);
            this.explorer_view = component_view;
            component_view.appendTo(this.right_panel_parent);
        },

        popupClear : function () {
            while( this.map.popups.length ) {
                 this.map.removePopup(this.map.popups[0]);
            }
        },

        show_sld_view: function (model, id, activatePage) {
            var self = this;
            this.current_sld_model = model;
            this.current_sld_id = id;
            this.$("#map").detach();
            if (activatePage){
                this.$(".o_cexplorer_sld").show();
                this.$(".sld_page").css( "visibility", "visible" );
            }
            var sld_element = document.getElementsByClassName("o_cexplorer_sld").item(0);
            var w = $('.tab-content').width();
            var h = $(window).height()-$('.oe-control-panel').height();
            var map_div = "<div id='map' style='width: "+w+"px; height: "+h+"px' />";
            $(window).resize(function () {
                var w = $('.tab-content').width();
                var h = $(window).height()-$('.oe-control-panel').height();
                $('#map').width(w);
                $('#map').height(h);
            });
            // var map_div = "<div id='map' style='width: 600px; height: 400px' />";
            this.$(".o_cexplorer_sld").append(map_div);
            OpenLayers.ImgPath = "/component_explorer/static/lib/OpenLayers/img/";
            var bounds = new OpenLayers.Bounds(-180, -90, 180, 90);
            var options = {
                projection: "EPSG:4326",
                restrictedExtent: bounds,
                center: bounds.getCenterLonLat(),
                controls: [
                    new OpenLayers.Control.PanZoom(),
                ],
                //zoom: 5,
                fractionalZoom: true,
                resolutions: [0.222473144531, 0.148315429688, 0.098876953125, 0.06591796875, 0.04394531250],
            };
            this.map = new OpenLayers.Map("map", options);
            var base_layer = new OpenLayers.Layer.Image(
                "Base",
                "/component_explorer/static/bg_layer.png",
                bounds,
                new OpenLayers.Size(2048, 1024),
                {}
            );
            self.map.addLayer(base_layer);
            var domain = [["id","=",Number(id)]];
            var hotpointsField = (this.current_sld_model == "component.location")?'single_line_diagram_hotpoints':'electric_board_hotpoints';
            var componentHotpointsField = (this.current_sld_model == "component.location")?'single_line_diagram_hotpoint':'electric_board_hotpoint';
            this._rpc({
               model: model,
               method: 'search_read',
               fields: ['id','single_line_diagram', hotpointsField ,'name'],
               domain: domain,
               limit: 1,
            }).done(function(result){
                //model.query(['id','single_line_diagram', hotpointsField ,'name']).first().done(function (record) {
                var record = result[0];
                if (record){
                    if (record['single_line_diagram'] != false){
                        var src = "/web/image?model="+model+"&id="+Number(id)+"&field=single_line_diagram";
                        var src = "data:image/png;base64,"+ record['single_line_diagram'];
                        var img = new Image();
                        img.src = src;
                        img.onload = function() {
                            var img_bounds = self.fix_bounds(bounds, img.width, img.height);
                            self.devLayer = new OpenLayers.Layer.Vector( "Devices",{silent: false});
                            var imageLayer = new OpenLayers.Layer.Image(
                                "Single Line Diagram",
                                src,
                                img_bounds,
                                new OpenLayers.Size(img.width, img.height),
                                {isBaseLayer: false, alwaysInRange: true}
                            );

                            self.map.addLayers([imageLayer, self.devLayer]);
                            //self.map.zoomToMaxExtent();
                            self.map.setCenter(imageLayer, 2);

                            if (record[hotpointsField] != false){
                                var geojson_format = new OpenLayers.Format.GeoJSON();
                                var features = geojson_format.read(record[hotpointsField], "FeatureCollection");
                                var styledFeatures = features.map(function(feature){
                                    feature.style = Object.assign({}, OpenLayers.Feature.Vector.style.default, {strokeColor: STATE_COLOR_SELECTION[feature.attributes['maintenance_state_color']]});
                                    return feature;
                                });
                                self.devLayer.addFeatures(styledFeatures);
                            }
                            self._rpc({
                              model: model,
                              method: 'check_access_rights',
                              args: ["create", false],
                            }).then(function (create_right) {
                                    self.toolBar = new OpenLayers.Control.ComponentToolbar(self.devLayer, {
                                        is_admin: create_right == true
                                    });
                                    self.toolBar.selectionControl.onSelect = function (feature) {
                                        self._rpc({
                                          model: 'component.component',
                                          method: 'search_read',
                                          domain: [["id","=",parseInt(feature.attributes["device_id"])]],
                                          limit: 1,
                                        }).then(function(component){
                                            var content = qweb.render('ComponentKanbanView', {record: component[0], parent: self.current_sld_model});
                                            var centroid = feature.geometry.getCentroid();
                                            var popup = new OpenLayers.Popup.FramedCloud(
                                                feature.attributes["electric_model"],
                                                new OpenLayers.LonLat(centroid.x, centroid.y),
                                                new OpenLayers.Size(200,50),
                                                content,
                                                null,
                                                true
                                            );
                                            self.map.addPopup(popup);
                                            self.toolBar.selectionControl.unselect(feature);
                                            self.process_popup(component[0], popup, feature);
                                        });
                                    };
                                    self.map.addControls([self.toolBar]);
                                    self.toolBar.deleteDeviceControl.deactivate();
                                    self.toolBar.addDeviceControl.deactivate();
                                    self.toolBar.navigationControl.deactivate();
                                    self.toolBar.selectionControl.activate();
                            });

                            self.devLayer.events.register('sketchcomplete', self.devLayer, function(evt) {
                                self.selected_component = evt.feature;
                                var value = self.select_component_for_map(model, id, function (dialog) {
                                    var geojson_format = new OpenLayers.Format.GeoJSON();
                                    var geojson = geojson_format.write(self.selected_component);
                                    var intId = parseInt(self.selected_component.attributes['device_id']);
                                    var objData = {};
                                    objData[componentHotpointsField] = geojson;

                                    self._rpc({
                                       model: 'component.component',
                                       method: 'write',
                                       args:[[intId], objData],
                                    }).then(function(){
                                        self.reload_sld_hotspots();
                                    });
                                });
                                return false;
                            });

                            self.devLayer.events.register('featureremoved', self.devLayer, function(evt) {
                                var intId = parseInt(evt.feature.attributes['device_id']);
                                var objData = {};
                                objData[componentHotpointsField] = null;

                                self._rpc({
                                       model: 'component.component',
                                       method: 'write',
                                       args:[[intId], objData],
                                }).then(function(){
                                        self.reload_sld_hotspots();
                                });
                            });
                        }
                    }else{
                        self.hide_sld_view();
                    }
                }else{
                    self.hide_sld_view();
                }
            });
        },

        select_component_test_model: function (component_model, do_it) {
            var test_types = [];
            if (component_model == "component.lvdc_cb"){
                test_types.push({title: 'LV DC Circuit Breaker Test', model: "component_test.lvdc_cb_test"});
            }else if (component_model == "component.lv_cb"){
                test_types.push({title: 'LV AC Circuit Breaker Test', model: "component_test.lv_cb_test"});
            }else if (component_model == "component.hvac_circuitbreaker"){
                test_types.push({title: 'HV AC Circuit Breaker Test', model: "component_test.hvac_circuitbreaker_test"});
            }else if (component_model == "component.lv_ac_cable"){
                test_types.push({title: 'HV AC cable Test', model: "component_test.hv_ac_cable_test"});
                test_types.push({title: 'LV AC cable Test', model: "component_test.lv_ac_cable_test"});
            }else if (component_model == "component.dry_transformer"){
                test_types.push({title: 'Dry Transformer Test', model: "component_test.dry_transformer_test"});
            }else if (component_model == "component.liquid_transformer"){
                test_types.push({title: 'Liquid Transformer Test', model: "component_test.liquid_transformer_test"});
            }else if (component_model == "component.curent_transformer"){
                test_types.push({title: 'Current Transformer Test', model: "component_test.curent_transformer_test"});
            }else if (component_model == "component.vol_transformer"){
                test_types.push({title: 'Voltaje Transformer Test', model: "component_test.vol_transformer_test"});
            }else if (component_model == "component.load_bs"){
                test_types.push({title: 'Load break Switch Test', model: "component_test.load_bs_test"});
            }else if (component_model == "component.automatic_transferswitch"){
                test_types.push({title: 'Automatic Transfer Switch Test', model: "component_test.automatic_transferswitch_test"});
            }else if (component_model == "component.lv_ac_bus"){
                test_types.push({title: 'Low Voltage Bus Test', model: "component_test.lv_ac_bus_test"});
            }else if (component_model == "component.hv_ac_bus"){
                test_types.push({title: 'Higth Voltage Bus Test', model: "component_test.hv_ac_bus_test"});
            }else if (component_model == "component.surge_arrestor"){
                test_types.push({title: 'Surge Arrestor Test', model: "component_test.surge_arrestor_test"});
            }else if (component_model == "component.mcb_withrelay"){
                test_types.push({title: 'Molded Case Break Test', model: "component_test.mcb_withrelay_test"});
            }else if (component_model == "component.board"){
                test_types.push({title: 'Board', model: "component_test.board_test"});
            }

            /*/*Continuar par todos los tipos de componentes y sus tests*/

            var buttons = [
                {
                    text: _t("Ok"),
                    classes: 'btn-primary',
                    close: true,
                    click: do_it
                },
                {
                    text: _t("Cancel"),
                    close: true,
                    click: options && options.cancel_callback
                }
            ];
            var options = {
                size: 'medium',
                buttons: buttons,
                title: _t("Select Test Type"),
                $content: qweb.render('TestTypeSelectDialog', {test_types: test_types}),
            };
            var test_dialog = new dialog(this, _.extend(options)).open();
            var self = this;
            test_dialog.$el.find('input').click(function() {
                var value = $(this).attr('value');
                self.selected_test_model = value;
            });
            return test_dialog;
        },

        process_popup: function (component, popup, feature) {
           var self = this;

           self._rpc({
                   model: 'res.users',
                   method: 'has_group',
                   args: ['component.group_component_component_manager'],
           }).then(function(is_admin) {
                if (!is_admin) {
                    self._rpc({
                       model: 'res.users',
                       method: 'has_group',
                       args: ['component.group_component_component_user'],
                    }).then(function(is_user) {
                         if (!is_user){
                            self.$('.new_report_map').hide();
                         }
                    });
                }
            });

            self.$(".open_component_view").click(function(ev){
               self.map.removePopup(popup);
            });

            this.$('.to_sublocation_map').click(function(ev){
                ev.preventDefault();
                ev.stopPropagation();
                var electric_model = "component."+feature.attributes["electric_model"];
                var domain = [["delegated_id","=",parseInt(feature.attributes["device_id"])]]

                self._rpc({
                   model: electric_model,
                   method: 'search_read',
                   fields: ["id", "parent_id", "electric_board_hotpoint"],
                   domain: domain,
                   limit: 1,
                }).then(function(component){
                  if (component.length > 0){
                    if (component[0]["electric_board_hotpoint"] == false){
                        dialog.alert(this, _t("Component has not been located in Electric Board."));
                    }else {
                        self.tree.activate_node_by_key("sublocation_" + component[0].parent_id);
                        var geojson_format = new OpenLayers.Format.GeoJSON();
                        var afeature = geojson_format.read(component[0]["electric_board_hotpoint"], "Feature");
                        window.setTimeout(function () {
                            self.$('.sld_page').click();
                            self.map.zoomToExtent(afeature.geometry.getBounds());
                            self.map.removePopup(popup);
                        }, 1000);
                    }
                  }
                });

            });

            this.$('.to_location_map').click(function(ev){
                ev.preventDefault();
                ev.stopPropagation();

                var electric_model = "component."+feature.attributes["electric_model"];
                var domain = [["delegated_id","=",parseInt(feature.attributes["device_id"])]]

                self._rpc({
                   model: electric_model,
                   method: 'search_read',
                   fields: ["id", "parent_id", "single_line_diagram_hotpoint"],
                   domain: domain,
                   limit: 1,
                }).then(function(component){
                 if (component.length > 0){
                    if (component[0]["single_line_diagram_hotpoint"] == false){
                        dialog.alert(this, _t("Component has not been located in SLD."));
                    }else{
                        var parentKey = self.tree.get_tree().getActiveNode().getParent().key;
                        self.tree.activate_node_by_key(parentKey);
                        var geojson_format = new OpenLayers.Format.GeoJSON();
                        var afeature = geojson_format.read(component[0]["single_line_diagram_hotpoint"], "Feature");
                        window.setTimeout(function(){
                            self.$('.sld_page').click();
                            self.map.zoomToExtent(afeature.geometry.getBounds());
                        },1000);
                    }
                  }
                });
                self.map.removePopup(popup);
            });

            this.$('.new_report_map').click(function (ev) {
                ev.preventDefault();
                ev.stopPropagation();
                var electric_model = "component." + feature.attributes["electric_model"];
                var domain = [["delegated_id","=",parseInt(feature.attributes["device_id"])]]
                self._rpc({
                   model: electric_model,
                   method: 'search_read',
                   fields: ["id", "parent_id", "component_number"],
                   domain: domain,
                   limit: 1,
                }).then(function(component){
                  if (component.length > 0){
                    self.select_component_test_model(electric_model, function () {
                        var context= {
                            default_component_model: electric_model,
                            default_base_component_id: parseInt(feature.attributes["device_id"]),
                            default_component_number: component[0]['component_number'],
                            default_parent_id: component[0]['parent_id'][0],
                            default_electric_component_type: feature.attributes["electric_model"],
                            default_test_type: self.selected_test_model
                        }

                        if (self.prev_form_dialog) {
                            self.prev_form_dialog.destroy();
                        }
                        self.prev_form_dialog = new dialogs.FormViewDialog(self, {
                            res_model: self.selected_test_model,
                            context: context,
                            title: "New..",
                            res_id: null,
                            readonly: false,
                        }).open();

                    });
                  }

                });
                self.map.removePopup(popup);
            });

            this.$('.open_component_view').click(function(ev){
              self._onOpenComponentView(ev);
            });
        },

        fix_bounds: function (bounds, width, height) {
            var scale_factor = width / height;
            var cy = (bounds.bottom + bounds.top) / 2;
            var new_height = bounds.getWidth() / scale_factor;
            var image_bounds = new OpenLayers.Bounds(bounds.left, cy - (new_height / 2), bounds.right, cy + (new_height / 2));
            image_bounds = image_bounds.scale(0.25);
            return image_bounds;
        },

        reload_sld_hotspots: function(){
            if (this.map){
                var devLayer = this.map.getLayersByName('Devices');
                devLayer[0].removeAllFeatures({silent: true});
                if (this.current_sld_model && this.current_sld_id){
                    var domain = [["id","=",Number(this.current_sld_id)]];
                    var hotpointsField = (this.current_sld_model == "component.location")?'single_line_diagram_hotpoints':'electric_board_hotpoints';
                    this._rpc({
                       model: this.current_sld_model,
                       method: 'search_read',
                       fields: [hotpointsField],
                       domain: domain,
                       limit: 1,
                    }).done(function (result) {
                        var record = result[0];
                        if (record[hotpointsField] != false) {
                            var geojson_format = new OpenLayers.Format.GeoJSON();
                            var features = geojson_format.read(record[hotpointsField], "FeatureCollection");
                            var styledFeatures = features.map(function(feature){
                                feature.style = Object.assign({}, OpenLayers.Feature.Vector.style.default, {strokeColor: STATE_COLOR_SELECTION[feature.attributes['maintenance_state_color']]});
                                return feature;
                            });
                            devLayer[0].addFeatures(styledFeatures);
                        }
                    });
                }
            }
        },

        select_component_for_map: function(parent_model, parent_id, do_it){
            var self = this;
            var componentDomain = self.tree.getActiveComponentsDomain();
            var only_not_located_domain = componentDomain;

                self._rpc({
                  model: 'component.component',
                  method: 'search_read',
                  fields: [
                            'id',
                            'name',
                            'model',
                            'electric_component_type',
                            'maintenance_statelor_co'
                          ],
                  domain: only_not_located_domain,
                }).then(function(components){
                var buttons = [
                    {
                        text: _t("Ok"),
                        classes: 'btn-primary',
                        close: true,
                        click: do_it
                    },
                    {
                        text: _t("Cancel"),
                        close: true,
                        click: options && options.cancel_callback
                    }

                ];
                var options = {
                    size: 'medium',
                    buttons: buttons,
                    title: _t("Select Component"),
                    $content: qweb.render('ComponentSelectDialog', {components: components}),
                    cancel_callback: function () {
                        self.selected_component = null;
                    }
                };
                var component_dialog = new dialog(this, _.extend(options)).open();
                component_dialog.$el.find('input').click(function() {
                    var value = $(this).attr('value');
                    var name = $(this).attr('device_name');
                    var model = $(this).attr('model');
                    var electric_model = $(this).attr('electric_model');
                    var maintenance_state_color = $(this).attr('maintenance_state_color');
                    self.selected_component.attributes['device_id'] = value;
                    self.selected_component.attributes['device_name'] = name;
                    self.selected_component.attributes['device_model'] = model;
                    self.selected_component.attributes['electric_model'] = electric_model;
                    self.selected_component.attributes['maintenance_state_color'] = maintenance_state_color;

                    self.selected_component.style = Object.assign({}, OpenLayers.Feature.Vector.style.default, {strokeColor: STATE_COLOR_SELECTION[maintenance_state_color]});
                });
                return component_dialog;
            });
        },


        show_property_view: function (model, id) {
            var self = this;

            this.$(".o_cexplorer_view").children().remove();
            this.$(".o_cexplorer_properties").children().remove();
            this.$(".o_cexplorer_properties").show();
            this.property_panel_parent = this.$(".o_cexplorer_properties");
            if (this.property_view != null) {
                this.remove_view(this.property_view);
            }

                var domain = [['model', '=', model], ['type', '=', 'form']];

                var self = this;
                this._rpc({
                   model: 'ir.ui.view',
                   method: 'search_read',
                   domain: domain,
                   limit: 1,
                }).then(function(view){
                    self._rpc({
                       model: view[0].model,
                       method: 'fields_view_get',
                       kwargs:{
                         view_id: view[0].id,
                         //view_type: 'form',
                       }
                    }).then(function(info){
                       var viewInfo = info;

                       var params = {
                           context: {
                             //default_res_model: 'component.component',
                             //default_res_id:  Number(id),
                           },
                           currentId: Number(id),
                           mode: 'readonly',
                           modelName: view[0].model,
                           default_buttons: true,
                        };


                        var form_view = new CExplorerPropertiesFormView(viewInfo, params);

                        form_view.getController(self).then(function (controller) {
                            controller.appendTo(self.property_panel_parent);
                        });
                    });
                 });
        },

        hide_component_detail_view: function () {
            this.$(".detail_page").hide();
        },

        hide_property_view: function () {
            this.$(".o_cexplorer_properties").hide();
        },

        //Single Line Diagram View
        hide_sld_view: function () {
            this.$(".o_cexplorer_sld").hide();
            this.$(".sld_page").css("visibility", "hidden");
        },

        remove_previous_view: function () {
            this.$(".o_kanban_view").remove();
            this.$(".oe_mixed_view").remove();
        },

        remove_record: function (model, id) {
            var self = this;
            dialog.confirm(this, _t("Are you sure you want to delete this record ?"), {confirm_callback: function () {
                return this._rpc({
                           model: model,
                           method: 'unlink',
                           args: [id],
                        }).then(function(){
                            dialog.alert(this, _t("Changes where done."));
                            self.tree.reload_content();
                            return true;
                        });
            }});
        },

        delete_project: function (id) {
            this.remove_record('component.project', id);
        },
        delete_site: function (id) {
            this.remove_record('component.site', id);
        },

        delete_location: function (id) {
            this.remove_record('component.location', id);
        },

        delete_sublocation: function (id) {
            this.remove_record('component.sublocation', id);
        },

        show_properties_action: function (model, id) {
            var action = {
                type: 'ir.actions.act_window',
                res_model: model,
                res_id: Number(id),
                views: [[false, 'form']],
                target: 'new',
                flags: {
                    action_buttons: true,
                    create: false,
                },

                on_saved: function (record, changed) {
                        if (changed) {
                            $.when(self.prev_form_dialog.view_form.save()).done(function () {
                                self.prev_form_dialog.close();
                                if (self.current_model == "component.component") {
                                    //self.show_component_view(self.current_location);
                                } else {
                                    self.show_location_view(self.current_site);
                                    self.tree.reload_content();
                                }


                            });
                        }
                },
            };
            return action
        },

        is_component: function (model) {
            if (model == 'component.component') {
                return true;
            }
            for (var i = 0; i < this.component_types.length; i++) {
                if (this.component_types[i].model == model) {
                    return true;
                }
            }
            return false;
        },

        show_properties: function (model, id) {
            var self = this;
            if (this.is_component(model)) {
                this.show_component_properties(model, id);
            } else {
                var view_fields = ['id', 'name', 'type'];
                var domain = [['model', '=', model], ['type', '=', 'form']];

                this._rpc({
                   model: 'ir.ui.view',
                   method: 'search_read',
                   fields: this.view_fields,
                   domain: this.domain,
                   limit: 1,
                }).then(function(view){
                    if (self.prev_form_dialog) {
                        self.prev_form_dialog.destroy();
                    }
                    self.prev_form_dialog = new dialogs.FormViewDialog(self, {
                        res_model: model,
                        title: "Edit..",
                        view_id: view.id,
                        res_id: id,
                        readonly: false,

                        on_saved: function (record, changed) {
                            //if (changed) {
                                self.prev_form_dialog.close();
                                if (self.current_model == "component.component") {
                                   //self.show_component_view(self.current_location);
                                } else {
                                   //self.show_location_view(self.current_site);
                                   self.tree.reload_content();
                                }
                            //}
                        },

                        /*write_function: function (id, withdata, options) {
                            return this._rpc({
                               model: model,
                               method: 'write',
                               args: [[id], withdata],
                            });
                        }*/
                    }).open();

                    /*self.prev_form_dialog.on('write_completed', self, function () {
                        //self.show_property_view(model, id);
                    });*/
                });
            }
        },


    });

    return ComponentExplorerTreeRenderer;

});
