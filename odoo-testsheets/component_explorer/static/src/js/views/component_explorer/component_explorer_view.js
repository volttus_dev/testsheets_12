odoo.define('component_explorer.ComponentExplorerView', function (require) {
    'use strict';

    var  AbstractView = require('web.AbstractView');
    var  view_registry = require('web.view_registry');
    var  ComponentExplorerController = require('component_explorer.ComponentExplorerController');
    var  ComponentExplorerModel = require('component_explorer.ComponentExplorerModel');
    var  ComponentExplorerRenderer = require('component_explorer.ComponentExplorerRenderer');

    var ComponentExplorerView = AbstractView.extend({
        display_name: 'Component Explorer',
        icon: 'fa-th-large',
        config: {
            Model: ComponentExplorerModel,
            Controller: ComponentExplorerController,
            Renderer: ComponentExplorerRenderer,
        },
        viewType: 'component_explorer',

        /*init: function (viewInfo, params) {
            this._super.apply(this, arguments);
       },*/
    });

    view_registry.add('component_explorer', ComponentExplorerView);
    return ComponentExplorerView;
});
