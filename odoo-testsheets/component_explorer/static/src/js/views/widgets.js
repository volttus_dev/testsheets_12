odoo.define('component_explorer.widgets', function (require) {
    "use strict";

    var Widget = require('web.Widget');
    var utils = require('web.utils');

    var ExplorerTreeView = Widget.extend({
        template: "ProjectTreeView",
        init: function (parent, locations, sublocations, domain) {
            this._super(parent);
            this.domain = domain;
            this.locations = locations;
            this.sublocations = sublocations;
            this.user_is_manager = false;
            if (utils.get_cookie("tree_active_key") != undefined){
                this.active_key = utils.get_cookie("tree_active_key");
            }else{
                this.active_key = 'root';
            }
        },
        getActiveComponentsDomain: function(){
            var key = this.get_tree().activeNode.key;
            var model = key.split("_")[0];
            var id = key.split("_")[1];
            var activeDomain = ["&",["parent_model","=","component."+model],["parent_id","=",id]];
            var childrens = this.get_tree().activeNode.getChildren();
            if (childrens != null){
                for (var i = 0; i < childrens.length; i++) {
                    key = childrens[i].key;
                    model = key.split("_")[0];
                    id = key.split("_")[1];
                    activeDomain = ["|"].concat(activeDomain).concat(["&",["parent_model","=","component."+model],["parent_id","=",id]]);
                    var slchildrens = childrens[i].getChildren();
                    if (slchildrens != null){
                        for (var i = 0; i < slchildrens.length; i++) {
                            key = slchildrens[i].key;
                            model = key.split("_")[0];
                            id = key.split("_")[1];
                            activeDomain = ["|"].concat(activeDomain).concat(["&",["parent_model","=","component."+model],["parent_id","=",id]]);
                        }
                    }
                }
            }
            return activeDomain;
        },
        appendTo: function (target) {
            this._super(target);
            this.init_tree();
            this.load();
            this.$("#tree").css("height", "100%");
            this.$(".ui-fancytree").css("height", "100%");
            /*var self = this;
            var Users = new data.Model('res.users');
            Users.call('has_group', ['component.group_component_component_manager']).done(function(is_manager) {
                self.user_is_manager = is_manager;
                self.load();
                self.$("#tree").css("height", "100%");
                self.$(".ui-fancytree").css("height", "100%");
            });*/
        },
        delete_selected: function (key) {
            var pair = key.split("_");
            var deleted = false;
            var id = Number(pair[1]);
            if (pair[0] == 'project') {
                deleted = this.getParent().delete_project(id);
            } else if (pair[0] == 'site') {
                deleted = this.getParent().delete_site(id);
            } else if (pair[0] == 'location') {
                deleted = this.getParent().delete_location(id);
            } else if (pair[0] == 'sublocation') {
                deleted = this.getParent().delete_sublocation(id);
            } else if (pair[0] == 'component') {
                deleted = this.getParent().delete_component(id);
            }
            if (deleted){
                var tree = this.get_tree();
                var todelete = tree.getNodeByKey(type+"_"+site);
                todelete.remove();
            }
        },
        initialized: function () {
            if (this.$el['0']){
                var obj = this.$el['0'];
                for (var property in obj){
                    if (property.indexOf('jQuery')==0){
                        return true;
                    }
                }
            }
            return false;
        },
        init_tree: function () {
            var self = this;
            this.tree = this.$el.fancytree({
                extensions: ['contextMenu'],
                contextMenu: {
                    selector: "fancytree-selector",
                    menu: function(node){
                        if ((node.key == "root")/*&&(self.user_is_manager)*/){
                            return {
                                'add_project': {'name': 'Add project', 'icon': 'new', 'disabled': false},
                            };
                        }else{
                            var pair = node.key.split("_");
                            var model = pair[0];
                            if (model == "project"){
                                if (self.user_is_manager){
                                    return {
                                        'add_site': {'name': 'Add site', 'icon': 'new', 'disabled': false},
                                        'properties': {'name': 'Edit', 'icon': 'new', 'disabled': false},
                                        'delete': {'name': 'Delete', 'icon': 'delete', 'disabled': false},
                                    };
                                }
                            } else if (model == "site"){
                                if (self.user_is_manager){
                                    return {
                                        'add_location': {'name': 'Add location', 'icon': 'new', 'disabled': false},
                                        'properties': {'name': 'Edit', 'icon': 'new', 'disabled': false},
                                        'delete': {'name': 'Delete', 'icon': 'delete', 'disabled': false},
                                    };
                                }
                            } else if (model == "location"){
                                if (self.user_is_manager){
                                    return {
                                        'add_sublocation': {'name': 'Add sublocation', 'icon': 'new', 'disabled': false},
                                        'properties': {'name': 'Edit', 'icon': 'new', 'disabled': false},
                                        'delete': {'name': 'Delete', 'icon': 'delete', 'disabled': false},
                                    };
                                }
                            } else if (model == "sublocation"){
                                if (self.user_is_manager){
                                    return {
                                        'add_device': {'name': 'Add device', 'icon': 'new', 'disabled': false},
                                        'properties': {'name': 'Edit', 'icon': 'new', 'disabled': false},
                                        'delete': {'name': 'Delete', 'icon': 'delete', 'disabled': false},
                                    };
                                }
                            }
                        }
                    },
                    actions: function (node, action) {
                        var pair = node.key.split("_");
                        var model = "component."+pair[0];
                        var id = Number(pair[1]);
                        switch (action) {
                            case "delete":
                                self.active_key = node.parent.key;
                                self.delete_selected(node.key);
                                break;
                            case "add_project":
                                self.getParent().add_record("component.project", {
                                });
                                break;
                            case "add_device":
                                self.getParent().add_record("component.component", {
                                    default_parent_id: id,
                                    default_parent_model: model
                                });
                                break;
                            case "add_location":
                                self.getParent().add_record("component.location", {
                                    default_site_id: id,
                                });
                                break;
                            case "add_sublocation":
                                self.getParent().add_record("component.sublocation", {
                                    default_location_id: id,
                                });
                                break;
                            case "add_site":
                                self.getParent().add_record("component.site", {
                                    default_project_id: id
                                });
                                break;
                            case "properties":
                                self.getParent().show_properties(model, id);
                                break;
                            default:
                                alert("Unhandled clipboard action '" + action + "'");
                        }
                    }
                },
                activate: function (event, data) {
                    self.on_activate(data.node.key);
                },
                expand: function(event, data){
                    self.$('.fancytree-icon').addClass(function (index, currentClass) {
                        if (!('fancytree-selector' in currentClass.split(/\s+/))){
                            return 'fancytree-selector';
                        }
                    });
                    self.$('.fancytree-title').addClass(function (index, currentClass) {
                        if (!('fancytree-selector' in currentClass.split(/\s+/))){
                            return 'fancytree-selector';
                        }
                    });
                }
            });
            var rootNode = this.tree.fancytree("getRootNode");
            rootNode = rootNode.addChildren({
                key: 'root',
                title: "Projects",
                folder: true,
                expanded: true,
                icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/projectroot.ico'
            });
            this.make_selectable(rootNode);
        },
        make_selectable: function (node) {
            this.$('.fancytree-icon').addClass(function (index, currentClass) {
                if (!('fancytree-selector' in currentClass.split(/\s+/))){
                    return 'fancytree-selector';
                }
            });
            this.$('.fancytree-title').addClass(function (index, currentClass) {
                if (!('fancytree-selector' in currentClass.split(/\s+/))){
                    return 'fancytree-selector';
                }
            });
        },
        activate_node_by_key: function (key) {
            this.active_key = key;
            var tree = this.get_tree();
            if (tree.getNodeByKey(key)){
                tree.activateKey(key);
            }
        },
        on_activate: function (node_key) {
            this.active_key = node_key;
            utils.set_cookie("tree_active_key", node_key);
            if (node_key == 'root'){
                this.getParent().show_project_view(id);
                this.getParent().hide_sld_view();
                this.getParent().hide_property_view();
            }else{
                var pair = node_key.split("_");
                var model = "component."+pair[0];
                var id = pair[1];
                this.getParent().update_view(pair[0], id, node_key);
            }
        },
        get_tree: function () {
            if (!this.initialized()){
                this.init_tree();
            }
            return this.$el.fancytree("getTree");
        },
        get_project_root: function () {
            var tree = this.get_tree();
            var rootNode = tree.getNodeByKey('root');
            return rootNode;
        },
        /*Para cargar los proyectos que no tienen componentes*/
        load_project_data: function () {
            var self = this;
            this.project_model = new Model('component.project');
            //ahora se cargan los locations
            this.device_fields = ['id', 'name'];
            var rootNode = this.get_tree().getNodeByKey('root');
            this.project_model.query(this.device_fields).all().done(function (projects) {
                var projects_ids = [];
                for (var i = 0; i < projects.length; i++) {
                    var project = projects[i];
                    var p = rootNode.addChildren({
                        key: 'project_' + project.id,
                        title: project.name,
                        tooltip: project.abstract,
                        folder: true,
                        expanded: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/prj.ico'
                    });
                    projects_ids.push(project.id);
                    self.make_selectable(p);
                }
                $.when(self.load_site_data(projects_ids)).done(function () {
                    return;
                });
            });
        },
        load_site_data: function (projects) {
            var domain = [['project_id','in',projects]];
            this.site_model = new Model('component.site', {}, domain);
            //ahora se cargan los locations
            this.site_fields = ['id', 'name', 'project_id'];
            var self = this;
            this.site_model.query(this.site_fields).all().done(function (sites) {
                var site_ids = [];
                for (var i = 0; i < sites.length; i++) {
                    var site = sites[i];
                    var tree = self.get_tree();
                    var projectNode = tree.getNodeByKey('project_' + site.project_id[0]);
                    self.add_child(projectNode, {
                        key: 'site_' + site.id,
                        title: site.name,
                        folder: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/site.ico'
                    });
                    site_ids.push(site.id);
                }
                $.when(self.load_location_data(site_ids)).done(function () {
                    return;
                });
            });*/
        },
        load_location_data: function (site_ids) {
            this.location_model = new Model('component.location', {},  [['site_id','in',site_ids]]);
            //ahora se cargan los locations
            this.location_fields = ['id', 'name', 'site_id'];
            var self = this;
            this.location_model.query(this.location_fields).all().done(function (locations) {
                var location_ids = [];
                for (var i = 0; i < locations.length; i++) {
                    var location = locations[i];
                    var nodeData = {
                        key: 'location_' + location.id,
                        title: location.name,
                        folder: true,
                        expanded: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/location.ico'
                    };
                    self.add_to_site_node(location.site_id[0], nodeData);
                    location_ids.push(location.id);
                }
                $.when(self.load_sublocation_data(location_ids)).done(function () {
                    return;
                });
            });*/
        },
        add_child: function (node, child) {
            var tree = this.get_tree();
            var childNode = tree.getNodeByKey(child.key);
            if ((childNode == undefined)&&(childNode == null)){
                childNode = node.addChildren(child);
            }
            if (this.active_key){
                //tratar de activar el último nodo activo
                this.activate_node_by_key(this.active_key);
            }
            this.make_selectable(childNode);
            return childNode;
        },
        add_childs: function (node, firstChild, secondChild, thirthChild) {
            if ((firstChild != undefined)&&(firstChild != null)){
                var firstNode = this.add_child(node, firstChild);
                if ((secondChild != undefined)&&(secondChild != null)){
                    var secondNode = this.add_child(firstNode, secondChild);
                    if ((thirthChild != undefined)&&(thirthChild != null)){
                        this.add_child(secondNode, thirthChild);
                    }
                }
                return firstNode;
            }
        },

        add_to_project_node: function (project, sitechild, locchild, sublocchild) {
            var self = this;
            var tree = this.get_tree();
            var projectNode = tree.getNodeByKey('project_' + project[0]);
            if ((projectNode == undefined)||(projectNode == null)){
                var domain = [['id','=',project[0]]];
                this.project_model = new Model('component.project');
                this.project_fields = ['id', 'name', 'abstract'];
                this.project_model.query(this.project_fields).filter(domain).first().done(function (project) {
                    var projectNode = tree.getNodeByKey('project_' + project.id);
                    if ((projectNode != undefined)&&(projectNode != null)) {
                        //Pregunto otra vez por lo del asincronico
                        return self.add_childs(projectNode, sitechild, locchild, sublocchild);
                    }else{
                        var nodeData = {
                            key: 'project_' + project.id,
                            title: project.name,
                            tooltip: project.abstract,
                            folder: true,
                            expanded: true,
                            icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/prj.ico'
                        };
                        var projectNode = self.get_project_root().addChildren(nodeData);
                        return self.add_childs(projectNode, sitechild, locchild, sublocchild);
                    }
                });
            }else{
                return self.add_childs(projectNode, sitechild, locchild, sublocchild);
            }
        },
        add_to_site_node: function (site, locchild, sublocchild) {
            var tree = this.get_tree();
            var siteNode = tree.getNodeByKey('site_' + site);
            if ((siteNode == undefined)||(siteNode == null)){
                var domain = [['id','=',site]];
                this.site_model = new Model('component.site', {}, domain);
                this.site_fields = ['id', 'name', 'project_id'];
                var self = this;
                this.site_model.query(this.site_fields).first().done(function (site) {
                    var siteNode = self.add_to_project_node(site.project_id, {
                        key: 'site_' + site.id,
                        title: site.name,
                        folder: true,
                        expanded: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/site.ico'
                    }, locchild, sublocchild);
                    return siteNode;
                });
            }else{
                return this.add_childs(siteNode, locchild, sublocchild);
            }
        },
        add_to_location_node: function (location, sublocchild) {
            var self = this;
            var tree = this.get_tree();
            var locationNode = tree.getNodeByKey('location_' + location);
            if ((locationNode == undefined)||(locationNode == null)){
                var domain = [['id','=',location]];
                this.location_model = new Model('component.location', {}, domain);
                this.location_fields = ['id', 'name', 'site_id'];
                this.location_model.query(this.location_fields).first().done(function (location) {
                    var nodeData = {
                        key: 'location_' + location.id,
                        title: location.name,
                        folder: true,
                        data: {
                            domain: self.domain
                        },
                        expanded: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/location.ico'
                    };
                    locationNode = self.add_to_site_node(location.site_id[0], nodeData, sublocchild);
                    return locationNode;
                });
            }else{
                return self.add_childs(locationNode, sublocchild);
            }
        },
        get_sublocation_node: function(sublocation){
            var tree = this.get_tree();
            var sublocationNode = tree.getNodeByKey('location_' + sublocation);
            if ((sublocationNode == undefined)||(sublocationNode == null)){
                var domain = [['id','=',sublocation]];
                this.sublocation_model = new Model('component.sublocation', {}, domain);
                this.sublocation_fields = ['id', 'name', 'location_id'];
                var self = this;
                this.sublocation_model.query(this.sublocation_fields).first().done(function (sublocation) {
                    var nodeData = {
                        key: 'sublocation_' + sublocation.id,
                        title: sublocation.name,
                        folder: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/subl.ico',
                        data: {
                            domain: self.domain
                        }
                    };
                    self.add_to_location_node(sublocation.location_id[0], nodeData);
                });
            }
        },
        load_component_data: function () {
            var componentModel = new Model("component.component");
            var self = this;
            componentModel.query(['id', 'parent_id', 'parent_model']).filter(this.domain).all().then(function(components) {
                if (components.length > 0){
                    var locations = [];
                    var sublocations = [];
                    for(var i= 0; i < components.length; i++){
                        if (components[i].parent_model == 'component.location'){
                            locations.push(components[i].parent_id);
                            self.add_to_location_node(components[i].parent_id);
                        }else{
                            self.get_sublocation_node(components[i].parent_id);
                        }
                    }
                }
            });
        },
        load_sublocation_data: function (location_ids) {
            this.sublocation_model = new Model('component.sublocation', {},  [['location_id','in',location_ids]]);
            //ahora se cargan los locations
            this.sublocation_fields = ['id', 'name', 'location_id'];
            var self = this;
            this.sublocation_model.query(this.sublocation_fields).all().done(function (sublocations) {
                for (var i = 0; i < sublocations.length; i++) {
                    var sublocation = sublocations[i];
                    var nodeData = {
                        key: 'sublocation_' + sublocation.id,
                        title: sublocation.name,
                        folder: true,
                        expanded: true,
                        icon: '/component_explorer/static/lib/fancytree/css/skin-xp/images/subl.ico'
                    };
                    self.add_to_location_node(sublocation.location_id[0], nodeData);
                }
            });
        },
        load: function () {
            var self = this;
            $.when(this.load_component_data()).done(function () {
                if (self.domain == undefined){
                    $.when(self.load_project_data()).done(function () {
                        if (self.active_key){
                            self.activate_node_by_key(self.active_key);
                        }
                    });
                }else{
                    self.activate_node_by_key(self.active_key);
                }
            })
        },
        reload_content: function () {
            var root = this.$el.fancytree("getRootNode");
            this.get_project_root().removeChildren();
            this.load();
        },
        activate_node: function (type, site) {
            var tree = this.get_tree();
            tree.activateKey(type+"_"+site);
        },
        activate_node_by_model: function (model, id) {
            var type = model.split('\.')[1];
            this.activate_node(type, id);
        },
        activate_site_node: function (site) {
            this.activate_node("site", site);
        },
        activate_location_node: function (location) {
            this.activate_node("location", location);
        },
        activate_project_node: function (project) {
            this.activate_node("project", project);
        },
        add_record: function(model, context) {
            this.getParent().add_record(model, context);
        },
    });
});