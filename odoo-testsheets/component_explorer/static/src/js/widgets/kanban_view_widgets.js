odoo.define('component_explorer.kanban_view_widgets', function (require) {
  "use strict";

   var Widget = require('web.Widget');
   var CExplorerKanbanView = require('web.KanbanView');
   var ActionManager = require('web.ActionManager');

   var BaseKanbanView = Widget.extend({
        init: function (parent, parent_model, parent_id) {
            this._super(parent);
            this.parent_id = parent_id;
            this.current_model = parent_model;
            //this.dataset = new data.DataSetSearch(this, this.current_model, null);
            //this.action_manager = new ActionManager();
        },

        get_treeview : function () {
            return this.getParent().get_treeview();
        },

        appendTo: function (target) {
            this._super(target);
            target.children().remove();
            this.load(target);
        },

        get_context: function () {
            return {
            }
        },

        get_default_kanban_options: function () {
            return {
                // records can be selected one by one
                selectable: true,
                // list rows can be deleted
                deletable: false,
                // whether the column headers should be displayed
                header: true,
                // display addition button, with that label
                addable: _lt("Create"),
                // whether the list view can be sorted, note that once a view has been
                // sorted it can not be reordered anymore
                sortable: false,
                // whether the view rows can be reordered (via vertical drag & drop)
                reorderable: false,
                action_buttons: true,
                //whether the editable property of the view has to be disabled
                disable_editable_mode: false,
                editable: 'top',
                creatable: true,
                context: this.get_context()
            };
        },

        get_domain: function () {
            return [[]];
        },

        do_reload: function () {
            if (this.right_panel_view){
                this.right_panel_view.do_reload();
            }
        },

        load: function (target) {
            this.target = target;
            var cexplorer = this.getParent();
            if (cexplorer['list_view']){
                cexplorer.remove_previous_view(target);
            }

            var view_fields = ['id','name','type', 'model', 'arch_db'];
            var domain = [['name','=',this.get_view_name()]];

            var widget_domain = this.get_domain();
            var widget_context = this.get_context();

            var self = this;
            this._rpc({
               model: 'ir.ui.view',
               method: 'search_read',
               //fields: view_fields,
               domain: domain,
               limit: 1,
            }).then(function(view){
                //alert('cargando Vista Kanban');

                self._rpc({
                   model: view[0].model,
                   method: 'fields_view_get',
                   kwargs:{
                     view_id: view[0].id,
                     view_type: 'kanban',
                   }
                }).then(function(info){
                   var viewInfo = info;

                    /*
                        action: action,
                        context: context,
                        domain: domain,
                        groupBy: context.group_by || [],
                        modelName: action.res_model,
                        hasSelectors: false,
                    */

                    var params = {
                       action: { },
                       context: widget_context,
                       domain: widget_domain,
                       groupBy: [],
                       modelName: view[0].model,
                       hasSelectors: false,
                    };


                    var kanban_view = new CExplorerKanbanView(viewInfo, params);
                    kanban_view.getController(self).then(function (controller) {
                        controller.appendTo(self.target);
                    });

                });
            });
        }
    });



    var ProjectKanbanView = BaseKanbanView.extend({
        init: function (parent, parent_id) {
            this._super(parent, 'component.project', -1);
        },
        get_view_name: function () {
            return 'Project Kanban Explorer';
        },
        get_domain: function () {
            return [[1, '=', 1]];
        },
        get_context: function () {
            return {

            }
        },
    });


    var SiteKanbanView = BaseKanbanView.extend({
        init: function (parent, parent_id) {
            this._super(parent, 'component.site', parent_id);
        },
        get_view_name: function () {
            return 'Site Kanban Explorer';
        },
        get_domain: function () {
            return [["project_id","=",Number(this.parent_id)]];
        },
        get_context: function () {
            return {
                default_project_id: this.parent_id,
            }
        },
    });


    var LocationKanbanView = BaseKanbanView.extend({
        init: function (parent, parent_id) {
            this._super(parent, 'component.location', parent_id);
        },
        get_view_name: function () {
            return 'Location Kanban Explorer';
        },
        get_domain: function () {
            return [["site_id","=",Number(this.parent_id)]];
        },
        get_context: function () {
            return {
                default_site_id: this.parent_id,
            }
        },
     });


    var SubLocationKanbanView = BaseKanbanView.extend({
        init: function (parent, parent_id) {
            this._super(parent, 'component.sublocation', parent_id);
        },
        get_view_name: function () {
            return 'Sublocation Kanban Explorer';
        },
        get_domain: function () {
            return [["location_id","=",Number(this.parent_id)]];
        },
        get_context: function () {
            return {
                default_location_id: this.parent_id,
            }
        },
    });


    var ComponentsView = Widget.extend({
        init: function (parent, parent_id, parent_model, domain) {
            this._super(parent);
            this.parent_id = parent_id;
            this.parent_model = parent_model;
            this.current_model = "component.component";
            this.domain = domain;
        },
        appendTo: function (target) {
            this._super(target);
            target.children().remove();
            this.load(target)
        },

        get_default_kanban_options: function (concept) {
            return {
                // records can be selected one by one
                selectable: true,
                // list rows can be deleted
                deletable: false,
                // whether the column headers should be displayed
                header: true,
                // display addition button, with that label
                addable: _lt("Create "+concept),
                // whether the list view can be sorted, note that once a view has been
                // sorted it can not be reordered anymore
                sortable: false,
                // whether the view rows can be reordered (via vertical drag & drop)
                reorderable: false,
                action_buttons: true,
                //whether the editable property of the view has to be disabled
                disable_editable_mode: false,
                editable: 'top',
                creatable: true,
                context: {
                    default_location_id: this.current_location,
                    default_site_id: this.current_site,
                },
                domain: this.get_component_domain()
            };
        },
        get_component_domain: function () {
            var domain = [["parent_id","=",Number(this.parent_id)], ["parent_model","=",this.parent_model]];
            if (this.domain.length > 0){
                domain = domain.concat(this.domain);
            }
            return domain;
        },
        on_delete_record: function (e) {
            alert();
        },
        load: function (target) {
            var self = this;
            this.target = target;
            var cexplorer = this.getParent();
            if (cexplorer['list_view']){
                cexplorer.remove_previous_view(target);
            }

            var domain = [['name','=','Component Kanban']];
            var widget_domain = this.get_component_domain();

            var self = this;
            this._rpc({
               model: 'ir.ui.view',
               method: 'search_read',
               //fields: view_fields,
               domain: domain,
               limit: 1,
            }).then(function(view){
                //alert('cargando Vista Kanban');

                self._rpc({
                   model: view[0].model,
                   method: 'fields_view_get',
                   kwargs:{
                     view_id: view[0].id,
                     view_type: 'kanban',
                   }
                }).then(function(info){
                   var viewInfo = info;

                    /*
                        action: action,
                        context: context,
                        domain: domain,
                        groupBy: context.group_by || [],
                        modelName: action.res_model,
                        hasSelectors: false,
                    */

                    var params = {
                       context: {},
                       domain: widget_domain,
                       groupBy: [],
                       modelName: view[0].model,
                       hasSelectors: false,
                    };


                    var kanban_view = new CExplorerKanbanView(viewInfo, params);

                    kanban_view.getController(self).then(function (controller) {
                        controller.appendTo(self.target);
                    });

                });
            });

        },
        get_treeview : function () {
            return this.getParent().get_treeview();
        },
    });


    return {
       ProjectKanbanView: ProjectKanbanView,
       SiteKanbanView: SiteKanbanView,
       LocationKanbanView: LocationKanbanView,
       SubLocationKanbanView: SubLocationKanbanView,
       ComponentsView: ComponentsView,
    }

  });

