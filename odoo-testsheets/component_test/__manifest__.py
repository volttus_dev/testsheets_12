# -*- encoding: utf-8 -*-
##############################################################################
#
#    Samples module for Odoo Web Login Screen
#    Copyright (C) 2018- XUBI.ME (http://www.xubi.me)
#    @author binhnguyenxuan (https://www.linkedin.com/in/binhnguyenxuan)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#    
#    Background Source: http://forum.xda-developers.com/showpost.php?p=37322378
#
##############################################################################
{
    'name': 'VA Component Test',
    'version': '1.0',
    'summary': 'Component Testing',
    'description': """
Testing Electric Component
=====================================

Component Testing and record of Test sheets.

Required modules:
    * component
    """,
    'author': 'VoltusAutomation, KRKA',
    'website': 'http://www.voltusautomation.com/component_test',
    'category': 'Enterprise Component Management',
    'sequence': 0,
    'depends': [
                'component',
                'document',
                'hr',
                'web'
                ],
    'demo': [],
    'js': ['static/src/js/component_test_view.js',
           'static/src/js/digital_sign.js',
           'static/lib/jSignature/jSignatureCustom.js'
           ],
    'data': [
        'security/ir.model.access.csv',
        'views/view_electriccomponent_wtest.xml',
        'views/view_testsheets.xml',
        'views/view_service_reports.xml',
        'views/view_drytransformer_test.xml',
        'views/view_voltransformer_test.xml',
        'views/view_liquidtransformer_test.xml',
        'views/view_curenttransformer_test.xml',
        'views/view_pindextransformer_test.xml',
        'views/view_automatictransferswitch_test.xml',
        'views/view_lvaccable_test.xml',
        'views/view_board_test.xml',
        'views/view_lvdccable_test.xml',
        'views/view_hvaccable_test.xml',
        'views/view_lvacbus_test.xml',
        'views/view_hvacbus_test.xml',
        'views/view_hvbusjcr_test.xml',
        'views/view_hvaccircuitbreaker_test.xml',
        'views/view_lv_circuitbreaker_test.xml',
        'views/view_lv_circuitbreakereatonamp_test.xml',
        'views/view_lvdc_circuitbreaker_test.xml',
        'views/view_load_breakswitch_test.xml',
        'views/view_mcb_withrelay_test.xml',
        'views/view_ptopground_test.xml',
        'views/view_surgearrestor_test.xml',
        'views/view_service_reports.xml',
        'views/view_site_service_reports.xml',
        'reports/report_drytransformer.xml',
        'reports/report_drytrans.xml',
        'reports/report_voltransformer.xml',
        'reports/report_liquidtransformer.xml',
        'reports/report_curenttransformer.xml',
        'reports/report_pindextransformer.xml',
        'reports/report_automatictransferswitch.xml',
        'reports/report_hvaccircuitbreaker.xml',
        'reports/report_lvaccable.xml',
        'reports/report_board.xml',
        'reports/report_lvacbus.xml',
        'reports/report_hvacbus.xml',
        'reports/report_hvbusjcr.xml',
        'reports/report_hvaccable.xml',
        'reports/report_lvdccable.xml',
        'reports/report_lvaccircuitbreaker.xml',
        'reports/report_lvaccircuitbreakereatonamp.xml',
        'reports/report_lvdccircuitbreaker.xml',
        'reports/report_lvdccb.xml',
        'reports/report_mcbwithrelay.xml',
        'reports/report_loadbreakswitch.xml',
        'reports/report_loadbs.xml',
        'reports/report_surgearrestor.xml',
        'reports/report_ptopground.xml',
        'reports/report_site_service.xml',
    ],
    'qweb': [
        'static/src/xml/component_test.xml',
        'static/src/xml/digital_sign.xml',

    ],
    'application': True,
    'installable': True,
}
