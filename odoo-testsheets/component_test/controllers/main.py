# -*- coding: utf-8 -*-

import json

from odoo import http
from odoo.http import request
from odoo.tools import misc

from io import BytesIO
from openpyxl import load_workbook


class TestController(http.Controller):

    @http.route('/component_test/import_excel', methods=['POST'])
    def import_excel(self, xls_file, type_select, component_id, jsonp='callback'):
        # book = xlrd.open_workbook(file_contents=xls_file.read() or b'')
        # page1 = book.sheet_by_index(0)
        # result = page1.cell_value(5,0)

        # doc = load_workbook(filename=BytesIO(xls_file.read()) or b'')
        # sheet1 = doc.worksheets[0]
        # result = sheet1['Z8'].value

        request.env[type_select].import_excel(xls_file, component_id)

        return 'window.top.%s(%s)' % (misc.html_escape(jsonp), json.dumps({'result': 'OKKK'}))
