# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields

from io import BytesIO
from openpyxl import load_workbook

class hv_ac_bus_test(models.Model):
    _name = 'component_test.hv_ac_bus_test'
    _inherits = {
        'component_test.order': 'delegated_id',
    }
    _description = 'HV Bus Test'

    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)


    # ********** Hi pot Test Reading of Leakage Current*************************

    TestVoltageA = fields.Integer('TestVoltageA', default=0)
    WConditionA = fields.Char('WConditionA', size=20, required=False)
    PhaseA1 = fields.Float('PhaseA1', default=0)
    PhaseA2 = fields.Float('PhaseA2', default=0)
    PhaseA3 = fields.Float('PhaseA3', default=0)
    PhaseA4 = fields.Float('PhaseA4', default=0)
    PhaseA5 = fields.Float('PhaseA5', default=0)
    PhaseA6 = fields.Float('PhaseA6', default=0)
    PhaseA7 = fields.Float('PhaseA7', default=0)


    TestVoltageB = fields.Integer('TestVoltageB', default=0)
    WConditionB = fields.Char('WConditionB', size=20, required=False)
    PhaseB1 = fields.Float('PhaseB1', default=0)
    PhaseB2 = fields.Float('PhaseB2', default=0)
    PhaseB3 = fields.Float('PhaseB3', default=0)
    PhaseB4 = fields.Float('PhaseB4', default=0)
    PhaseB5 = fields.Float('PhaseB5', default=0)
    PhaseB6 = fields.Float('PhaseB6', default=0)
    PhaseB7 = fields.Float('PhaseB7', default=0)

    TestVoltageC = fields.Integer('TestVoltageA', default=0)
    WConditionC = fields.Char('WConditionA', size=20, required=False)
    PhaseC1 = fields.Float('PhaseC1', default=0)
    PhaseC2 = fields.Float('PhaseC2', default=0)
    PhaseC3 = fields.Float('PhaseC3', default=0)
    PhaseC4 = fields.Float('PhaseC4', default=0)
    PhaseC5 = fields.Float('PhaseC5', default=0)
    PhaseC6 = fields.Float('PhaseC6', default=0)
    PhaseC7 = fields.Float('PhaseC7', default=0)

    # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
    PhtoPhAB = fields.Integer('PhtoPhAB', default=0)
    PhtoPhBA = fields.Integer('PhtoPhBA', default=0)
    PhtoPhCA = fields.Integer('PhtoPhCA', default=0)
    PhtoGroundA = fields.Integer('PhtoGroundA', default=0)
    PhtoGroundB = fields.Integer('PhtoGroundB', default=0)
    PhtoGroundC = fields.Integer('PhtoGroundC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)



    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(hv_ac_bus_test, self).fields_view_get(view_id=view_id,
                                                                       view_type=view_type,
                                                                       toolbar=toolbar,
                                                                       submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(hv_ac_bus_test, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
    # return category_id

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_hvac_bus_action').report_action(self, data=data)

    @api.model
    def import_excel(self, xls_file, component_id):
        doc = load_workbook(BytesIO(xls_file.read()))
        sheet1 = doc.worksheets[0]

        job_number = sheet1['Z8'].value

        # Status1 = fields.Char('Status1', size=20, required=False)
        # Status2 = fields.Char('Status2', size=20, required=False)
        # Status3 = fields.Char('Status3', size=20, required=False)
        # Status4 = fields.Char('Status4', size=20, required=False)
        # Status5 = fields.Char('Status5', size=20, required=False)
        # Status6 = fields.Char('Status6', size=20, required=False)
        # Notes1 = fields.Char('Notes1', size=40, required=False)
        # Notes2 = fields.Char('Notes2', size=40, required=False)
        # Notes3 = fields.Char('Notes3', size=40, required=False)
        # Notes4 = fields.Char('Notes4', size=40, required=False)
        # Notes5 = fields.Char('Notes5', size=40, required=False)
        # Notes6 = fields.Char('Notes6', size=40, required=False)

        # ********** Hi pot Test Reading of Leakage Current*************************

        TestVoltageA = sheet1['H38'].value
        # WConditionA = fields.Char('WConditionA', size=20, required=False)
        PhaseA1 = sheet1['L42'].value
        PhaseA2 = sheet1['W42'].value
        PhaseA3 = sheet1['AG42'].value
        PhaseA4 = sheet1['AQ42'].value

        TestVoltageB = sheet1['H45'].value
        # WConditionB = fields.Char('WConditionB', size=20, required=False)

        PhaseB1 = sheet1['L49'].value
        PhaseB2 = sheet1['W49'].value
        PhaseB3 = sheet1['AG49'].value
        PhaseB4 = sheet1['AQ49'].value


        TestVoltageC = sheet1['H52'].value
        # WConditionC = fields.Char('WConditionA', size=20, required=False)

        PhaseC1 = sheet1['L56'].value 
        PhaseC2 = sheet1['W56'].value 
        PhaseC3 = sheet1['AG56'].value
        PhaseC4 = sheet1['AQ56'].value


        # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
        PhtoPhAB = sheet1['U62'].value
        PhtoPhBA = sheet1['AH62'].value
        PhtoPhCA = sheet1['AU62'].value
        PhtoGroundA = sheet1['U64'].value
        PhtoGroundB = sheet1['AH64'].value
        PhtoGroundC = sheet1['AU64'].value

        # ********** Notes & Comments*************************
        # Comments = fields.Text('Comments', required=False)

        vals = {
            'job_number': job_number,
            'base_component_id': component_id,
            'TestVoltageA': TestVoltageA,
            # WConditionA

            'PhaseA1': PhaseA1,
            'PhaseA2': PhaseA2,
            'PhaseA3': PhaseA3,
            'PhaseA4': PhaseA4,

            'TestVoltageB': TestVoltageB,
            # WConditionB =
            'PhaseB1': PhaseB1,
            'PhaseB2': PhaseB2,
            'PhaseB3': PhaseB3,
            'PhaseB4': PhaseB4,

            'TestVoltageB': TestVoltageC,
            # WConditionB =
            'PhaseC1': PhaseC1,
            'PhaseC2': PhaseC2,
            'PhaseC3': PhaseC3,
            'PhaseC4': PhaseC4,

            'PhtoPhAB': PhtoPhAB,
            'PhtoPhBA': PhtoPhBA,
            'PhtoPhCA': PhtoPhCA,
            'PhtoGroundA': PhtoGroundA,
            'PhtoGroundB': PhtoGroundB,
            'PhtoGroundC': PhtoGroundC,

            # Comments
        }

        self.create(vals)

        # try:
        #     self.create(vals)
        #     return {'msg': 'File imported succesfully' }
        # except ValueError:
        #     return {'msg': 'File imported failed' }
        #

