﻿# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo import api

from lxml import etree
import simplejson  # If not installed, you have to install it by executing pip install simplejson
import time


class test_base(models.Model):
    _name = 'doc_report.doc'
    _description = 'Documents Report Base'

    STATE_SELECTION = [
        ('draft', 'DRAFT'),
        ('released', 'WAITING PARTS'),
        ('ready', 'READY TO MAINTENANCE'),
        ('done', 'DONE'),
        ('cancel', 'CANCELED')
    ]

    @api.v7
    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        data['ids'] = context.get('active_ids', [])
        data['model'] = context.get('active_model', 'ir.ui.menu')
        return self.pool['report'].get_action(cr, uid, [], 'component_test.order', data=data, context=context)

    # Columns

    # ********* Component Information ****************************************
    project_id = fields.Many2one('component.project', required=False, string='Site', readonly=True)
    project_name = fields.Char(related='project_id.name', string='Project Name:', readonly=True)
    proj_cont_rec = fields.Many2one(related='project_id.company_contacts', string='Customer Contact', relation='res.partner', readonly=True)
    krka_proj_cont_rec = fields.Many2one(related='project_id.krka_contacts', string='Krka Contact', relation='res.partner', readonly=True)

    proj_cont_name = fields.Char(related='proj_cont_rec.name', string='Project Contact Name', readonly=True)
    proj_cont_phone = fields.Char(related='proj_cont_rec.phone', string='Project Contact Phone', readonly=True)

    krka_cont_name = fields.Char(related='krka_proj_cont_rec.name', string='Krka Contact Name', readonly=True)
    krka_cont_phone = fields.Char(related='krka_proj_cont_rec.phone', string='Krka Contact Phone', readonly=True)

    doc_type = fields.Char(string="Document Type", readonly=False, required=True, help="Type of the Documents.")
    technician = fields.Many2one('res.users', string='Technician:', size=64, translate=True, required=False, readonly=True)
    job_number = fields.Char('Job number:', size=64, translate=True, required=True)
    date_execution = fields.Date('Execution Date:', required=True, default=time.strftime('%Y-%m-%d %H:%M:%S'))

    company_id = fields.Many2one('res.company', string='Company:', readonly=True, required=True, default=lambda self: self.env.user.company_id)
    company_logo = fields.Binary(related='company_id.logo', readonly=True)
    company_name = fields.Char(related='company_id.name', readonly=True)

    customer = fields.Char(related='project_id.abstract', string='Project Name:', readonly=True)

    state = fields.Selection(STATE_SELECTION, 'Status', readonly=True,
                                  help="When the maintenance order is created the status is set to 'Draft'.\n\
            If the order is confirmed the status is set to 'Waiting Parts'.\n\
            If the stock is available then the status is set to 'Ready to Maintenance'.\n\
            When the maintenance is over, the status is set to 'Done'.", default='done'),

    # ******************constant field reports*****************************
    representative_name = fields.Char('Representative Name', required=False)
    representative_phone = fields.Char('Representative Phone', required=False)
    customer_name = fields.Char('Customer', required=False)
    customer_contact_name = fields.Char('Customer Contact Name', required=False)
    customer_contact_phone = fields.Char('Customer', required=False)
    end_customer = fields.Text('End Customer', required=False)

    # ***********************Sign data*******************************
    sig_cust_name = fields.Char('SCoordinatorA', size=20, required=False)
    sign_date = fields.Date('Execution Date:', required=False)
    sign_image = fields.Binary("Image",
                                    help="This field holds the image used as image for the Component, limited to 1024x1024px.")

    editable = fields.Boolean('Editable', default=True)
    draft = fields.Boolean('Draft', default=True)

    _defaults = {
        'date_execution': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        'state': lambda *a: 'done',
        'doc_type': 'any',
        'editable': True
    }

    def set_readonly_form(self, body):
        user = self.env['res.users']

        is_manager = user.has_group('component.group_component_component_manager')
        is_user = user.has_group('component.group_component_component_user')

        if is_manager | is_user:
            doc = etree.XML(body['arch'])  # Get the view architecture of record

            for node in doc.xpath('//field'):  # Get all the fields navigating through xpath
                node_name = node.get('name')

                if (node_name != 'id') & (node_name != 'editable'):
                    modifiers = simplejson.loads(node.get('modifiers'))
                    modifiers['readonly'] = [('editable', '=', False)]
                    node.attrib['modifiers'] = simplejson.dumps(modifiers)
                    node.attrib['modifiers']

            body['arch'] = etree.tostring(doc)  # Update the view architecture of record with new architecture

        return body

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        cr, uid, context = self.env.args
        if 'default_technician' in context:
            vals['technician'] = context['default_technician']
        else:
            vals['technician'] = uid

            # if vals['draft']:
            #     vals['editable'] = True
            # else:
            #     vals['editable'] = not vals['editable']

        vals['draft'] = True
        # vals['editable'] = True

        # vals['doc_type'] = 'popo';
        return super(test_base, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'draft' in vals:
            draft = vals['draft']
        else:
            draft = self.draft

        vals['editable'] = draft

        res = super(test_base, self).write(vals)
        return res


