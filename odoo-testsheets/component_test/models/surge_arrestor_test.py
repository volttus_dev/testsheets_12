# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_surge_arrestor(models.Model):
    _name = 'component_test.surge_arrestor_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Surge Arrestor'

    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Visual & Mechanical Inspection*************************
    # Here the specific data of the Test
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)

    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)

    #  ********** Insulation Resistan as per NETA*************************
    cell1 = fields.Char('cell1', size=20, required=False)
    manu1 = fields.Char('manu1', size=20, required=False)
    catn1 = fields.Char('catn1', size=20, required=False)
    max1 = fields.Char('max1', size=20, required=False)
    agnd1 = fields.Char('agnd1', size=20, required=False)
    bgnd1 = fields.Char('bgnd1', size=20, required=False)
    cgnd1 = fields.Char('cgnd1', size=20, required=False)

    cell2 = fields.Char('cell2', size=20, required=False)
    manu2 = fields.Char('manu2', size=20, required=False)
    catn2 = fields.Char('catn2', size=20, required=False)
    max2 = fields.Char('max2', size=20, required=False)
    agnd2 = fields.Char('agnd2', size=20, required=False)
    bgnd2 = fields.Char('bgnd2', size=20, required=False)
    cgnd2 = fields.Char('cgnd2', size=20, required=False)

    cell3 = fields.Char('cell3', size=20, required=False)
    manu3 = fields.Char('manu3', size=20, required=False)
    catn3 = fields.Char('catn3', size=20, required=False)
    max3 = fields.Char('max3', size=20, required=False)
    agnd3 = fields.Char('agnd3', size=20, required=False)
    bgnd3 = fields.Char('bgnd3', size=20, required=False)
    cgnd3 = fields.Char('cgnd3', size=20, required=False)

    cell4 = fields.Char('cell4', size=20, required=False)
    manu4 = fields.Char('manu4', size=20, required=False)
    catn4 = fields.Char('catn4', size=20, required=False)
    max4 = fields.Char('max4', size=20, required=False)
    agnd4 = fields.Char('agnd4', size=20, required=False)
    bgnd4 = fields.Char('bgnd4', size=20, required=False)
    cgnd4 = fields.Char('cgnd4', size=20, required=False)

    cell5 = fields.Char('cell5', size=20, required=False)
    manu5 = fields.Char('manu5', size=20, required=False)
    catn5 = fields.Char('catn5', size=20, required=False)
    max5 = fields.Char('max5', size=20, required=False)
    agnd5 = fields.Char('agnd5', size=20, required=False)
    bgnd5 = fields.Char('bgnd5', size=20, required=False)
    cgnd5 = fields.Char('cgnd5', size=20, required=False)

    cell6 = fields.Char('cell6', size=20, required=False)
    manu6 = fields.Char('manu6', size=20, required=False)
    catn6 = fields.Char('catn6', size=20, required=False)
    max6 = fields.Char('max6', size=20, required=False)
    agnd6 = fields.Char('agnd6', size=20, required=False)
    bgnd6 = fields.Char('bgnd6', size=20, required=False)
    cgnd6 = fields.Char('cgnd6', size=20, required=False)

    cell7 = fields.Char('cell7', size=20, required=False)
    manu7 = fields.Char('manu7', size=20, required=False)
    catn7 = fields.Char('catn7', size=20, required=False)
    max7 = fields.Char('max7', size=20, required=False)
    agnd7 = fields.Char('agnd7', size=20, required=False)
    bgnd7 = fields.Char('bgnd7', size=20, required=False)
    cgnd7 = fields.Char('cgnd7', size=20, required=False)

    cell8 = fields.Char('cell8', size=20, required=False)
    manu8 = fields.Char('manu8', size=20, required=False)
    catn8 = fields.Char('catn8', size=20, required=False)
    max8 = fields.Char('max8', size=20, required=False)
    agnd8 = fields.Char('agnd8', size=20, required=False)
    bgnd8 = fields.Char('bgnd8', size=20, required=False)
    cgnd8 = fields.Char('cgnd8', size=20, required=False)

    cell9 = fields.Char('cell9', size=20, required=False)
    manu9 = fields.Char('manu9', size=20, required=False)
    catn9 = fields.Char('catn9', size=20, required=False)
    max9 = fields.Char('max9', size=20, required=False)
    agnd9 = fields.Char('agnd9', size=20, required=False)
    bgnd9 = fields.Char('bgnd9', size=20, required=False)
    cgnd9 = fields.Char('cgnd9', size=20, required=False)

    cell10 = fields.Char('cell10', size=20, required=False)
    manu10 = fields.Char('manu10', size=20, required=False)
    catn10 = fields.Char('catn10', size=20, required=False)
    max10 = fields.Char('max10', size=20, required=False)
    agnd10 = fields.Char('agnd10', size=20, required=False)
    bgnd10 = fields.Char('bgnd10', size=20, required=False)
    cgnd10 = fields.Char('cgnd10', size=20, required=False)

    cell11 = fields.Char('cell11', size=20, required=False)
    manu11 = fields.Char('manu11', size=20, required=False)
    catn11 = fields.Char('catn11', size=20, required=False)
    max11 = fields.Char('max11', size=20, required=False)
    agnd11 = fields.Char('agnd11', size=20, required=False)
    bgnd11 = fields.Char('bgnd11', size=20, required=False)
    cgnd11 = fields.Char('cgnd11', size=20, required=False)

    cell12 = fields.Char('cell12', size=20, required=False)
    manu12 = fields.Char('manu12', size=20, required=False)
    catn12 = fields.Char('catn12', size=20, required=False)
    max12 = fields.Char('max12', size=20, required=False)
    agnd12 = fields.Char('agnd12', size=20, required=False)
    bgnd12 = fields.Char('bgnd12', size=20, required=False)
    cgnd12 = fields.Char('cgnd12', size=20, required=False)

    cell13 = fields.Char('cell13', size=20, required=False)
    manu13 = fields.Char('manu13', size=20, required=False)
    catn13 = fields.Char('catn13', size=20, required=False)
    max13 = fields.Char('max13', size=20, required=False)
    agnd13 = fields.Char('agnd13', size=20, required=False)
    bgnd13 = fields.Char('bgnd13', size=20, required=False)
    cgnd13 = fields.Char('cgnd13', size=20, required=False)

    cell14 = fields.Char('cell14', size=20, required=False)
    manu14 = fields.Char('manu14', size=20, required=False)
    catn14 = fields.Char('catn14', size=20, required=False)
    max14 = fields.Char('max14', size=20, required=False)
    agnd14 = fields.Char('agnd14', size=20, required=False)
    bgnd14 = fields.Char('bgnd14', size=20, required=False)
    cgnd14 = fields.Char('cgnd14', size=20, required=False)

    cell15 = fields.Char('cell15', size=20, required=False)
    manu15 = fields.Char('manu15', size=20, required=False)
    catn15 = fields.Char('catn15', size=20, required=False)
    max15 = fields.Char('max15', size=20, required=False)
    agnd15 = fields.Char('agnd15', size=20, required=False)
    bgnd15 = fields.Char('bgnd15', size=20, required=False)
    cgnd15 = fields.Char('cgnd15', size=20, required=False)

    cell16 = fields.Char('cell16', size=20, required=False)
    manu16 = fields.Char('manu16', size=20, required=False)
    catn16 = fields.Char('catn16', size=20, required=False)
    max16 = fields.Char('max16', size=20, required=False)
    agnd16 = fields.Char('agnd16', size=20, required=False)
    bgnd16 = fields.Char('bgnd16', size=20, required=False)
    cgnd16 = fields.Char('cgnd16', size=20, required=False)

    cell17 = fields.Char('cell17', size=20, required=False)
    manu17 = fields.Char('manu17', size=20, required=False)
    catn17 = fields.Char('catn17', size=20, required=False)
    max17 = fields.Char('max17', size=20, required=False)
    agnd17 = fields.Char('agnd17', size=20, required=False)
    bgnd17 = fields.Char('bgnd17', size=20, required=False)
    cgnd17 = fields.Char('cgnd17', size=20, required=False)

    cell18 = fields.Char('cell18', size=20, required=False)
    manu18 = fields.Char('manu18', size=20, required=False)
    catn18 = fields.Char('catn18', size=20, required=False)
    max18 = fields.Char('max18', size=20, required=False)
    agnd18 = fields.Char('agnd18', size=20, required=False)
    bgnd18 = fields.Char('bgnd18', size=20, required=False)
    cgnd18 = fields.Char('cgnd18', size=20, required=False)

    cell19 = fields.Char('cell19', size=20, required=False)
    manu19 = fields.Char('manu19', size=20, required=False)
    catn19 = fields.Char('catn19', size=20, required=False)
    max19 = fields.Char('max19', size=20, required=False)
    agnd19 = fields.Char('agnd19', size=20, required=False)
    bgnd19 = fields.Char('bgnd19', size=20, required=False)
    cgnd19 = fields.Char('cgnd19', size=20, required=False)


    cell20 = fields.Char('cell20', size=20, required=False)
    manu20 = fields.Char('manu20', size=20, required=False)
    catn20 = fields.Char('catn20', size=20, required=False)
    max20 = fields.Char('max20', size=20, required=False)
    agnd20 = fields.Char('agnd20', size=20, required=False)
    bgnd20 = fields.Char('bgnd20', size=20, required=False)
    cgnd20 = fields.Char('cgnd20', size=20, required=False)


    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_surge_arrestor, self).fields_view_get(view_id=view_id,
                                                                 view_type=view_type,
                                                                 toolbar=toolbar,
                                                                 submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_surge_arrestor, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
                vals['test_type'] = self._name
        return super(testsheets_surge_arrestor, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_surge_arrestor_action').report_action(self, data=data)

