# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_dry_transformer(models.Model):
    _name = 'component_test.dry_transformer_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Dry transformer'
    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    # Here the specific data of the Test
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Status7 = fields.Char('Status7', size=20, required=False)
    Status8 = fields.Char('Status8', size=20, required=False)
    Status9 = fields.Char('Status9', size=20, required=False)
    Status10 = fields.Char('Status10', size=20, required=False)
    Status11 = fields.Char('Status11', size=20, required=False)
    Status12 = fields.Char('Status12', size=20, required=False)
    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)
    Notes7 = fields.Char('Notes7', size=40, required=False)
    Notes8 = fields.Char('Notes8', size=40, required=False)
    Notes9 = fields.Char('Notes9', size=40, required=False)
    Notes10 = fields.Char('Notes10', size=40, required=False)
    Notes11 = fields.Char('Notes11', size=40, required=False)
    Notes12 = fields.Char('Notes12', size=40, required=False)

    # ********** Transformer Turn Ratio Test*************************
    TapP1 = fields.Char('TapP1', size=40, required=False)
    TapP2 = fields.Char('TapP2', size=40, required=False)
    TapP3 = fields.Char('TapP3', size=40, required=False)
    TapP4 = fields.Char('TapP4', size=40, required=False)
    TapP5 = fields.Char('TapP5', size=40, required=False)
    PriV1 = fields.Integer('PriV1', default=0)
    PriV2 = fields.Integer('PriV2', default=0)
    PriV3 = fields.Integer('PriV3', default=0)
    PriV4 = fields.Integer('PriV4', default=0)
    PriV5 = fields.Integer('PriV5', default=0)
    SecV1 = fields.Char('SecV1', size=20, required=False)
    SecV2 = fields.Char('SecV2', size=20, required=False)
    SecV3 = fields.Char('SecV3', size=20, required=False)
    SecV4 = fields.Char('SecV4', size=20, required=False)
    SecV5 = fields.Char('SecV5', size=20, required=False)
    CalcRatio1 = fields.Float('CalcRatio1', default=0)
    CalcRatio2 = fields.Float('CalcRatio2', default=0)
    CalcRatio3 = fields.Float('CalcRatio3', default=0)
    CalcRatio4 = fields.Float('CalcRatio4', default=0)
    CalcRatio5 = fields.Float('CalcRatio5', default=0)
    H3H1R1 = fields.Float('H3H1R1', default=0)
    H3H1R2 = fields.Float('H3H1R2', default=0)
    H3H1R3 = fields.Float('H3H1R3', default=0)
    H3H1R4 = fields.Float('H3H1R4', default=0)
    H3H1R5 = fields.Float('H3H1R5', default=0)
    H3H1mA1 = fields.Integer('H3H1mA1', default=0)
    H3H1mA2 = fields.Integer('H3H1mA2', default=0)
    H3H1mA3 = fields.Integer('H3H1mA3', default=0)
    H3H1mA4 = fields.Integer('H3H1mA4', default=0)
    H3H1mA5 = fields.Integer('H3H1mA5', default=0)
    H1H2R1 = fields.Float('H1H2R1', default=0)
    H1H2R2 = fields.Float('H1H2R2', default=0)
    H1H2R3 = fields.Float('H1H2R3', default=0)
    H1H2R4 = fields.Float('H1H2R4', default=0)
    H1H2R5 = fields.Float('H1H2R5', default=0)
    H1H2mA1 = fields.Integer('H1H2mA1', default=0)
    H1H2mA2 = fields.Integer('H1H2mA2', default=0)
    H1H2mA3 = fields.Integer('H1H2mA3', default=0)
    H1H2mA4 = fields.Integer('H1H2mA4', default=0)
    H1H2mA5 = fields.Integer('H1H2mA5', default=0)
    H2H3R1 = fields.Float('H2H3R1', default=0)
    H2H3R2 = fields.Float('H2H3R2', default=0)
    H2H3R3 = fields.Float('H2H3R3', default=0)
    H2H3R4 = fields.Float('H2H3R4', default=0)
    H2H3R5 = fields.Float('H2H3R5', default=0)
    H2H3mA1 = fields.Integer('H2H3mA1', default=0)
    H2H3mA2 = fields.Integer('H2H3mA2', default=0)
    H2H3mA3 = fields.Integer('H2H3mA3', default=0)
    H2H3mA4 = fields.Integer('H2H3mA4', default=0)
    H2H3mA5 = fields.Integer('H2H3mA5', default=0)

    # ********** Transformer Surge Arrestor*************************
    TSAManufacturer = fields.Char('TSAManufacturer', size=50, required=False)
    CatalogNo = fields.Char('CatalogNo', size=20, required=False)
    MCOVRating = fields.Char('MCOVRating', size=20, required=False)

    # **********Insulation Resistance (MΩ) (5000 VDC for 1-Minute)*************************
    PhtoGroundA = fields.Integer('PhtoGroundA', default=0)
    PhtoGroundB = fields.Integer('PhtoGroundB', default=0)
    PhtoGroundC = fields.Integer('PhtoGroundC', default=0)

    # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
    TestV1 = fields.Char('TestV1', size=20, required=False)
    TestV2 = fields.Char('TestV2', size=20, required=False)
    TestV3 = fields.Char('TestV3', size=20, required=False)
    TestV4 = fields.Char('TestV4', size=20, required=False)
    TestV5 = fields.Char('TestV5', size=20, required=False)
    Measureat = fields.Char('Measureat', size=20, required=False)
    Reportat = fields.Char('Reportat', size=20, required=False)
    Measured1 = fields.Integer('Measured1', default=0)
    Measured2 = fields.Integer('Measured2', default=0)
    Measured3 = fields.Integer('Measured3', default=0)
    Measured4 = fields.Integer('Measured4', default=0)
    Measured5 = fields.Integer('Measured5', default=0)
    Reported1 = fields.Integer('Reported1', default=0)
    Reported2 = fields.Integer('Reported2', default=0)
    Reported3 = fields.Integer('Reported3', default=0)
    Reported4 = fields.Integer('Reported4', default=0)
    Reported5 = fields.Integer('Reported5', default=0)

    # ********** Primary Winding Resistance*************************
    H1H2Current =    fields.Integer('H1H2Current',    default=0)
    H2H3Current =    fields.Integer('H2H3Current',    default=0)
    H3H1Current =    fields.Integer('H3H1Current',    default=0)
    H1H2Duration =   fields.Integer('H1H2Duration',   default=0)
    H2H3Duration =   fields.Integer('H2H3Duration',   default=0)
    H3H1Duration =   fields.Integer('H3H1Duration',   default=0)
    H1H2Resistance = fields.Integer('H1H2Resistance', default=0)
    H2H3Resistance = fields.Integer('H2H3Resistance', default=0)
    H3H1Resistance = fields.Integer('H3H1Resistance', default=0)

    # ********** Secondary Winding Resistance*************************
    X0X1Current =    fields.Integer('X0X1Current',    default=0)
    X0X2Current =    fields.Integer('X0X2Current',    default=0)
    X0X3Current =    fields.Integer('X0X3Current',    default=0)
    X0X1Duration =   fields.Integer('X0X1Duration',   default=0)
    X0X2Duration =   fields.Integer('X0X2Duration',   default=0)
    X0X3Duration =   fields.Integer('X0X3Duration',   default=0)
    X0X1Resistance = fields.Integer('X0X1Resistance', default=0)
    X0X2Resistance = fields.Integer('X0X2Resistance', default=0)
    X0X3Resistance = fields.Integer('X0X3Resistance', default=0)
    X1X2Current =    fields.Integer('X1X2Current',    default=0)
    X2X3Current =    fields.Integer('X2X3Current',    default=0)
    X3X1Current =    fields.Integer('X3X1Current',    default=0)
    X1X2Duration =   fields.Integer('X1X2Duration',   default=0)
    X2X3Duration =   fields.Integer('X2X3Duration',   default=0)
    X3X1Duration =   fields.Integer('X3X1Duration',   default=0)
    X1X2Resistance = fields.Integer('X1X2Resistance', default=0)
    X2X3Resistance = fields.Integer('X2X3Resistance', default=0)
    X3X1Resistance = fields.Integer('X3X1Resistance', default=0)

    # **********Dissipation Factor & Insulation Capacitance*************************
    DisFactor1 = fields.Char('DisFactor1', size=20, required=False)
    DisFactor2 = fields.Char('DisFactor2', size=20, required=False)
    DisFactor3 = fields.Char('DisFactor3', size=20, required=False)
    DisFactor4 = fields.Char('DisFactor4', size=20, required=False)
    DisFactor5 = fields.Char('DisFactor5', size=20, required=False)
    CapReading1 = fields.Char('CapReading1', size=20, required=False)
    CapReading2 = fields.Char('CapReading2', size=20, required=False)
    CapReading3 = fields.Char('CapReading3', size=20, required=False)
    CapReading4 = fields.Char('CapReading4', size=20, required=False)
    CapReading5 = fields.Char('CapReading5', size=20, required=False)
    Capacitance1 = fields.Char('Capacitance1', size=20, required=False)
    Capacitance2 = fields.Char('Capacitance2', size=20, required=False)
    Capacitance3 = fields.Char('Capacitance3', size=20, required=False)
    Capacitance4 = fields.Char('Capacitance4', size=20, required=False)
    Capacitance5 = fields.Char('Capacitance5', size=20, required=False)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)


    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_dry_transformer, self).fields_view_get(view_id=view_id,
                                                                          view_type=view_type,
                                                                          toolbar=toolbar,
                                                                          submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_dry_transf_action').report_action(self, data=data)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_dry_transformer, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(testsheets_dry_transformer, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)