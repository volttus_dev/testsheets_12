# -*- coding: utf-8 -*-
from odoo import api
from odoo import fields, models

class testsheets_automatic_transferswitch(models.Model):
    _name = 'component_test.automatic_transferswitch_test'
    _inherits = {
        'component_test.order': 'delegated_id'}

    _description = 'Automatic Transfer Switch'

    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    # Here the specific data of the Test
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Status7 = fields.Char('Status7', size=20, required=False)
    Status8 = fields.Char('Status8', size=20, required=False)
    Status9 = fields.Char('Status9', size=20, required=False)
    Status10 = fields.Char('Status10', size=20, required=False)
    Status11 = fields.Char('Status11', size=20, required=False)
    Status12 = fields.Char('Status12', size=20, required=False)
    Status13 = fields.Char('Status13', size=20, required=False)
    Status14 = fields.Char('Status14', size=20, required=False)
    Status15 = fields.Char('Status15', size=20, required=False)
    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)
    Notes7 = fields.Char('Notes7', size=40, required=False)
    Notes8 = fields.Char('Notes8', size=40, required=False)
    Notes9 = fields.Char('Notes9', size=40, required=False)
    Notes10 = fields.Char('Notes10', size=40, required=False)
    Notes11 = fields.Char('Notes11', size=40, required=False)
    Notes12 = fields.Char('Notes12', size=40, required=False)
    Notes13 = fields.Char('Notes13', size=40, required=False)
    Notes14 = fields.Char('Notes14', size=40, required=False)
    Notes15 = fields.Char('Notes15', size=40, required=False)

    Common1 = fields.Char('Common1', size=40, required=False)
    Common2 = fields.Char('Common2', size=40, required=False)
    Common3 = fields.Char('Common3', size=40, required=False)

    # ********** Contact Resistance (µΩ):*************************
    PhNormalA = fields.Float('PhNormalA', default=0)
    PhNormalB = fields.Float('PhNormalB', default=0)
    PhNormalC = fields.Float('PhNormalC', default=0)
    PhNormalN = fields.Float('PhNormalN', default=0)
    PhEmergA = fields.Float('PhEmergA', default=0)
    PhEmergB = fields.Float('PhEmergB', default=0)
    PhEmergC = fields.Float('PhEmergC', default=0)
    PhEmergN = fields.Float('PhEmergN', default=0)


    # ********** Contact Resistance (Bypass switch) (µΩ):*************************
    PhNormalA2 = fields.Float('PhNormalA2', default=0)
    PhNormalB2 = fields.Float('PhNormalB2', default=0)
    PhNormalC2 = fields.Float('PhNormalC2', default=0)
    PhNormalN2 = fields.Float('PhNormalN2', default=0)
    PhEmergA2 = fields.Float('PhEmergA2', default=0)
    PhEmergB2 = fields.Float('PhEmergB2', default=0)
    PhEmergC2 = fields.Float('PhEmergC2', default=0)
    PhEmergN2 = fields.Float('PhEmergN2', default=0)

    # ********** Insulation Resistance as per NETA Specifications (MΩ):*************************
    PhtoPhNAB = fields.Integer('PhtoPhNAB', default=0)
    PhtoPhNBC = fields.Integer('PhtoPhNBC', default=0)
    PhtoPhNCA = fields.Integer('PhtoPhNCA', default=0)

    PhtoGndNA = fields.Integer('PhtoGndNA', default=0)
    PhtoGndNB = fields.Integer('PhtoGndNB', default=0)
    PhtoGndNC = fields.Integer('PhtoGndNC', default=0)

    PhtoPhEAB = fields.Integer('PhtoPhEAB', default=0)
    PhtoPhEBC = fields.Integer('PhtoPhEBC', default=0)
    PhtoPhECA = fields.Integer('PhtoPhECA', default=0)

    PhtoGndEA = fields.Integer('PhtoGndEA', default=0)
    PhtoGndEB = fields.Integer('PhtoGndEB', default=0)
    PhtoGndEC = fields.Integer('PhtoGndEC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)


    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_automatic_transferswitch, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                                                toolbar=toolbar,
                                                                                submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_autimatic_transferswitch_action').report_action(self, data=data)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_automatic_transferswitch, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(testsheets_automatic_transferswitch, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)