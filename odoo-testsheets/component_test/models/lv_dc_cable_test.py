# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields

class lv_dc_cable_test(models.Model):
    _name = 'component_test.lv_dc_cable_test'
    _inherits = {
        'component_test.order': 'delegated_id',
    }
    _description = 'LV DC Cable Test'

    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)

    # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
    PhtoPhAB = fields.Char('PhtoPhAB', size=20, required=False)
    PhtoPhBA = fields.Char('PhtoPhBA', size=20, required=False)
    PhtoPhCA = fields.Char('PhtoPhCA', size=20, required=False)
    PhtoGroundA = fields.Char('PhtoGroundA', size=20, required=False)
    PhtoGroundB = fields.Char('PhtoGroundB', size=20, required=False)
    PhtoGroundC = fields.Char('PhtoGroundC', size=20, required=False)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(lv_dc_cable_test, self).fields_view_get(view_id=view_id,
                                                                     view_type=view_type,
                                                                     toolbar=toolbar,
                                                                     submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(lv_dc_cable_test, self).create(vals)
