# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields

class hv_ac_cable_test(models.Model):
    _name = 'component_test.hv_ac_cable_test'
    _inherits = {
        'component_test.order': 'delegated_id',
    }
    _description = 'HV AC Cable Test'

    # columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Hi pot Test Reading of Leakage Current*************************

    TestVoltageA = fields.Integer('TestVoltageA', default=0)
    WConditionA = fields.Char('WConditionA', size=20, required=False)
    PhaseA1 = fields.Integer('PhaseA1', default=0)
    PhaseA2 = fields.Integer('PhaseA2', default=0)
    PhaseA3 = fields.Integer('PhaseA3', default=0)
    PhaseA4 = fields.Integer('PhaseA4', default=0)
    PhaseA5 = fields.Integer('PhaseA5', default=0)
    PhaseA6 = fields.Integer('PhaseA6', default=0)
    PhaseA7 = fields.Integer('PhaseA7', default=0)

    TestVoltageB = fields.Integer('TestVoltageB', default=0)
    WConditionB = fields.Char('WConditionB', size=20, required=False)
    PhaseB1 = fields.Integer('PhaseB1', default=0)
    PhaseB2 = fields.Integer('PhaseB2', default=0)
    PhaseB3 = fields.Integer('PhaseB3', default=0)
    PhaseB4 = fields.Integer('PhaseB4', default=0)
    PhaseB5 = fields.Integer('PhaseB5', default=0)
    PhaseB6 = fields.Integer('PhaseB6', default=0)
    PhaseB7 = fields.Integer('PhaseB7', default=0)

    TestVoltageC = fields.Integer('TestVoltageA', default=0)
    WConditionC = fields.Char('WConditionA', size=20, required=False)
    PhaseC1 = fields.Integer('PhaseC1', default=0)
    PhaseC2 = fields.Integer('PhaseC2', default=0)
    PhaseC3 = fields.Integer('PhaseC3', default=0)
    PhaseC4 = fields.Integer('PhaseC4', default=0)
    PhaseC5 = fields.Integer('PhaseC5', default=0)
    PhaseC6 = fields.Integer('PhaseC6', default=0)
    PhaseC7 = fields.Integer('PhaseC7', default=0)

    # ********** Voltage Incrememnt*************************
    PhaseA10 = fields.Integer('PhaseA10', default=0)
    PhaseA20 = fields.Integer('PhaseA20', default=0)
    PhaseA30 = fields.Integer('PhaseA30', default=0)
    PhaseA40 = fields.Integer('PhaseA40', default=0)
    PhaseA50 = fields.Integer('PhaseA50', default=0)
    PhaseA60 = fields.Integer('PhaseA60', default=0)

    PhaseB10 = fields.Integer('PhaseB10', default=0)
    PhaseB20 = fields.Integer('PhaseB20', default=0)
    PhaseB30 = fields.Integer('PhaseB30', default=0)
    PhaseB40 = fields.Integer('PhaseB40', default=0)
    PhaseB50 = fields.Integer('PhaseB50', default=0)
    PhaseB60 = fields.Integer('PhaseB60', default=0)

    PhaseC10 = fields.Integer('PhaseC10', default=0)
    PhaseC20 = fields.Integer('PhaseC20', default=0)
    PhaseC30 = fields.Integer('PhaseC30', default=0)
    PhaseC40 = fields.Integer('PhaseC40', default=0)
    PhaseC50 = fields.Integer('PhaseC50', default=0)
    PhaseC60 = fields.Integer('PhaseC60', default=0)


    # ********** Ground Continuiy*************************
    GContinuityA = fields.Char('GContinuityA', size=20, required=False)
    GContinuityB = fields.Char('GContinuityB', size=20, required=False)
    GContinuityC = fields.Char('GContinuityC', size=20, required=False)



    # ********** Voltage discharge level*************************
    PhaseA1s =  fields.Integer('PhaseA1s', default=0)
    PhaseA2s =  fields.Integer('PhaseA2s', default=0)
    PhaseA5s =  fields.Integer('PhaseA5s', default=0)
    PhaseA10s = fields.Integer('PhaseA10s',default=0)
    PhaseA15s = fields.Integer('PhaseA15s',default=0)

    PhaseB1s =  fields.Integer('PhaseB1s', default=0)
    PhaseB2s =  fields.Integer('PhaseB2s', default=0)
    PhaseB5s =  fields.Integer('PhaseB5s', default=0)
    PhaseB10s = fields.Integer('PhaseB10s',default=0)
    PhaseB15s = fields.Integer('PhaseB15s',default=0)

    PhaseC1s =  fields.Integer('PhaseC1s',  default=0)
    PhaseC2s =  fields.Integer('PhaseC2s',  default=0)
    PhaseC5s =  fields.Integer('PhaseC5s',  default=0)
    PhaseC10s = fields.Integer('PhaseC10s', default=0)
    PhaseC15s = fields.Integer('PhaseC15s', default=0)


    # ********** Insulation Resistance (MΩ) as per NETA Specifications*************************
    PhtoPhAB = fields.Integer('PhtoPhAB', default=0)
    PhtoPhBA = fields.Integer('PhtoPhBA', default=0)
    PhtoPhCA = fields.Integer('PhtoPhCA', default=0)
    PhtoGroundA = fields.Integer('PhtoGroundA', default=0)
    PhtoGroundB = fields.Integer('PhtoGroundB', default=0)
    PhtoGroundC = fields.Integer('PhtoGroundC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(hv_ac_cable_test, self).fields_view_get(view_id=view_id,
                                                           view_type=view_type,
                                                           toolbar=toolbar,
                                                           submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(hv_ac_cable_test, self).create(vals)


    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_hvac_cable_action').report_action(self, data=data)

