﻿# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo import api

import time

class test_order(models.Model):
    _name = 'component_test.order'
    _inherits = {'doc_report.doc': 'delegated_id_doc'}
    _description = 'Test Order'

    STATE_SELECTION = [
        ('draft', 'DRAFT'),
        ('released', 'WAITING PARTS'),
        ('ready', 'READY TO MAINTENANCE'),
        ('done', 'DONE'),
        ('cancel', 'CANCELED')
    ]

    @api.v7
    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        data['ids'] = context.get('active_ids', [])
        data['model'] = context.get('active_model', 'ir.ui.menu')
        return self.pool['report'].get_action(cr, uid, [], 'component_test.order', data=data, context=context)

    def _get_parent(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for test in self.browse(cr, uid, ids, context=context):
            for device in self.pool['component.component'].search_read(cr, uid, domain=[['id','=',test.base_component_id.id]], fields=['parent'], context=context):
                result[test.id] = device['parent']
        return result

    def action_get_order_form_view(self):
        for order in self:
            if order.test_type != "any":
                model_test = order.test_type
                def_id = order.id
            else:
                model_test = order.component_model.replace('component.', 'component_test.')+"_test"
                true_component_test = self.pool[model_test]
                def_id = true_component_test.search([('delegated_id', '=', order.id)])[0]


            return {
                'type': 'ir.actions.act_window',
                'res_model': model_test, # this model
                'res_id': def_id, # the current wizard record
                'view_mode': 'form',
                'view_type': 'form',
                'views': [[False, 'form']],
                'target': 'new'
            }

    # columns
    delegated_id_doc = fields.Many2one('doc_report.doc', required=True, ondelete='cascade')
    component_model = fields.Char('Component Model', readonly=True, help="The database object this attachment \n\ "
                                                                              "will be attached to")

    # ********* Component Information ****************************************
    base_component_id = fields.Many2one('component.component', required=False, string='Component', readonly=True)
    component_image = fields.Binary(related='base_component_id.image_medium', readonly=True)
    component_name = fields.Char(related='base_component_id.name', string='Component Name', readonly=True)
    customer_id = fields.Many2one(related='base_component_id.customer_id', string='Customer', relation='res.partner', readonly=True)
    site_id = fields.Many2one(related='base_component_id.site_id', string='Site', relation='component.site', readonly=True)
    location_id = fields.Many2one(related='base_component_id.location_id', string='Location', relation='component.location', readonly=True)
    sublocation_id = fields.Many2one(related='base_component_id.sublocation_id', string='Sublocation', relation='component.sublocation', readonly=True)
    electric_component_type = fields.Selection(related='base_component_id.electric_component_type', string='Electric Component Type', readonly=True)
    Manufacturer = fields.Char(related='base_component_id.manufacturer', string="Manufacturer", readonly=True)

    test_type = fields.Char(string="Test Type", readonly=False, required=True, help="Type of the Test.", default='any')
    parent = fields.Char(compute='_get_parent', string='Parent')

    # ******************constant field reports*****************************
    site_address = fields.Char('Site Address', required=False)
    site_name = fields.Char('Site Name', required=False)
    site_contact_name = fields.Char('Site Contact Name',  required=False)
    site_contact_phone = fields.Char('Site Phone', required=False)

    location_name = fields.Char('Location Name', required=False)
    sublocation_name = fields.Char('Sublocation Name',  required=False)

    _defaults = {
        'date_execution': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        'state': lambda *a: 'done',
        'test_type': 'any'
    }

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        cr, uid, context = self.env.args
        # if 'default_technician' in context:
        #     vals['technician'] = context['default_technician'];

        component_id = vals['base_component_id']
        vals['doc_type'] = 'test';

        for device_component in self.env['component.component'].search([('id', '=', component_id)]):

            ###### Upadate Test Constant Field #########


            vals['site_address'] = device_component.site_id.siteAddress
            vals['site_name'] = device_component.site_id.name
            vals['site_contact_name'] = device_component.site_id.site_contacts.name
            vals['site_contact_phone'] = device_component.site_id.site_contacts.phone

            vals['location_name'] = device_component.location_id.name
            vals['sublocation_name'] = device_component.sublocation_id.name
            #vals['parent11111'] =

            ###### Upadate Doc Constant Field #########
            vals['representative_name'] = device_component.project_id.krka_contacts.name
            vals['representative_phone'] = device_component.project_id.krka_contacts.phone
            vals['customer_name'] = device_component.project_id.company_id.name
            vals['customer_contact_name'] = device_component.project_id.company_contacts.name
            vals['customer_contact_phone'] = device_component.project_id.company_contacts.phone

        return super(test_order, self).create(vals)

    def copy_reload(self):
        for test in self:
            if test.test_type != "any":
                model_test = test.test_type
            else:
                model_test = test.component_model.replace('component.', 'component_test.') + "_test"

            vals = {
                'job_number': test.job_number + ' copy',
                'editable': True,
                'draft': True
            }

            true_component_test = self.env[model_test]
            true_component_test.search([('delegated_id', '=', test.id)]).copy(vals)

# class One2many(fields.One2many):
#
#     def get(self, cr, obj, ids, name, user=None, offset=0, context=None, values=None):
#         if self._context:
#             context = dict(context or {})
#             context.update(self._context)
#         new_ids = []
#         ids_map = {}
#         for device in obj.search_read(cr, user, domain=[['id','in',ids]], fields=['id','delegated_id'], context=context):
#             if ('delegated_id' in device):
#                 new_ids.append(device['delegated_id'][0])
#                 ids_map[device['delegated_id'][0]] = device['id']
#             else:
#                 new_ids.append(device['id'])
#                 ids_map[device['id']]=device['id']
#         # retrieve the records in the comodel
#         comodel = obj.pool[self._obj].browse(cr, user, [], context)
#         inverse = self._fields_id
#         domain = self._domain(obj) if callable(self._domain) else self._domain
#         domain = domain + [(inverse, 'in', new_ids)]
#         records = comodel.search(domain, limit=self._limit)
#
#         result = {id: [] for id in ids}
#         # read the inverse of records without prefetching other fields on them
#         for record in records.with_context(prefetch_fields=False):
#             # record[inverse] may be a record or an integer
#             maped_id = ids_map[int(record[inverse])]
#             result[maped_id].append(record.id)
#
#         return result
#
#     def set(self, cr, obj, id, field, values, user=None, context=None):
#         result = []
#         context = dict(context or {})
#         context.update(self._context)
#         if not values:
#             return
#         obj = obj.pool[self._obj]
#         rec = obj.browse(cr, user, [], context=context)
#         with rec.env.norecompute():
#             _table = obj._table
#             for act in values:
#                 if act[0] == 0:
#                     model = act[2]['component_model']
#                     for device in obj.pool[model].search_read(cr, user, domain=[['id','=',id]], fields=['delegated_id'], context=context):
#                         id = device['delegated_id'][0]
#                         break
#                     act[2][self._fields_id] = id
#                     id_new = obj.create(cr, user, act[2], context=context)
#                     result += obj._store_get_values(cr, user, [id_new], act[2].keys(), context)
#                 elif act[0] == 1:
#                     obj.write(cr, user, [act[1]], act[2], context=context)
#                 elif act[0] == 2:
#                     obj.unlink(cr, user, [act[1]], context=context)
#                 elif act[0] == 3:
#                     inverse_field = obj._fields.get(self._fields_id)
#                     assert inverse_field, 'Trying to unlink the content of a o2m but the pointed model does not have a m2o'
#                     # if the model has on delete cascade, just delete the row
#                     if inverse_field.ondelete == "cascade":
#                         obj.unlink(cr, user, [act[1]], context=context)
#                     else:
#                         cr.execute('update '+_table+' set '+self._fields_id+'=null where id=%s', (act[1],))
#                 # elif act[0] == 4:
#                     # check whether the given record is already linked
#                     # rec = obj.browse(cr, SUPERUSER_ID, act[1], {'prefetch_fields': False})
#                     # if int(rec[self._fields_id]) != id:
#                     #     # Must use write() to recompute parent_store structure if needed and check access rules
#                     #     obj.write(cr, user, [act[1]], {self._fields_id:id}, context=context or {})
#                 elif act[0] == 5:
#                     inverse_field = obj._fields.get(self._fields_id)
#                     assert inverse_field, 'Trying to unlink the content of a o2m but the pointed model does not have a m2o'
#                     # if the o2m has a static domain we must respect it when unlinking
#                     domain = self._domain(obj) if callable(self._domain) else self._domain
#                     extra_domain = domain or []
#                     ids_to_unlink = obj.search(cr, user, [(self._fields_id,'=',id)] + extra_domain, context=context)
#                     # If the model has cascade deletion, we delete the rows because it is the intended behavior,
#                     # otherwise we only nullify the reverse foreign key column.
#                     if inverse_field.ondelete == "cascade":
#                         obj.unlink(cr, user, ids_to_unlink, context=context)
#                     else:
#                         obj.write(cr, user, ids_to_unlink, {self._fields_id: False}, context=context)
#                 elif act[0] == 6:
#                     # Must use write() to recompute parent_store structure if needed
#                     obj.write(cr, user, act[2], {self._fields_id:id}, context=context or {})
#                     ids2 = act[2] or [0]
#                     cr.execute('select id from '+_table+' where '+self._fields_id+'=%s and id <> ALL (%s)', (id,ids2))
#                     ids3 = map(lambda x:x[0], cr.fetchall())
#                     obj.write(cr, user, ids3, {self._fields_id:False}, context=context or {})
#         return result
#
#     def search(self, cr, obj, args, name, value, offset=0, limit=None, uid=None, operator='like', context=None):
#         domain = self._domain(obj) if callable(self._domain) else self._domain
#         return obj.pool[self._obj].name_search(cr, uid, value, domain, operator, context=context,limit=limit)


class electric_component(models.Model):
    _name = 'component.component'
    _inherit = 'component.component'

    # Columns
    test_ids = fields.One2many('component_test.order', 'base_component_id', string='Tests')

    def action_get_test_tree_view(self):
        ctx = dict()
        def_location = False
        def_sublocation = False
        for component in self:
            if 'delegated_id' in component:
                def_id = component['delegated_id'].id
                def_location = component.location_id
                def_sublocation = component.sublocation_id
                ctx = self.get_record_as_default(self._cr, self._uid, component)
            else:
                true_components = self.env[component.component_model]
                domain = [('delegated_id', '=', component.id)]
                for rec in true_components.search(domain):
                    def_location = rec.location_id
                    def_sublocation = rec.sublocation_id
                    ctx = self.get_record_as_default(self._cr, self._uid, true_components, rec)
                def_id = component.id
            ctx.update({
                # 'default_delegated_id': def_id,
                'default_component_id': def_id,
                'default_base_component_id': def_id,
                'default_location_id': def_location.id,
                'default_sublocation_id': def_sublocation.id,
                'default_technician': self._uid,
                'default_id': False,
            })
            actions = self.env['ir.actions.act_window']
            for action in actions.search_read(domain=[['res_model','=','component_test.order']], fields=[]):
                result = {
                    'name': action['name'],
                    'help': action['help'],
                    'type': action['type'],
                    'view_type': action['view_type'],
                    'view_mode': action['view_mode'],
                    'view_id': self.env.ref('component_test.test_sheets_order_edit_tree').id,
                    'target': action['target'],
                    'context': ctx,
                    'domain': str([('base_component_id', '=', def_id)]),
                    'res_model': action['res_model'],
                }
                return result



