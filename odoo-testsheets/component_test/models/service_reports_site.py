# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields
from datetime import datetime, timedelta

from lxml import etree
import simplejson  #If not installed, you have to install it by executing pip install simplejson
# from openerp.osv.orm import setup_modifiers


class service_reports_site(models.Model):
    _name = 'service_reports.site'
    _inherits = {'service_reports.order': 'delegated_id'}
    _description = 'Site Service Reports'

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(service_reports_site, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    def action_get_order_form_view(self):
        for order in self:
            if order.service_reports_type != "any":
                model = order.service_reports_type
                def_id = order.id
            else:
                model = order.component_model.replace('component.', 'component_test.')+"_test"
                true_component = self.env[model]
                def_id = true_component.search([('delegated_id', '=', order.id)])[0]

            return {
                'type': 'ir.actions.act_window',
                'res_model': model, # this model
                'res_id': def_id, # the current wizard record
                'view_mode': 'form',
                'view_type': 'form',
                'views': [[False, 'form']],
                # 'target': 'new'
            }


    def _total(self):
        datetimeFormat = '%Y-%m-%d %H:%M:%S'
        hours = 0.0
        days = 0
        seconds = 0
        for rec in self:
            if rec.ArriveA2 and rec.LeaveA2:
                date_final = datetime.strptime(rec.ArriveA2, datetimeFormat)
                date_inicial = datetime.strptime(rec.LeaveA2, datetimeFormat)
                timedelta = date_final - date_inicial
                days += timedelta.days
                seconds += timedelta.seconds
            if rec.ArriveB2 and rec.LeaveB2:
                date_final = datetime.strptime(rec.ArriveB2, datetimeFormat)
                date_inicial = datetime.strptime(rec.LeaveB2, datetimeFormat)
                timedelta = date_final - date_inicial
                days += timedelta.days
                seconds += timedelta.seconds
            if rec.ArriveC2 and rec.LeaveC2:
                date_final = datetime.strptime(rec.ArriveC2, datetimeFormat)
                date_inicial = datetime.strptime(rec.LeaveC2, datetimeFormat)
                timedelta = date_final - date_inicial
                days += timedelta.days
                seconds += timedelta.seconds
            if rec.ArriveD2 and rec.LeaveD2:
                date_final = datetime.strptime(rec.ArriveD2, datetimeFormat)
                date_inicial = datetime.strptime(rec.LeaveD2, datetimeFormat)
                timedelta = date_final - date_inicial
                days += timedelta.days
                seconds += timedelta.seconds
            if rec.ArriveE2 and rec.LeaveE2:
                date_final = datetime.strptime(rec.ArriveE2, datetimeFormat)
                date_inicial = datetime.strptime(rec.LeaveE2, datetimeFormat)
                timedelta = date_final - date_inicial
                days += timedelta.days
                seconds += timedelta.seconds
        hours += ((float)(days*86400 + seconds))/3600
        self.total = hours

    def _str_total(self):
        strtotal = "00:00"
        for rec in self:
            hours = int(rec.total)
            m = int((rec.total-hours)*60)
            strtotal = str(hours) + ":" + str(m)
        self.str_total = strtotal

    # Columns
    delegated_id = fields.Many2one('service_reports.order', required=True, ondelete='cascade')

    # ********** Description of Work*************************
    Resolution = fields.Text('Resolution:', required=False)

    # ********** Service Performed*************************
    Service = fields.Text('Service:', required=False)

    # ********** Service Coordinator*************************
    SCoordinatorA = fields.Char('SCoordinatorA', size=20, required=False)
    SCoordinatorB = fields.Char('SCoordinatorB', size=20, required=False)
    SCoordinatorC = fields.Char('SCoordinatorC', size=20, required=False)
    SCoordinatorD = fields.Char('SCoordinatorD', size=20, required=False)
    SCoordinatorE = fields.Char('SCoordinatorE', size=20, required=False)
    LeaveA1 = fields.Float('LeaveA1', digits=(16,2), required=False)
    LeaveA2 = fields.Datetime('LeaveA2', required=False)
    LeaveB1 = fields.Float('LeaveB1', digits=(16,2), required=False)
    LeaveB2 = fields.Datetime('LeaveB2', required=False)
    LeaveC1 = fields.Float('LeaveC1', digits=(16,2), required=False)
    LeaveC2 = fields.Datetime('LeaveC2', required=False)
    LeaveD1 = fields.Float('LeaveD1', digits=(16,2), required=False)
    LeaveD2 = fields.Datetime('LeaveD2', required=False)
    LeaveE1 = fields.Float('LeaveE1', digits=(16,2), required=False)
    LeaveE2 = fields.Datetime('LeaveE2', required=False)
    ArriveA1 = fields.Float('ArriveA1', digits=(16,2), required=False)
    ArriveA2 = fields.Datetime('ArriveA2', required=False)
    ArriveB1 = fields.Float('ArriveB1', digits=(16,2), required=False)
    ArriveB2 = fields.Datetime('ArriveB2', required=False)
    ArriveC1 = fields.Float('ArriveC1', digits=(16,2), required=False)
    ArriveC2 = fields.Datetime('ArriveC2', required=False)
    ArriveD1 = fields.Float('ArriveD1', digits=(16,2), required=False)
    ArriveD2 = fields.Datetime('ArriveD2', required=False)
    ArriveE1 = fields.Float('ArriveE1', digits=(16,2), required=False)
    ArriveE2 = fields.Datetime('ArriveE2', required=False)
    STA = fields.Float('STA', digits=(16,2), required=False)
    STB = fields.Float('STB', digits=(16,2), required=False)
    STC = fields.Float('STC', digits=(16,2), required=False)
    STD = fields.Float('STD', digits=(16,2), required=False)
    STE = fields.Float('STE', digits=(16,2), required=False)
    OTA = fields.Float('OTA', digits=(16,2), required=False)
    OTB = fields.Float('OTB', digits=(16,2), required=False)
    OTC = fields.Float('OTC', digits=(16,2), required=False)
    OTD = fields.Float('OTD', digits=(16,2), required=False)
    OTE = fields.Float('OTE', digits=(16,2), required=False)
    DTA = fields.Float('DTA', digits=(16,2), required=False)
    DTB = fields.Float('DTB', digits=(16,2), required=False)
    DTC = fields.Float('DTC', digits=(16,2), required=False)
    DTD = fields.Float('DTD', digits=(16,2), required=False)
    DTE = fields.Float('DTE', digits=(16,2), required=False)
    TKm = fields.Float('TKm', digits=(16,2), required=False)
    THs = fields.Float('THs', digits=(16,2), required=False)
    total = fields.Float(compute='_total', string='Total')
    str_total = fields.Char(compute='_str_total', string='Total')

    # ********** Notes*************************
    Note = fields.Text('Note:', required=False)

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        if not vals.get('service_reports_type') and 'service_reports_type' not in self._context:
            vals['service_reports_type'] = self._name
        return super(service_reports_site, self).create(vals)


    @api.model
    def check_report(self, data=None):
        # return self.env['report'].get_action(self, 'component_test.service_reports_site')
        """Redirects to the report with the values obtained from the wizard
                        'data['form']': name of employee and the date duration"""
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_site_service_reports_action').report_action(self, data=data)


    #@api.returns('self', lambda value: value.id)
    def copy_reload(self):
        for test in self:
            vals = {
                'job_number': test.job_number + ' copy'
            }
            super(service_reports_site, self).copy(vals)

        return {
            'type': 'ir.actions.client',
            'tag': 'reload'
        }


