# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_lvdc_cb(models.Model):
    _name = 'component_test.lvdc_cb_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'LV DC Circuit Breaker'

    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')

    # ********** Mechanical & Electrical Inspection*************************
    Status1 = fields.Char('Status1', size=20, required=False)
    Status2 = fields.Char('Status2', size=20, required=False)
    Status3 = fields.Char('Status3', size=20, required=False)
    Status4 = fields.Char('Status4', size=20, required=False)
    Status5 = fields.Char('Status5', size=20, required=False)
    Status6 = fields.Char('Status6', size=20, required=False)
    Status7 = fields.Char('Status7', size=20, required=False)
    Status8 = fields.Char('Status8', size=20, required=False)
    Status9 = fields.Char('Status9', size=20, required=False)
    Status10 = fields.Char('Status10', size=20, required=False)
    Status11 = fields.Char('Status11', size=20, required=False)
    Status12 = fields.Char('Status12', size=20, required=False)
    Status13 = fields.Char('Status13', size=20, required=False)
    Status14 = fields.Char('Status14', size=20, required=False)
    Status15 = fields.Char('Status15', size=20, required=False)
    Status16 = fields.Integer('Status16', default=0) # Counter as found

    Notes1 = fields.Char('Notes1', size=40, required=False)
    Notes2 = fields.Char('Notes2', size=40, required=False)
    Notes3 = fields.Char('Notes3', size=40, required=False)
    Notes4 = fields.Char('Notes4', size=40, required=False)
    Notes5 = fields.Char('Notes5', size=40, required=False)
    Notes6 = fields.Char('Notes6', size=40, required=False)
    Notes7 = fields.Char('Notes7', size=40, required=False)
    Notes8 = fields.Char('Notes8', size=40, required=False)
    Notes9 = fields.Char('Notes9', size=40, required=False)
    Notes10 = fields.Char('Notes10', size=40, required=False)
    Notes11 = fields.Char('Notes11', size=40, required=False)
    Notes12 = fields.Char('Notes12', size=40, required=False)
    Notes13 = fields.Char('Notes13', size=40, required=False)
    Notes14 = fields.Char('Notes14', size=40, required=False)
    Notes15 = fields.Char('Notes15', size=40, required=False)
    Notes16 = fields.Integer('Notes16', default=0) # As Left

    # **********Relay Test & Settings:*************************
    RelayType = fields.Char('RelayType', size=40, required=False)
    RelaySerial = fields.Char('RelaySerial', size=40, required=False)
    RPlug = fields.Char('RPlug', size=40, required=False)
    IG = fields.Char('IG', size=40, required=False)
    PreTrip = fields.Char('PreTrip', size=40, required=False)
    TestEqp = fields.Char('TestEqp', size=40, required=False)
    TypeofInjection = fields.Char('TypeofInjection', size=40, required=False)

    Rtest1 = fields.Char('Rtest1', size=20, required=False)
    Rtest2 = fields.Char('Rtest2', size=20, required=False)
    Rtest3 = fields.Char('Rtest3', size=20, required=False)
    Rtest4 = fields.Char('Rtest4', size=20, required=False)
    Rtest5 = fields.Char('Rtest5', size=20, required=False)
    Rtest6 = fields.Char('Rtest6', size=20, required=False)
    Rtest7 = fields.Char('Rtest7', size=20, required=False)
    Rtest8 = fields.Char('Rtest8', size=20, required=False)
    Rtest9 = fields.Char('Rtest9', size=20, required=False)
    Rtest10 = fields.Char('Rtest10', size=20, required=False)
    Rtest11 = fields.Char('Rtest11', size=20, required=False)
    Rtest12 = fields.Char('Rtest12', size=20, required=False)
    Rtest13 = fields.Char('Rtest13', size=20, required=False)
    Rtest14 = fields.Char('Rtest14', size=20, required=False)
    Rtest15 = fields.Char('Rtest15', size=20, required=False)

    Rtest16 = fields.Char('Rtest16', size=40, required=False)
    Rtest17 = fields.Char('Rtest17', size=40, required=False)
    Rtest18 = fields.Char('Rtest18', size=40, required=False)
    Rtest19 = fields.Char('Rtest19', size=40, required=False)
    Rtest20 = fields.Char('Rtest20', size=40, required=False)
    Rtest21 = fields.Char('Rtest21', size=40, required=False)
    Rtest22 = fields.Char('Rtest22', size=40, required=False)
    Rtest23 = fields.Char('Rtest23', size=40, required=False)
    Rtest24 = fields.Char('Rtest24', size=40, required=False)
    Rtest25 = fields.Char('Rtest25', size=40, required=False)
    Rtest26 = fields.Char('Rtest26', size=40, required=False)
    Rtest27 = fields.Char('Rtest27', size=40, required=False)
    Rtest28 = fields.Char('Rtest28', size=40, required=False)
    Rtest29 = fields.Char('Rtest29', size=40, required=False)
    Rtest30 = fields.Char('Rtest30', size=40, required=False)

    Rtest31 = fields.Char('Rtest31', size=40, required=False)
    Rtest32 = fields.Char('Rtest32', size=40, required=False)
    Rtest33 = fields.Char('Rtest33', size=40, required=False)
    Rtest34 = fields.Char('Rtest34', size=40, required=False)
    Rtest35 = fields.Char('Rtest35', size=40, required=False)
    Rtest36 = fields.Char('Rtest36', size=40, required=False)
    Rtest37 = fields.Char('Rtest37', size=40, required=False)
    Rtest38 = fields.Char('Rtest38', size=40, required=False)
    Rtest39 = fields.Char('Rtest39', size=40, required=False)
    Rtest40 = fields.Char('Rtest40', size=40, required=False)
    Rtest41 = fields.Char('Rtest41', size=40, required=False)
    Rtest42 = fields.Char('Rtest42', size=40, required=False)
    Rtest43 = fields.Char('Rtest43', size=40, required=False)
    Rtest44 = fields.Char('Rtest44', size=40, required=False)
    Rtest45 = fields.Char('Rtest45', size=40, required=False)
    Rtest46 = fields.Char('Rtest46', size=40, required=False)
    Rtest47 = fields.Char('Rtest47', size=40, required=False)
    Rtest48 = fields.Char('Rtest48', size=40, required=False)
    Rtest49 = fields.Char('Rtest49', size=40, required=False)
    Rtest50 = fields.Char('Rtest50', size=40, required=False)
    Rtest51 = fields.Char('Rtest51', size=40, required=False)
    Rtest52 = fields.Char('Rtest52', size=40, required=False)
    Rtest53 = fields.Char('Rtest53', size=40, required=False)
    Rtest54 = fields.Char('Rtest54', size=40, required=False)
    Rtest55 = fields.Char('Rtest55', size=40, required=False)
    Rtest56 = fields.Char('Rtest56', size=40, required=False)
    Rtest57 = fields.Char('Rtest57', size=40, required=False)
    Rtest58 = fields.Char('Rtest58', size=40, required=False)
    Rtest59 = fields.Char('Rtest59', size=40, required=False)
    Rtest60 = fields.Char('Rtest60', size=40, required=False)
    Rtest61 = fields.Char('Rtest61', size=40, required=False)
    Rtest62 = fields.Char('Rtest62', size=40, required=False)
    Rtest63 = fields.Char('Rtest63', size=40, required=False)

    # ********** Contact Resistance (µΩ):*************************
    PhaseA = fields.Char('PhaseA', size=40, required=False)
    PhaseB = fields.Char('PhaseB', size=40, required=False)
    PhaseC = fields.Char('PaseC', size=40, required=False)
    FuseA = fields.Integer('FuseA', default=0)
    FuseB = fields.Integer('FuseB', default=0)
    FuseC = fields.Integer('FuseC', default=0)

    # **********Insulation Resistance (5000VDC(MΩ))*************************
    PhtoPhAB = fields.Char('PhtoPhAB', size=40, required=False)
    PhtoPhBC = fields.Char('PhtoPhBC', size=40, required=False)
    PhtoPhCA = fields.Char('PhtoPhCA', size=40, required=False)
    PhtoGndA = fields.Integer('PhtoGndA', default=0)
    PhtoGndB = fields.Integer('PhtoGndB', default=0)
    PhtoGndC = fields.Integer('PhtoGndC', default=0)

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_lvdc_cb, self).fields_view_get(view_id=view_id,
                                                             view_type=view_type,
                                                             toolbar=toolbar,
                                                             submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_lvdc_cb, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        return super(testsheets_lvdc_cb, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def check_report(self, data=None):
        rec = self.browse(data)
        data = {}
        # data['form'] = rec.read(['location'])

        return self.env.ref('component_test.report_lvdc_circuitbreaker_action').report_action(self, data=data)
