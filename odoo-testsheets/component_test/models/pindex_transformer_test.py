# -*- coding: utf-8 -*-
from odoo import api
from odoo import models, fields


class testsheets_pindex_transformer(models.Model):
    _name = 'component_test.pindex_transformer_test'
    _inherits = {'component_test.order': 'delegated_id'}

    _description = 'Transformer Polarization Index'

    def _hp_index(self, cr, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, "")
        for rec in self.browse(cr, uid, ids, context):
            value = float(rec.HTime13)
            value /= float(rec.HTime4)
            res[rec.ids[0]] = ' = ' + str(round(value, 2))
        return res

    def _xp_index(self, cr, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, "")
        for rec in self.browse(cr, uid, ids, context):
            value = float(rec.XTime13)
            value /= float(rec.XTime4)
            res[rec.ids[0]] = ' = ' + str(round(value, 2))
        return res

    # Columns
    delegated_id = fields.Many2one('component_test.order', required=True, ondelete='cascade')
    component_id = fields.Many2one('component.pindex_transformer', required=True, string='Component inherited', readonly=True)

    # ********** Equipment Data. *************************
    # This is exactly a relation to all fields in
    # Component.electric_component_pindextransformer
    HV = fields.Char(realted='component_id.HV', string='HV', readonly=True)
    LV = fields.Char(realted='component_id.LV', string='LV', readonly=True)
    PowerRating = fields.Char(related='component_id.PowerRating', string='PowerRating', readonly=True)
    Frequency = fields.Char(realted='component_id.Frequency', string='Frequency', readonly=True)
    Serial = fields.Char(realted='component_id.serial', string='Serial', readonly=True)
    Type = fields.Char(related='component_id.Type', string='Type', readonly=True)

    MFGDate = fields.Date(related='component_id.MFGDate', string='MFGDate', readonly=True)
    Phase = fields.Char(related='component_id.Phase', string='Phase', readonly=True)
    Tap = fields.Char(related='component_id.Tap', string='Tap', readonly=True)
    TCooling = fields.Char(related='component_id.TCooling', string='TCooling', readonly=True)
    Impedance = fields.Char(related='component_id.Impedance', string='Impedance', readonly=True)
    ImpTemp = fields.Char(related='component_id.ImpTemp', string='ImpTemp', readonly=True)
    TempRise = fields.Char(related='component_id.TempRise', string='TempRise', readonly=True)
    WindingConf = fields.Char(related='component_id.WindingConf', string='WindingConf', readonly=True)
    Style = fields.Char(related='component_id.Style', string='Style', readonly=True)
    Equip_Designation = fields.Char(related='component_id.Equip_Designation', string='Equip_Designation', readonly=True)


    # ********** Test results*************************
    # Here the specific data of the Test
    HTime1 = fields.Char('HTime1', size=20, required=False)
    HTime2 = fields.Char('HTime2', size=20, required=False)
    HTime3 = fields.Char('HTime1', size=20, required=False)
    HTime4 = fields.Char('HTime2', size=20, required=False)
    HTime5 = fields.Char('HTime1', size=20, required=False)
    HTime6 = fields.Char('HTime2', size=20, required=False)
    HTime7 = fields.Char('HTime1', size=20, required=False)
    HTime8 = fields.Char('HTime2', size=20, required=False)
    HTime9 = fields.Char('HTime1', size=20, required=False)
    HTime10 = fields.Char('HTime2', size=20, required=False)
    HTime11 = fields.Char('HTime1', size=20, required=False)
    HTime12 = fields.Char('HTime2', size=20, required=False)
    HTime13 = fields.Char('HTime1', size=20, required=False)

    XTime1 = fields.Char('XTime1', size=20, required=False)
    XTime2 = fields.Char('XTime2', size=20, required=False)
    XTime3 = fields.Char('XTime1', size=20, required=False)
    XTime4 = fields.Char('XTime2', size=20, required=False)
    XTime5 = fields.Char('XTime1', size=20, required=False)
    XTime6 = fields.Char('XTime2', size=20, required=False)
    XTime7 = fields.Char('XTime1', size=20, required=False)
    XTime8 = fields.Char('XTime2', size=20, required=False)
    XTime9 = fields.Char('XTime1', size=20, required=False)
    XTime10 = fields.Char('XTime2', size=20, required=False)
    XTime11 = fields.Char('XTime1', size=20, required=False)
    XTime12 = fields.Char('XTime2', size=20, required=False)
    XTime13 = fields.Char('XTime1', size=20, required=False)

    HPIndex = fields.Char(compute='_hp_index', string='HPIndex')
    XPIndex = fields.Char(compute='_xp_index', string='XPIndex')

    # ********** Notes & Comments*************************
    Comments = fields.Text('Comments', required=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        body = super(testsheets_pindex_transformer, self).fields_view_get(view_id=view_id,
                                                                     view_type=view_type,
                                                                     toolbar=toolbar,
                                                                     submenu=submenu)
        return self.env['doc_report.doc'].set_readonly_form(body)

    @api.multi
    def write(self, vals):
        # if 'delegated_id' in vals:
        #     vals['component_id'] = vals['delegated_id']
        res = super(testsheets_pindex_transformer, self).write(vals)
        return res

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        if not vals.get('test_type') and 'test_type' not in self._context:
            vals['test_type'] = self._name
        # self = self.with_context(alias_model_name='hr.equipment.request', alias_parent_model_name=self._name)
        return super(testsheets_pindex_transformer, self).create(vals)
        # category_id.alias_id.write({'alias_parent_thread_id': category_id.id, 'alias_defaults': {'category_id': category_id.id}})
        # return category_id

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('component_test.order')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
        }
        return report_obj.render('component_test.order', docargs)