# -*- coding: utf-8 -*-
from odoo import models, fields
from odoo import api


class service_reports_order(models.Model):
    _name = 'service_reports.order'
    _inherits = {'doc_report.doc': 'delegated_id_doc'}
    _description = 'Services Report Order'

    STATE_SELECTION = [
        ('draft', 'DRAFT'),
        ('released', 'WAITING PARTS'),
        ('ready', 'READY TO MAINTENANCE'),
        ('done', 'DONE'),
        ('cancel', 'CANCELED')
    ]

    @api.v7
    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        data['ids'] = context.get('active_ids', [])
        data['model'] = context.get('active_model', 'ir.ui.menu')
        return self.pool['report'].get_action(cr, uid, [], 'service_reports.order', data=data, context=context)


    def action_get_order_form_view(self):
        for order in self:
            if order.service_reports_type != "any":
                model = order.service_reports_type
                true_component = self.env[model]
                def_id = true_component.search([('delegated_id', '=', order.id)])[0].id
            else:
                model = order.component_model.replace('component.', 'component_test.')+"_test"
                true_component = self.env[model]
                def_id = true_component.search([('delegated_id', '=', order.id)])[0].id

            return {
                'type': 'ir.actions.act_window',
                'res_model': model, # this model
                'res_id': def_id, # the current wizard record
                'view_mode': 'form',
                'view_type': 'form',
                'views': [[False, 'form']],
                'target': 'new'
            }

    # Columns
    delegated_id_doc = fields.Many2one('doc_report.doc', required=True, ondelete='cascade')

    # ********* Site Information ****************************************
    base_site_id = fields.Many2one('component.site', required=False, string='Site', readonly=True)
    site_name = fields.Char(related='base_site_id.name', string='Site Name', readonly=True)
    project_id = fields.Many2one(related='base_site_id.project_id', string='Project', relation='component.project', readonly=True)

    parent = fields.Char(compute='_get_parent', string='Parent')
    service_reports_type = fields.Char(string="Services Report Type", readonly=False, required=True, help="Type of the Services Report.")

    # ******************constant field reports*****************************
    site_address = fields.Char('Site Address', required=False)
    site_contact_name = fields.Char('Site Contact Name',  required=False)
    site_contact_phone = fields.Char('Site Phone', required=False)

    _defaults = {
        # 'date_execution': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
        # 'state': lambda *a: 'done',
        'service_report_type': 'any'
    }

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        cr, uid, context = self.env.args
        # if 'default_technician' in context:
        #     vals['technician'] = context['default_technician']
        # else:
        #     vals['technician'] = uid
        vals['doc_type'] = 'service';
        return super(service_reports_order, self).create(vals)

    def copy_reload(self):
        for test in self:
            if test.service_reports_type != "any":
                model = test.service_reports_type
            else:
                model = test.component_model.replace('component.', 'component_test.')+"_test"

            vals = {
                'job_number': test.job_number + ' copy',
                'editable': True,
                'draft': True
            }

            true_component = self.env[model]
            true_component.search([('delegated_id', '=', test.id)]).copy(vals)

        return {
            'type': 'ir.actions.client',
            'tag': 'reload'
        }

    def move(self, site_id):
        return self.write({'base_site_id': site_id})

class One2many(fields.One2many):

    def get(self, cr, obj, ids, name, user=None, offset=0, context=None, values=None):
        if self._context:
            context = dict(context or {})
            context.update(self._context)
        new_ids = []
        ids_map = {}
        for report in obj.search_read(cr, user, domain=[['id','in',ids]], fields=['id','delegated_id'], context=context):
            if ('delegated_id' in report):
                new_ids.append(report['delegated_id'][0])
                ids_map[report['delegated_id'][0]] = report['id']
            else:
                new_ids.append(report['id'])
                ids_map[report['id']]=report['id']
        # retrieve the records in the comodel
        comodel = obj.pool[self._obj].browse(cr, user, [], context)
        inverse = self._fields_id
        domain = self._domain(obj) if callable(self._domain) else self._domain
        domain = domain + [(inverse, 'in', new_ids)]
        records = comodel.search(domain, limit=self._limit)

        result = {id: [] for id in ids}
        # read the inverse of records without prefetching other fields on them
        for record in records.with_context(prefetch_fields=False):
            # record[inverse] may be a record or an integer
            maped_id = ids_map[int(record[inverse])]
            result[maped_id].append(record.id)

        return result

    def set(self, cr, obj, id, field, values, user=None, context=None):
        result = []
        context = dict(context or {})
        context.update(self._context)
        if not values:
            return
        obj = obj.pool[self._obj]
        rec = obj.browse(cr, user, [], context=context)
        with rec.env.norecompute():
            _table = obj._table
            for act in values:
                if act[0] == 0:
                    model = act[2]['component_model']
                    for report in obj.pool[model].search_read(cr, user, domain=[['id','=',id]], fields=['delegated_id'], context=context):
                        id = report['delegated_id'][0]
                        break
                    act[2][self._fields_id] = id
                    id_new = obj.create(cr, user, act[2], context=context)
                    result += obj._store_get_values(cr, user, [id_new], act[2].keys(), context)
                elif act[0] == 1:
                    obj.write(cr, user, [act[1]], act[2], context=context)
                elif act[0] == 2:
                    obj.unlink(cr, user, [act[1]], context=context)
                elif act[0] == 3:
                    inverse_field = obj._fields.get(self._fields_id)
                    assert inverse_field, 'Trying to unlink the content of a o2m but the pointed model does not have a m2o'
                    # if the model has on delete cascade, just delete the row
                    if inverse_field.ondelete == "cascade":
                        obj.unlink(cr, user, [act[1]], context=context)
                    else:
                        cr.execute('update '+_table+' set '+self._fields_id+'=null where id=%s', (act[1],))
                # elif act[0] == 4:
                    # check whether the given record is already linked
                    # rec = obj.browse(cr, SUPERUSER_ID, act[1], {'prefetch_fields': False})
                    # if int(rec[self._fields_id]) != id:
                    #     # Must use write() to recompute parent_store structure if needed and check access rules
                    #     obj.write(cr, user, [act[1]], {self._fields_id:id}, context=context or {})
                elif act[0] == 5:
                    inverse_field = obj._fields.get(self._fields_id)
                    assert inverse_field, 'Trying to unlink the content of a o2m but the pointed model does not have a m2o'
                    # if the o2m has a static domain we must respect it when unlinking
                    domain = self._domain(obj) if callable(self._domain) else self._domain
                    extra_domain = domain or []
                    ids_to_unlink = obj.search(cr, user, [(self._fields_id,'=',id)] + extra_domain, context=context)
                    # If the model has cascade deletion, we delete the rows because it is the intended behavior,
                    # otherwise we only nullify the reverse foreign key column.
                    if inverse_field.ondelete == "cascade":
                        obj.unlink(cr, user, ids_to_unlink, context=context)
                    else:
                        obj.write(cr, user, ids_to_unlink, {self._fields_id: False}, context=context)
                elif act[0] == 6:
                    # Must use write() to recompute parent_store structure if needed
                    obj.write(cr, user, act[2], {self._fields_id:id}, context=context or {})
                    ids2 = act[2] or [0]
                    cr.execute('select id from '+_table+' where '+self._fields_id+'=%s and id <> ALL (%s)', (id,ids2))
                    ids3 = map(lambda x:x[0], cr.fetchall())
                    obj.write(cr, user, ids3, {self._fields_id:False}, context=context or {})
        return result

    def search(self, cr, obj, args, name, value, offset=0, limit=None, uid=None, operator='like', context=None):
        domain = self._domain(obj) if callable(self._domain) else self._domain
        return obj.pool[self._obj].name_search(cr, uid, value, domain, operator, context=context,limit=limit)

#
# class model_with_image(osv.osv):
#     _name = 'component.imaged'
#
# class model_with_sld(model_with_image):
#     _name = 'component.sld'
#     _inherit = 'component.imaged'
#
# class site(model_with_sld):
#     _name = 'component.site'
#     _inherit = ['component.sld']
#     _columns = {
#         'services_reports_ids': one2many('report_services.order', 'base_site_id', string='Report Services'),
#     }
