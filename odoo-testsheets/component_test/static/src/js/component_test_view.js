odoo.define('component_test.order', function (require) {
    'use strict';

   var core = require('web.core');
   var QWeb = core.qweb;
   var _t = core._t;
   var Dialog = require('web.Dialog');
   var WebDialog = require('web.view_dialogs');
   var ListViewController = require('web.ListController');
   var ListViewRenderer = require('web.ListRenderer');
   //var ListRenderer = require('web.ListRenderer');
   var FormController = require('web.FormController');

   ListViewController.include({
        renderButtons: function ($node) {
           this._super.apply(this, arguments);

           if (this.modelName.startsWith('component_test.')) {
               this.$buttons.find('.o_button_import').hide();

               if (this.initialState.context["default_base_component_id"] != undefined){
                 var self = this;
                 this._rpc({
                    model: this.modelName,
                    method: 'check_access_rights',
                    args: ["create", true],
                 }).then(function (create_right) {
                    if (create_right){
                       self.$buttons.find('.o_list_button_add').show();
                       self.$buttons.find('button.o_btn_import_excel').show();

                       //Adicionar el flujo del boton de importar el excel
                       var import_excel_btn = self.$buttons.find('button.o_btn_import_excel');
                       import_excel_btn.on('click', self.proxy('show_import_excel_component_dialog'));
                    }else{
                       self.$buttons.find('button.o_btn_import_excel').hide();
                       self.$buttons.find('.o_list_button_add').hide();
                    }
                 });
               }else{
                 this.$buttons.find('.o_list_button_add').hide();
                 this.$buttons.find('button.o_btn_import_excel').hide();
               }
           }

           if (this.modelName.startsWith('service_reports.')){
             this.$buttons.find('.o_button_import').hide();
             var self = this;
             this._rpc({
                model: this.modelName,
                method: 'check_access_rights',
                args: ["create", true],
             }).then(function (create_right) {
                if (create_right){
                   self.$buttons.find('.o_list_button_add').show();
                   self.$buttons.find('button.o_btn_import_excel').show();

                   //Adicionar el flujo del boton de importar el excel
                   var import_excel_btn = self.$buttons.find('button.o_btn_import_excel');
                   import_excel_btn.on('click', self.proxy('show_import_excel_site_dialog'));
                }else{
                   self.$buttons.find('.o_list_button_add').hide();
                   self.$buttons.find('button.o_btn_import_excel').hide();
                }
             });
           }
        },

        show_import_excel_component_dialog: function(){
          if (this.modelName == 'component_test.order') {
                this.$('table:first').show();
                this.$('.oe_view_nocontent').remove();
                var self = this;
                var component_id = this.initialState.context.default_base_component_id;
                this.select_component_test_model_and_file(component_id, function (dialog) {
                    //self.do_switch_view(self.selected_test_model, self.initialState.context);
                });
            }
        },

        show_import_excel_site_dialog: function(){
          alert('UFF al fin');
        },

        showSiteDialog: function(report_id, doit){
            var self = this;
            this._rpc({
               model: 'component.site',
               method: 'search_read',
               fields: ['id', 'name'],
            }).then(function(sites){
                if (sites.length){
                    var buttons = [
                        {
                            text: _t("Ok"),
                            classes: 'btn-primary',
                            close: true,
                            click: doit
                        },
                        {
                            text: _t("Cancel"),
                            close: true,
                            click: options && options.cancel_callback
                        }
                    ];
                    var options = {
                        size: 'medium',
                        buttons: buttons,
                        title: _t("Select Site"),
                        $content: QWeb.render('SiteSelectDialog', {sites: sites}),
                    };

                    var site_dialog = new Dialog(this, _.extend(options)).open();
                    site_dialog.$el.find('input').click(function() {
                        var value = $(this).attr('value');
                        self.selected_site = value;
                    });
                    return site_dialog;
                }else{
                    Dialog.alert(this, _t("Must at least create one site."));
                }
            });
        },

      move_report_to_site: function(model, report_id){
            var self = this;
            Dialog.confirm(this, _t("Are you sure you want to move this service reports ?"),
                {
                    confirm_callback: function (){
                        self.showSiteDialog(report_id, function(dialog){
                            var site_to = self.selected_site;
                            self._rpc({
                              model: model,
                              method: 'move',
                              args: [ [report_id], site_to]
                            }).then(function(){
                              self.trigger_up('reload_component_view');
                              self.trigger_up('reload');
                              Dialog.alert(self, 'Report moved succesfully');
                            });
                        });
                    }
                });
        },


      showSublocationDialog: function(device_id, current_sublocation, doit){
            var self = this;
            this._rpc({
               model: 'component.sublocation',
               method: 'search_read',
               fields: ['id', 'name'],
               domain: [['id', '!=', current_sublocation]],
            }).then(function(sublocations){
              if (sublocations.length){
                var sublocation_list = [];
                var dialog_content = '';

                dialog_content = '<select id="dialog_sublocation_id">'
                for (var i = 0; i < sublocations.length; i++) {
                     dialog_content = dialog_content +  "<option value='" + sublocations[i].id + "'>" + sublocations[i].name + "</option>";
                }

                dialog_content = dialog_content +  '</select>';

                 var buttons = [
                     {
                         text: _t("Ok"),
                         classes: 'btn-primary',
                         close: true,
                         click: doit
                     },
                     {
                         text: _t("Cancel"),
                         close: true,
                         click: options && options.cancel_callback
                     }
                 ];
                 var options = {
                     size: 'medium',
                     buttons: buttons,
                     title: _t("Select Sublocation"),
                     $content: dialog_content,
                 };

                 var sublocation_dialog = new Dialog(this, _.extend(options)).open();
                 return sublocation_dialog;
              }else{
                 Dialog.alert(this, _t("Must at least create other sublocation."));
              }
            });
        },

      move_device_to_sublocation: function(model, device_id){
             var self = this;
             Dialog.confirm(this, _t("Are you sure you want to move this device to other sublocation ?"),
                  {
                     confirm_callback: function (){
                         self._rpc({
                            model: model,
                            method: 'search_read',
                            fields: ['parent_id'],
                            domain: [['id', '=', device_id]],
                         }).then(function(device){
                             var current_sublocation = device[0].parent_id;

                             self.showSublocationDialog(device_id, current_sublocation, function(dialog){
                                 var sublocation_to = $('#dialog_sublocation_id').prop('selected', true).val();
                                 self._rpc({
                                   model: model,
                                   method: 'move',
                                   args: [ [device_id], sublocation_to]
                                 }).then(function(){
                                   self.trigger_up('reload_component_view');
                                   self.trigger_up('reload');
                                   Dialog.alert(self, 'Device moved succesfully');
                                 });
                             })
                          });
                     }
                  });
      },


      do_custom_action:function(action, model, id){
            switch (action) {
                case 'custom.move_report_to_site':
                    return this.move_report_to_site(model, id);
                case 'custom.copy_reload':
                    var self = this;
                     return this._rpc({
                                      model: model,
                                      method: 'copy_reload',
                                      args: [id]
                                    }).then(function(){
                                      self.trigger_up('reload_component_view');
                                      self.trigger_up('reload');
                                      Dialog.alert(self, 'Report copied succesfully');
                                    });
                case 'custom.copy':
                    var self = this;
                    return this._rpc({
                                  model: model,
                                  method: 'copy',
                                  args: [id]
                                }).then(function(){
                                  self.trigger_up('reload_component_view');
                                  self.trigger_up('reload');
                                  self.trigger_up('reload_tree');
                                  Dialog.alert(self, 'Copied succesfully');
                                });
                case 'custom.copy_device':
                    var self = this;
                    return this._rpc({
                                  model: model,
                                  method: 'copy_device',
                                  args: [id]
                                }).then(function(){
                                  self.trigger_up('reload_component_view');
                                  self.trigger_up('reload');
                                  Dialog.alert(self, 'Copied succesfully');
                                });
                case 'custom.move_device_to_sublocation':
                    return this.move_device_to_sublocation(model, id);
                case 'custom.delete':
                    var self = this;
                    Dialog.confirm(this, _t("Are you sure you want to remove this record ?"),
                        {
                            confirm_callback: function (){
                                return this._rpc({
                                  model: model,
                                  method: 'unlink',
                                  args: [id]
                                }).then(function(){
                                  self.trigger_up('reload_component_view');
                                  self.trigger_up('reload');
                                  self.trigger_up('reload_tree');
                                  Dialog.alert(self, 'Remove succesfully');
                                });
                            }
                        });

            }
        },

      _onButtonClicked: function (event) {
         var action_name = event.data.attrs.name;
         var record_id = event.data.record.res_id;
         var model = event.data.record.model

         if (action_name.startsWith('custom.')){
           event.stopPropagation();
           this.do_custom_action(action_name, model, record_id);
         }else{
           this._super.apply(this, arguments);
         }
      },

      _onEditLine: function (ev) {
         ev.stopPropagation();
         if (this.modelName.startsWith('component_test.') || this.modelName.startsWith('service_reports.')) {
                var record = this.model.get(this.handle);
                var editedRecord = record.data[ev.data.index];

                var self = this;
                this._rpc({
                     model: self.modelName.startsWith('component_test.') ? editedRecord.data.test_type : editedRecord.data.service_reports_type,
                     method: 'search_read',
                     fields: ['id'],
                     domain: [['delegated_id', '=', editedRecord.data.id]],
                }).then(function(test){
                     var id = test[0].id;
                     new WebDialog.FormViewDialog(self, {
                            //context: ev.data.context,
                            res_id: id,
                            res_model: self.modelName.startsWith('component_test.') ? editedRecord.data.test_type : editedRecord.data.service_reports_type,
                            title:  _t("Edit "),
                       }).open();
                });
         }else{
           this._super.apply(this, arguments);
         }
      },


      /* _addRecord: function () {
            var self = this;
            this._disableButtons();
            return this.renderer.unselectRow().then(function () {
                return self.model.addDefaultRecord(self.handle, {
                    position: self.editable,
                });
            }).then(function (recordID) {
                var state = self.model.get(self.handle);
                self.renderer.updateState(state, {});
                self.renderer.editRecord(recordID);
                self._updatePager();
            }).always(this._enableButtons.bind(this));
        },*/

      _addRecord: function () {
        if (this.modelName == 'component_test.order') {
                this.$('table:first').show();
                this.$('.oe_view_nocontent').remove();
                var self = this;
                this.select_component_test_model(function (dialog) {
                    self.do_switch_view(self.selected_test_model, self.initialState.context);
                });
            } else if (this.modelName == 'service_reports.order') {
                this.$('table:first').show();
                this.$('.oe_view_nocontent').remove();
                var self = this;
                this.select_service_reports_model(function (dialog) {
                    self.do_switch_view(self.selected_service_reports_model, self.initialState.context);
                });
            }
            else{
                this._super.apply(this, arguments);
            }
      },

       select_component_test_model: function (do_it) {
            var test_types = [];
            var context = this.getContext();
            var component_model = this.initialState.context["default_component_model"];
            if (component_model == "component.lvdc_cb"){
                test_types.push({title: 'LV DC Circuit Breaker Test', model: "component_test.lvdc_cb_test"});
            }else if (component_model == "component.lv_cb"){
                test_types.push({title: 'LV AC Circuit Breaker Test', model: "component_test.lv_cb_test"});
            }else if (component_model == "component.hvac_circuitbreaker"){
                test_types.push({title: 'HV AC Circuit Breaker Test', model: "component_test.hvac_circuitbreaker_test"});
            }else if (component_model == "component.lv_ac_cable"){
                test_types.push({title: 'HV AC cable Test', model: "component_test.hv_ac_cable_test"});
                test_types.push({title: 'LV AC cable Test', model: "component_test.lv_ac_cable_test"});
            }else if (component_model == "component.dry_transformer"){
                test_types.push({title: 'Dry Transformer Test', model: "component_test.dry_transformer_test"});
            }else if (component_model == "component.liquid_transformer"){
                test_types.push({title: 'Liquid Transformer Test', model: "component_test.liquid_transformer_test"});
            }else if (component_model == "component.curent_transformer"){
                test_types.push({title: 'Current Transformer Test', model: "component_test.curent_transformer_test"});
            }else if (component_model == "component.vol_transformer"){
                test_types.push({title: 'Voltaje Transformer Test', model: "component_test.vol_transformer_test"});
            }else if (component_model == "component.load_bs"){
                test_types.push({title: 'Load break Switch Test', model: "component_test.load_bs_test"});
            }else if (component_model == "component.automatic_transferswitch"){
                test_types.push({title: 'Automatic Transfer Switch Test', model: "component_test.automatic_transferswitch_test"});
            }else if (component_model == "component.lv_ac_bus"){
                test_types.push({title: 'Low Voltage Bus Test', model: "component_test.lv_ac_bus_test"});
            }else if (component_model == "component.hv_ac_bus"){
                test_types.push({title: 'Higth Voltage Bus Test', model: "component_test.hv_ac_bus_test"});
            }else if (component_model == "component.surge_arrestor"){
                test_types.push({title: 'Surge Arrestor Test', model: "component_test.surge_arrestor_test"});
            }else if (component_model == "component.mcb_withrelay"){
                test_types.push({title: 'Molded Case Break Test', model: "component_test.mcb_withrelay_test"});
            }else if (component_model == "component.board"){
                test_types.push({title: 'Board', model: "component_test.board_test"});
            }

          var buttons = [
              {
                  text: _t("Ok"),
                  classes: 'btn-primary',
                  close: true,
                  click: do_it
              },
              {
                  text: _t("Cancel"),
                  close: true,
                  click: options && options.cancel_callback
              }
          ];
          var options = {
              size: 'medium',
              buttons: buttons,
              title: _t("Select Test Type"),
              $content: QWeb.render('TestTypeSelectDialog', {test_types: test_types}),
          };
          var test_dialog = new Dialog(this, _.extend(options)).open();
          var self = this;
          test_dialog.$el.find('input').click(function() {
              var value = $(this).attr('value');
              self.selected_test_model = value;
          });
          return test_dialog;
      },

      jsonp: function(form, attributes, callback) {
        attributes = attributes || {};
        var options = {jsonp: _.uniqueId('import_callback_')};
        window[options.jsonp] = function () {
            delete window[options.jsonp];
            callback.apply(null, arguments);
        };
        if ('data' in attributes) {
            _.extend(attributes.data, options);
        } else {
            _.extend(attributes, {data: options});
        }
        _.extend(attributes, {
            dataType: 'script',
        });
        form.ajaxSubmit(attributes);
      },

       select_component_test_model_and_file: function (component_id, do_it) {
            var test_types = [];
            var context = this.getContext();
            var component_model = this.initialState.context["default_component_model"];
            if (component_model == "component.lvdc_cb"){
                test_types.push({title: 'LV DC Circuit Breaker Test', model: "component_test.lvdc_cb_test"});
            }else if (component_model == "component.lv_cb"){
                test_types.push({title: 'LV AC Circuit Breaker Test', model: "component_test.lv_cb_test"});
            }else if (component_model == "component.hvac_circuitbreaker"){
                test_types.push({title: 'HV AC Circuit Breaker Test', model: "component_test.hvac_circuitbreaker_test"});
            }else if (component_model == "component.lv_ac_cable"){
                test_types.push({title: 'HV AC cable Test', model: "component_test.hv_ac_cable_test"});
                test_types.push({title: 'LV AC cable Test', model: "component_test.lv_ac_cable_test"});
            }else if (component_model == "component.dry_transformer"){
                test_types.push({title: 'Dry Transformer Test', model: "component_test.dry_transformer_test"});
            }else if (component_model == "component.liquid_transformer"){
                test_types.push({title: 'Liquid Transformer Test', model: "component_test.liquid_transformer_test"});
            }else if (component_model == "component.curent_transformer"){
                test_types.push({title: 'Current Transformer Test', model: "component_test.curent_transformer_test"});
            }else if (component_model == "component.vol_transformer"){
                test_types.push({title: 'Voltaje Transformer Test', model: "component_test.vol_transformer_test"});
            }else if (component_model == "component.load_bs"){
                test_types.push({title: 'Load break Switch Test', model: "component_test.load_bs_test"});
            }else if (component_model == "component.automatic_transferswitch"){
                test_types.push({title: 'Automatic Transfer Switch Test', model: "component_test.automatic_transferswitch_test"});
            }else if (component_model == "component.lv_ac_bus"){
                test_types.push({title: 'Low Voltage Bus Test', model: "component_test.lv_ac_bus_test"});
            }else if (component_model == "component.hv_ac_bus"){
                test_types.push({title: 'Higth Voltage Bus Test', model: "component_test.hv_ac_bus_test"});
            }else if (component_model == "component.surge_arrestor"){
                test_types.push({title: 'Surge Arrestor Test', model: "component_test.surge_arrestor_test"});
            }else if (component_model == "component.mcb_withrelay"){
                test_types.push({title: 'Molded Case Break Test', model: "component_test.mcb_withrelay_test"});
            }else if (component_model == "component.board"){
                test_types.push({title: 'Board', model: "component_test.board_test"});
            }

          var buttons = [
              {
                  text: _t("Ok"),
                  classes: 'btn-primary',
                  close: true,
                  click: do_it,
              },
              {
                  text: _t("Cancel"),
                  close: true,
                  click: options && options.cancel_callback
              }
          ];
          var options = {
              size: 'medium',
              buttons: null,
              title: _t("Select Test File"),
              $content: QWeb.render('TestTypeSelectAndFileDialog', {test_types: test_types, component_id: component_id}),
          };
          var test_dialog = new Dialog(this, _.extend(options)).open();
          var self = this;

          self.selected_file = null;
          self.selected_test_model = null;

          test_dialog.$el.find('input.type_select').click(function() {
              var value = $(this).attr('value');
              self.selected_test_model = value;
              //alert(value);
          });

          test_dialog.$el.find('input.xls_file').change(function() {
              self.selected_file = $(this)[0].files[0];
          });

          test_dialog.$el.find('input.btn_ok').click(function() {
              var msg = "";
              if (!self.selected_test_model){
                msg = "select test model. ";
              }

              if (!self.selected_file){
                msg += "select excel file. ";
              }

              if (msg != "") {
                Dialog.alert(self, 'Must, ' +  msg);
              }else{
                /*test_dialog.$el.find('.oe_import_excel_form').ajaxSubmit({
                    url:  '/component_test/import_excel',
                    data: {
                      file: self.selected_file,
                      model: self.selected_test_model,
                    },
                    success: function (quantity) {
                       alert('Success');
                    }
                });*/
                /*self._rpc({
                    model: 'component_test.order',
                    method: 'import_excel',
                    kwargs: {
                      file: self.selected_file,
                      model: self.selected_test_model,
                    },
                }).then(function(test){
                });*/


                self.jsonp(test_dialog.$el.find('.oe_import_excel_form'), {
                   url: '/component_test/import_excel'
                }, self.proxy('import_excel_response'));

                test_dialog.close();

                /*var formData = new FormData();
                formData.append("file",  self.selected_file);
                formData.append("model", self.selected_test_model);
                formData.append("csrf_token", test_dialog.$el.find(".csrf_token"));

                //formData.append(f.attr("name"), $(this)[0].files[0]);
                $.ajax({
                    url: "/component_test/import_excel",
                    type: "post",
                    dataType: "html",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function(res){
                        //$("#mensaje").html("Respuesta: " + res);
                });*/
              }

          });

          return test_dialog;
      },

      import_excel_response: function(response){
        alert(JSON.stringify(response.result));
        //if (response.msg)
        this.trigger_up('reload');
      },


      select_service_reports_model: function(do_it) {
            var service_reports_types = [];

            // Insertar todos los tipos de Service Reports
            service_reports_types.push({title: 'Site Service Reports', model: "service_reports.site"});


            var buttons = [
                {
                    text: _t("Ok"),
                    classes: 'btn-primary',
                    close: true,
                    click: do_it
                },
                {
                    text: _t("Cancel"),
                    close: true,
                    click: options && options.cancel_callback
                }
            ];
            var options = {
                size: 'medium',
                buttons: buttons,
                title: _t("Select Service Reports Type"),
                $content: QWeb.render('ServiceReportsTypeSelectDialog', {service_reports_types: service_reports_types}),
            };
            var report_dialog = new Dialog(this, _.extend(options)).open();
            var self = this;
            report_dialog.$el.find('input').click(function() {
                var value = $(this).attr('value');
                self.selected_service_reports_model = value;
            });
            return report_dialog;
        },

        do_switch_view: function(model, context){
            var self = this;
            if (model.startsWith('component_test.') || model.startsWith('service_reports.') ) {
                self._rpc({
                  model: 'ir.ui.view',
                  method: 'search_read',
                  fields: ['id', 'name', 'type'],
                  domain: [['model', '=', model], ['type', '=', 'form']],
                  limit: 1,
                }).then(function(result){
                    var view = result[0];
                    //var def = $.Deferred();
                    if (self.prev_form_dialog) {
                        self.prev_form_dialog.destroy();
                    }
                    self.prev_form_dialog = new WebDialog.FormViewDialog(self, {
                        res_model: model,
                        context: context,
                        title: "New..",
                        view_id: view.id,
                        //res_id: null,
                        //readonly: false,
                        buttons : [
                            {
                                text: _t("Save"),
                                classes: "btn-primary",
                                //close: true,
                                click: function (ev) {
                                    ev.stopPropagation();
                                    var self = this;
                                    this._save().then(function(){
                                       self.trigger_up('pp');
                                       self.trigger_up('reload');
                                       self.close();
                                    });
                                },
                            },
                            {
                                text: _t("Discard"),
                                classes: "btn-secondary o_form_button_cancel",
                                close: true,
                            }
                        ],
                    }).open();
                });
            }
        },

   });

  ListViewRenderer.include({
      init: function (parent, state, params) {
          this._super.apply(this, arguments);
          this.parent = parent;
      },

      _onAddRecord: function (ev) {
        // we don't want the browser to navigate to a the # url
        ev.preventDefault();

        // we don't want the click to cause other effects, such as unselecting
        // the row that we are creating, because it counts as a click on a tr
        ev.stopPropagation();

        // but we do want to unselect current row
        var self = this;
        this.unselectRow().then(function () {
            self.trigger_up('add_record'/*, {context: ev.currentTarget.dataset.context && [ev.currentTarget.dataset.context]}*/); // TODO write a test, the deferred was not considered
        });
    },

     /*_onAddRecord: function (ev) {
        ev.preventDefault();
        ev.stopPropagation();


        if (this.state.model == 'component_test.order') {
                this.$('table:first').show();
                this.$('.oe_view_nocontent').remove();
                var self = this;
                this.select_component_test_model(function (dialog) {
                   self._rpc({
                      model: self.parent.model,
                      method: 'search_read',
                      fields: ['delegated_id'],
                      domain: [['id', '=', self.parent.record.data.id]],
                   }).then(function(record){
                       var delegated_id = record[0].delegated_id[0];
                       self.do_switch_view(self.selected_test_model, {default_base_component_id: delegated_id});
                   });
                });
        } else if (this.state.model == 'service_reports.order') {
                    this.$('table:first').show();
                    this.$('.oe_view_nocontent').remove();
                    var self = this;
                    this.select_service_reports_model(function (dialog) {
                        self.do_switch_view(self.selected_service_reports_model, {default_base_component_id: self.parent.record.data.delegated_id.res_id});
                    });
                }else{
                   this._super.apply(this, arguments);
                }

        },

        select_component_test_model: function (do_it) {
            var test_types = [];
            //var context = this.getContext();
            var component_model = this.parent.model;
            if (component_model == "component.lvdc_cb"){
                test_types.push({title: 'LV DC Circuit Breaker Test', model: "component_test.lvdc_cb_test"});
            }else if (component_model == "component.lv_cb"){
                test_types.push({title: 'LV AC Circuit Breaker Test', model: "component_test.lv_cb_test"});
            }else if (component_model == "component.hvac_circuitbreaker"){
                test_types.push({title: 'HV AC Circuit Breaker Test', model: "component_test.hvac_circuitbreaker_test"});
            }else if (component_model == "component.lv_ac_cable"){
                test_types.push({title: 'HV AC cable Test', model: "component_test.hv_ac_cable_test"});
                test_types.push({title: 'LV AC cable Test', model: "component_test.lv_ac_cable_test"});
            }else if (component_model == "component.dry_transformer"){
                test_types.push({title: 'Dry Transformer Test', model: "component_test.dry_transformer_test"});
            }else if (component_model == "component.liquid_transformer"){
                test_types.push({title: 'Liquid Transformer Test', model: "component_test.liquid_transformer_test"});
            }else if (component_model == "component.curent_transformer"){
                test_types.push({title: 'Current Transformer Test', model: "component_test.curent_transformer_test"});
            }else if (component_model == "component.vol_transformer"){
                test_types.push({title: 'Voltaje Transformer Test', model: "component_test.vol_transformer_test"});
            }else if (component_model == "component.load_bs"){
                test_types.push({title: 'Load break Switch Test', model: "component_test.load_bs_test"});
            }else if (component_model == "component.automatic_transferswitch"){
                test_types.push({title: 'Automatic Transfer Switch Test', model: "component_test.automatic_transferswitch_test"});
            }else if (component_model == "component.lv_ac_bus"){
                test_types.push({title: 'Low Voltage Bus Test', model: "component_test.lv_ac_bus_test"});
            }else if (component_model == "component.hv_ac_bus"){
                test_types.push({title: 'Higth Voltage Bus Test', model: "component_test.hv_ac_bus_test"});
            }else if (component_model == "component.surge_arrestor"){
                test_types.push({title: 'Surge Arrestor Test', model: "component_test.surge_arrestor_test"});
            }else if (component_model == "component.mcb_withrelay"){
                test_types.push({title: 'Molded Case Break Test', model: "component_test.mcb_withrelay_test"});
            }else if (component_model == "component.board"){
                test_types.push({title: 'Board', model: "component_test.board_test"});
            }


          var buttons = [
              {
                  text: _t("Ok"),
                  classes: 'btn-primary',
                  close: true,
                  click: do_it
              },
              {
                  text: _t("Cancel"),
                  close: true,
                  click: options && options.cancel_callback
              }
          ];
          var options = {
              size: 'medium',
              buttons: buttons,
              title: _t("Select Test Type"),
              $content: QWeb.render('TestTypeSelectDialog', {test_types: test_types}),
          };
          var test_dialog = new Dialog(this, _.extend(options)).open();
          var self = this;
          test_dialog.$el.find('input').click(function() {
              var value = $(this).attr('value');
              self.selected_test_model = value;
          });
          return test_dialog;
      },


      /*select_service_reports_model: function(do_it) {
            var service_reports_types = [];

            service_reports_types.push({title: 'Site Service Reports', model: "service_reports.site"});


            var buttons = [
                {
                    text: _t("Ok"),
                    classes: 'btn-primary',
                    close: true,
                    click: do_it
                },
                {
                    text: _t("Cancel"),
                    close: true,
                    click: options && options.cancel_callback
                }
            ];
            var options = {
                size: 'medium',
                buttons: buttons,
                title: _t("Select Service Reports Type"),
                $content: QWeb.render('ServiceReportsTypeSelectDialog', {service_reports_types: service_reports_types}),
            };
            var report_dialog = new Dialog(this, _.extend(options)).open();
            var self = this;
            report_dialog.$el.find('input').click(function() {
                var value = $(this).attr('value');
                self.selected_service_reports_model = value;
            });
            return report_dialog;
        },*/

        do_switch_view: function(model, context){
            var self = this;
            if (model.startsWith('component_test.') || model.startsWith('service_reports.') ) {
                self._rpc({
                  model: 'ir.ui.view',
                  method: 'search_read',
                  fields: ['id', 'name', 'type'],
                  domain: [['model', '=', model], ['type', '=', 'form']],
                  limit: 1,
                }).then(function(result){
                    var view = result[0];
                    //var def = $.Deferred();
                    if (self.prev_form_dialog) {
                        self.prev_form_dialog.destroy();
                    }
                    self.prev_form_dialog = new WebDialog.FormViewDialog(self, {
                        res_model: model,
                        context: context,
                        title: "New..",
                        view_id: view.id,
                        //res_id: null,
                        //readonly: false,
                        buttons : [
                            {
                                text: _t("Save"),
                                classes: "btn-primary",
                                //close: true,
                                click: function (ev) {
                                    ev.stopPropagation();
                                    var self = this;
                                    this._save().then(function(){
                                       self.trigger_up('reload_component_view');
                                       self.trigger_up('reload');
                                       self.close();
                                    });
                                },
                            },
                            {
                                text: _t("Discard"),
                                classes: "btn-secondary o_form_button_cancel",
                                close: true,
                            }
                        ],
                    }).open();
                });
            }
        },

       /* _onRowClicked: function () {
         if (this.modelName == "PP" and !this._isEditable()) {
             //alert('OKKKKK PPP');
          }
      */

        /*_selectRow: function (rowIndex) {
          //alert('seleccionando la fila');
        },

        //editRecord: function (recordID) {
        //   alert('Editando la fila');
        //},

        _onCellClick: function (event) {
           event.stopPropagation();
           //alert('Probando ');
        },*/



   });

   FormController.include({
      init: function (parent, model, renderer, params) {
        this._super.apply(this, arguments);
        this.parent = parent;
      },

      _onCopyReload: function (model, id){
         var self = this;
         this._rpc({
            model: model,
            method: 'copy_reload',
            args: [id]
         }).then(function(){
               self.trigger_up('reload_component_view');
               self.trigger_up('reload');
               //Dialog.alert(self, 'Report copied succesfully');
         });
       },

       _onSave: function (ev) {
         this._super(ev);
          if (this.electric_component){
            this.trigger_up('reload');
            this.trigger_up('reload_component_view');
          }
       },

      do_custom_action:function(event, action, model, id){
            switch (action) {
                case 'custom.copy_reload':
                  if (event.data.readonly){
                    this._onCopyReload(model, id);
                  }else{
                     var self = this;
                     this.saveRecord(this.handle, {
                        stayInEdit: true,
                     }).then(function(){
                        self._onCopyReload(model, id);
                     });
                  }
                  return;
                case 'custom.write':
                    var self = this;
                    self.saveRecord().then(function(){
                      self.trigger_up('reload_component_view');
                      self.parent.trigger_up('reload');
                      self.parent.close();
                    });
                   return;
                case 'custom.cancel':
                    this.parent.close();
            }
        },

      _onButtonClicked: function (event) {
         var action_name = event.data.attrs.name;
         var record_id = event.data.record.res_id;
         var model = event.data.record.model;
         var attrs = event.data.attrs;

         if (action_name.startsWith('custom.')){
           event.stopPropagation();
           this.do_custom_action(event, action_name, model, record_id);
         }else{
           //event.stopPropagation();
           this._super.apply(this, arguments);
         }
      },

      _onOpenOne2ManyRecord: function (event) {
       if (this.modelName.startsWith('component.')){
           if (event.data.field.relation == 'component_test.order'){
                var self = this;
                this.saveRecord(this.handle, {
                   stayInEdit: true,
                }).then(function () {
                   //event.stopPropagation();
                   self._onOpenTestForm(event);
                });
            }else if (event.data.field.relation == 'ir.attachment'){
              this._onOpenDocForm(event);
            }
       }else{
         this._super(event);
       }
      },

      _onOpenTestForm: function (event){
        var data = event.data;
        var record;
        if (data.id) {
            //Editar el test
            record = this.model.get(data.id, {raw: true});

            var self = this;
            this._rpc({
                 model: record.data.test_type,
                 method: 'search_read',
                 fields: ['id'],
                 domain: [['delegated_id', '=', record.data.id]],
            }).then(function(test){
                 var id = test[0].id;
                 new WebDialog.FormViewDialog(self, {
                        context: data.context,
                        res_id: id,
                        res_model: record.data.test_type,
                        title:  _t("Edit "),
                   }).open();
            });
        }else{
           //Crear el Test
           var self = this;
           this.select_component_test_model(function (dialog) {
                   self._rpc({
                         model: self.modelName,
                         method: 'search_read',
                         fields: ['delegated_id'],
                         domain: [['id', '=', self.initialState.data.id]],
                    }).then(function(component){
                           var id = component[0].delegated_id[0];
                           var context = data.context;
                           context.default_base_component_id = id;
                           context.default_test_type = self.selected_test_model;

                           new WebDialog.FormViewDialog(self, {
                                context: context,
                                res_model: self.selected_test_model,
                                title:  _t("Create "),
                           }).open();
                    });
           });
        }
      },

      _onOpenDocForm: function (event){
        event.stopPropagation();
        var data = event.data;
        var record;
        if (data.id) {
            record = this.model.get(data.id, {raw: true});
        }

        new WebDialog.FormViewDialog(this, {
            context: data.context,
            domain: data.domain,
            fields_view: data.fields_view,
            model: this.model,
            on_saved: data.on_saved,
            on_remove: data.on_remove,
            parentID: data.parentID,
            readonly: data.readonly,
            deletable: data.deletable,
            recordID: record && record.id,
            res_id: record && record.res_id,
            res_model: data.field.relation,
            shouldSaveLocally: true,
            title: (record ? _t("Open: ") : _t("Create ")) + (event.target.string || data.field.string),
        }).open();
      },

      select_component_test_model: function (do_it) {
            var test_types = [];
            var component_model = this.modelName;
            if (component_model == "component.lvdc_cb"){
                test_types.push({title: 'LV DC Circuit Breaker Test', model: "component_test.lvdc_cb_test"});
            }else if (component_model == "component.lv_cb"){
                test_types.push({title: 'LV AC Circuit Breaker Test', model: "component_test.lv_cb_test"});
            }else if (component_model == "component.hvac_circuitbreaker"){
                test_types.push({title: 'HV AC Circuit Breaker Test', model: "component_test.hvac_circuitbreaker_test"});
            }else if (component_model == "component.lv_ac_cable"){
                test_types.push({title: 'HV AC cable Test', model: "component_test.hv_ac_cable_test"});
                test_types.push({title: 'LV AC cable Test', model: "component_test.lv_ac_cable_test"});
            }else if (component_model == "component.dry_transformer"){
                test_types.push({title: 'Dry Transformer Test', model: "component_test.dry_transformer_test"});
            }else if (component_model == "component.liquid_transformer"){
                test_types.push({title: 'Liquid Transformer Test', model: "component_test.liquid_transformer_test"});
            }else if (component_model == "component.curent_transformer"){
                test_types.push({title: 'Current Transformer Test', model: "component_test.curent_transformer_test"});
            }else if (component_model == "component.vol_transformer"){
                test_types.push({title: 'Voltaje Transformer Test', model: "component_test.vol_transformer_test"});
            }else if (component_model == "component.load_bs"){
                test_types.push({title: 'Load break Switch Test', model: "component_test.load_bs_test"});
            }else if (component_model == "component.automatic_transferswitch"){
                test_types.push({title: 'Automatic Transfer Switch Test', model: "component_test.automatic_transferswitch_test"});
            }else if (component_model == "component.lv_ac_bus"){
                test_types.push({title: 'Low Voltage Bus Test', model: "component_test.lv_ac_bus_test"});
            }else if (component_model == "component.hv_ac_bus"){
                test_types.push({title: 'Higth Voltage Bus Test', model: "component_test.hv_ac_bus_test"});
            }else if (component_model == "component.surge_arrestor"){
                test_types.push({title: 'Surge Arrestor Test', model: "component_test.surge_arrestor_test"});
            }else if (component_model == "component.mcb_withrelay"){
                test_types.push({title: 'Molded Case Break Test', model: "component_test.mcb_withrelay_test"});
            }else if (component_model == "component.board"){
                test_types.push({title: 'Board', model: "component_test.board_test"});
            }


          var buttons = [
              {
                  text: _t("Ok"),
                  classes: 'btn-primary',
                  close: true,
                  click: do_it
              },
              {
                  text: _t("Cancel"),
                  close: true,
                  click: options && options.cancel_callback
              }
          ];
          var options = {
              size: 'medium',
              buttons: buttons,
              title: _t("Select Test Type"),
              $content: QWeb.render('TestTypeSelectDialog', {test_types: test_types}),
          };
          var test_dialog = new Dialog(this, _.extend(options)).open();
          var self = this;
          test_dialog.$el.find('input').click(function() {
              var value = $(this).attr('value');
              self.selected_test_model = value;
          });
          return test_dialog;
      },

   });

});
